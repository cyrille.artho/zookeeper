public class Main {
  public final static void main(String[] args) {
    Server server = new Server();
    Client c1 = new Client(server);
    Client c2 = new Client(server);
    c1.doSomething(); // create or modify data
    c2.doSomething(); // create or modify data
    try {
      c1.callback.join();
      c2.callback.join();
    } catch (InterruptedException e) {}
    // check result
  }
}
