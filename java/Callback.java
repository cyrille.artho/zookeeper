abstract class Callback extends Thread {
  int idx, val;
  void Callback(int index, int value) {
    idx = index;
    val = value;
    this.start();
  }

  public void run() {
    try {
      Thread.sleep(50);
    } catch (InterruptedException e) {}
    process(idx, val);
  }

  abstract void process(int index, int value);
}
