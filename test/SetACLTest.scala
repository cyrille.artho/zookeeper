package test

import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.Assertions._
import org.apache.zookeeper.KeeperException
import scala.collection.mutable.ListBuffer
import org.apache.zookeeper.{Watcher, WatchedEvent, ZooKeeper, CreateMode}
import org.apache.zookeeper.AsyncCallback.StatCallback
import org.apache.zookeeper.KeeperException.Code
import org.apache.zookeeper.KeeperException.Code._
import java.io.{File, IOException}
import org.apache.zookeeper.server.{ServerConfig, ZooKeeperServer, ServerCnxnFactory}
import org.apache.zookeeper.data.{ACL,Stat,Id}
import org.apache.zookeeper.KeeperException
import org.apache.zookeeper.Watcher.Event.{KeeperState, EventType}
import org.apache.zookeeper.ZooDefs.{Ids, Perms}
import scala.collection.JavaConversions._

class SetACLTest extends FlatSpec with Matchers {

  //Config server
  val numConnections = 5000
  var tickTime = 3000
  val dataDirectory = "./temp"
  var zkPort: Integer = null
  var standaloneServerFactory: ServerCnxnFactory = null
  var zkServer: ZooKeeperServer = null
  var minSessionTimeout = -1
  var maxSessionTimeout = -1
  var zks:ZooKeeperServer = null
  val lock = new Object

  //local variables and checking
  var serverIsConnected = false
  var zk:ZooKeeper = null

  class CheckSetACLCallback() extends StatCallback {
    override def processResult(rc: Int, path: String, ctx: Object, stat: Stat) {
      val returnedCode = Code.get(rc)
      returnedCode match {
        case OK =>
        case _ => assert(false, "An abnormal rc code has been returned" + returnedCode)
      }
    }
  }

  def startServer {
    var dir = new File(dataDirectory, "zookeeper").getAbsoluteFile()
    zks = new ZooKeeperServer(dir, dir, tickTime)
    standaloneServerFactory = ServerCnxnFactory.createFactory(0, numConnections)
    zkPort = standaloneServerFactory.getLocalPort()
    standaloneServerFactory.startup(zks)
    serverIsConnected = true
  }

  def startClient {
    val host = "127.0.0.1:" + zkPort + "/"
    val SESSIONTIMEOUT = 2000
    var isConnected = false
    val watchConnection: Watcher = new Watcher() {
      override def process(event: WatchedEvent) {
        lock.synchronized {
          event.getState() match {
            case KeeperState.SyncConnected => isConnected = true
            case KeeperState.Disconnected => isConnected = false
            case _ =>
          }
          lock.notify()
        }
      }
    }
    zk = new ZooKeeper(host, SESSIONTIMEOUT, watchConnection)
    lock.synchronized {
      try {
        while (!isConnected) { // let watcher be triggered on slow computers
          lock.wait()
        }
        while(!zk.getState.isConnected()) {
          Thread.sleep(10)
        }
      } catch {
        case _: InterruptedException => // ignore InterruptedException
      }
    }
  }

  def closeServer {
    standaloneServerFactory.shutdown()
    zks.shutdown()
  }

  def closeClient {
    zk.close()
  }


  "Create /a without permission" should " throw a NoAuthException" in{
    startServer
    startClient
    val aclBuffer = ListBuffer[ACL](new ACL (0, Ids.ANYONE_ID_UNSAFE))
    val result = zk.getACL("/", new Stat)
    zk.setACL("/", aclBuffer, -1, new CheckSetACLCallback(), null)
    // Thread.sleep(100)
    a [KeeperException.NoAuthException] should be thrownBy {
      zk.create("/a", null, Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)
    }
    closeClient
    closeServer
  }
}
