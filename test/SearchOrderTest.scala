package test

import events._
import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.Assertions._
import org.apache.zookeeper.{KeeperException, OpResult}
import org.apache.zookeeper.data.Id
import org.apache.zookeeper.ZooDefs.Ids

class SearchOrderTest extends FlatSpec with Matchers {
  val noSync = List[ZKEvent]()
  val zkIDs = List(Ids.ANYONE_ID_UNSAFE)
  val testData = "testData".getBytes
  Search.PRUNING = false
  /*
   session 1: create /a, delete /a
   session 2: delete /a, create /a
   timestamp_created: create /a(session1) < delete /a(2) <  delete /a(1) < create /a(2)
   timestamp_finish: create /a(1) < delete /a(1) < delete /a(2) < create /a(2)
   when the result of delete /a(2) is () and the oracle check the result,
   the search order of each algorithm should be as follows:
   BFS : create /a(1) -> delete /a(2) -> delete /a(1)  -> delete /a(2)
   DFS : delete /a(2) -> create /a(1) -> delete /a(2)
   HG_create : create /a(1) -> delete /a(2)
   HG_finish : create /a(1) -> delete /a(1) -> delete /a(2) -> delete /a(2)
   HG_create_strict : create /a(1) -> delete /a(2) -> delete /a(2)
   HG_finish_strict : create /a(1) -> delete /a(1) -> delete /a(2)
   when the result of create /a(2) is "/a" and the oracle check the result,
   the search order of each algorithm should be as follows:
   BFS : create /a(1) -> delete /a(2) -> delete /a(1)  -> delete /a(2) -> create /a(1) -> create /a(2)
   DFS : delete /a(2) -> create /a(2)
   HG_create : create /a(1) -> delete /a(2) -> delete /a(1) -> create /a(2)
   HG_finish : create /a(1) -> delete /a(1) -> delete /a(2) -> create /a(2)
   HG_create_strict : create /a(1) -> delete /a(2) -> create /a(1) -> delete /a(2) -> delete /a(1) -> delete /a(2) -> delete /a(1) -> delete /a(1) -> create /a(2)
   HG_finish_strict : create /a(1) -> delete /a(1) -> delete /a(2) -> delete /a(1) -> delete /a(2) -> create /a(1) -> delete /a(1) -> delete /a(2) -> create /a(2)
   */
  val client1Name = "client1"
  val client2Name = "client2"
  val e11 = ZKEvent.create("/a", Create, zkIDs, client1Name)
  val e12 = ZKEvent.create("/a", Delete, zkIDs, client1Name)
  val e21 = ZKEvent.create("/a", Delete, zkIDs, client2Name)
  val e22 = ZKEvent.create("/a", Create, zkIDs, client2Name)
  e11.timestamp_create = 1
  e12.timestamp_create = 3
  e21.timestamp_create = 2
  e22.timestamp_create = 4
  e11.timestamp_finish = 5
  e12.timestamp_finish = 6
  e21.timestamp_finish = 7
  e22.timestamp_finish = 8
  val sequence = List(List(e11, e12), List(e21, e22))
  println(sequence)
  val sg21 = new ZKSessionGraph(noSync, e21, sequence, null)
  val sg22 = new ZKSessionGraph(noSync, e22, sequence, null)

  Search.debug = true
  "The order of bfs(e21)" should "(List(0, 0), 0), (List(0, 0), 1), (List(1, 0), 0), (List(1, 0), 1)" in {
    Search.SEARCH_ALGO = BFS
    ZKEvent.order.clear
    ZKSearch.analyze(sg21, Some(()), None)
    assert(List((List(0, 0), 0), (List(0, 0), 1), (List(1, 0), 0), (List(1, 0), 1)) == ZKEvent.order)
  }

  "The order of dfs(e21)" should "(List(0, 0), 1),(List(0, 0), 0), (List(1, 0), 1)" in {
    Search.SEARCH_ALGO =DFS
    ZKEvent.order.clear
    ZKSearch.analyze(sg21, Some(()), None)
    assert(List((List(0, 0), 1),(List(0, 0), 0), (List(1, 0), 1)) == ZKEvent.order)
  }

  "The order of hg_create(e21)" should "(List(0, 0), 0), (List(1, 0), 1)" in {
    Search.SEARCH_ALGO = HG_Create
    ZKEvent.order.clear
    ZKSearch.analyze(sg21, Some(()), None)
    assert(List((List(0, 0), 0), (List(1, 0), 1)) == ZKEvent.order)
  }

  "The order of hg_finish(e21)" should "(List(0, 0), 0), (List(1, 0), 0), (List(2, 0), 1), (List(1, 0), 1)" in {
    Search.SEARCH_ALGO = HG_Finish
    ZKEvent.order.clear
    ZKSearch.analyze(sg21, Some(()), None)
    assert(List((List(0, 0), 0), (List(1, 0), 0), (List(2, 0), 1), (List(1, 0), 1)) == ZKEvent.order)
  }

  "The order of hg_create_strict(e21)" should "(List(0, 0), 0), (List(1, 0), 1)" in {
    // The order depends on which value is choosed when the timestamps of two values are the same.
    Search.SEARCH_ALGO = HG_Create_Strict
    ZKEvent.order.clear
    ZKSearch.analyze(sg21, Some(()), None)
    assert(List((List(0, 0), 0), (List(0, 0), 1), (List(1, 0), 1)) == ZKEvent.order)
  }

  "The order of hg_finish_strict" should "(List(0, 0), 0), (List(1, 0), 0), (List(2, 0), 1), (List(1, 0), 1)" in {
    // The order depends on which value is choosed when the timestamps of two values are the same.
    Search.SEARCH_ALGO = HG_Finish_Strict
    ZKEvent.order.clear
    ZKSearch.analyze(sg21, Some(()), None)
    // assert(List((List(0, 0), 0), (List(1, 0), 0), (List(0, 0), 1), (List(1, 0), 1)) == ZKEvent.order)
    assert(List((List(0, 0), 0), (List(1, 0), 0), (List(1, 0), 1)) == ZKEvent.order)
  }

  "The order of bfs(e22)" should "List((List(0, 0), 0), (List(0, 0), 1), (List(1, 0), 0), (List(1, 0), 1), (List(0, 1), 0), (List(0, 1), 1))" in {
    Search.SEARCH_ALGO = BFS
    ZKEvent.order.clear
    ZKSearch.analyze(sg22, Some("/a"), None)
    assert(List((List(0, 0), 0), (List(0, 0), 1), (List(1, 0), 0), (List(1, 0), 1), (List(0, 1), 0), (List(0, 1), 1)) == ZKEvent.order)
  }

  "The order of dfs(e22)" should "List((List(0, 0), 1), (List(0, 1), 1))" in {
    Search.SEARCH_ALGO = DFS
    ZKEvent.order.clear
    ZKSearch.analyze(sg22, Some("/a"), None)
    assert(List((List(0, 0), 1), (List(0, 1), 1)) == ZKEvent.order)
  }

  "The order of hg_create(e22)" should "List((List(0, 0), 0), (List(1, 0), 1), (List(1, 1), 0), (List(2, 1), 1))" in {
    Search.SEARCH_ALGO = HG_Create
    ZKEvent.order.clear
    ZKSearch.analyze(sg22, Some("/a"), None)
    assert(List((List(0, 0), 0), (List(1, 0), 1), (List(1, 1), 0), (List(2, 1), 1)) == ZKEvent.order)
  }

  "The order of hg_finish(e22)" should "List((List(0, 0), 0), (List(1, 0), 0), (List(2, 0), 1), (List(2, 1), 1))" in {
    Search.SEARCH_ALGO = HG_Finish
    ZKEvent.order.clear
    ZKSearch.analyze(sg22, Some("/a"), None)
    assert(List((List(0, 0), 0), (List(1, 0), 0), (List(2, 0), 1), (List(2, 1), 1)) == ZKEvent.order)
  }

  "The order of hg_create_strict(e22)" should "(List(0, 0), 0), (List(0, 0), 1), (List(0, 1), 0), (List(1, 0), 1), (List(1, 0), 0), (List(2, 0), 1), (List(1, 1), 0), (List(1, 1), 0), (List(1, 1), 1)" in {
    // The order depends on which value is choosed when the timestamps of two values are the same.
    Search.SEARCH_ALGO = HG_Create_Strict
    ZKEvent.order.clear
    ZKSearch.analyze(sg22, Some("/a"), None)
    assert(List((List(0, 0), 0), (List(0, 0), 1), (List(0, 1), 0), (List(1, 0), 1), (List(1, 0), 0), (List(2, 0), 1), (List(1, 1), 0), (List(1, 1), 0), (List(1, 1), 1)) == ZKEvent.order)
  }

  "The order of hg_finish_strict(e22)" should "(List(0, 0), 0), (List(1, 0), 0), (List(1, 0), 1), (List(1, 1), 0), (List(0, 0), 1), (List(0, 1), 0), (List(1, 1), 0), (List(2, 0), 1), (List(0, 1), 1)" in {
    // The order depends on which value is choosed when the timestamps of two values are the same.
    Search.SEARCH_ALGO = HG_Finish_Strict
    ZKEvent.order.clear
    ZKSearch.analyze(sg22, Some("/a"), None)
    assert(List((List(0, 0), 0), (List(1, 0), 0), (List(1, 0), 1), (List(1, 1), 0), (List(0, 0), 1), (List(0, 1), 0), (List(1, 1), 0), (List(2, 0), 1), (List(0, 1), 1)) == ZKEvent.order)
  }

}
