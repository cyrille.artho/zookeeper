package test

import events._
import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.Assertions._
import org.apache.zookeeper.{KeeperException, OpResult}
import org.apache.zookeeper.data.Id
import org.apache.zookeeper.ZooDefs.Ids

class WatcherTriggerTest extends FlatSpec with Matchers {
  val noSync = List[ZKEvent]()
  val zkIDs = List(Ids.ANYONE_ID_UNSAFE)
  val testData = "testData".getBytes
  Search.PRUNING = false
  val client1Name = "client1"
  val create = ZKEvent.create("/a", Create, zkIDs, client1Name)
  val delete = ZKEvent.create("/a", Delete, zkIDs, client1Name)
  val exists = ZKEvent.create("/a", Exists, zkIDs, client1Name, true)
  val getData = ZKEvent.create("/a", GetData, zkIDs, client1Name, true)
  val getChildren = ZKEvent.create("/", GetChildren, zkIDs, client1Name, true)
  val setData = ZKEvent.create("/a", SetData("datatest".getBytes), zkIDs, client1Name, true)

  "create /a, delete /a, exists /a(watch)" should "not trigger the watcher" in {
    val sequence = List(List(create, delete, exists))
    val sg = new ZKSessionGraph(noSync, exists, sequence, null)
    ZKSearch.watcherTriggerAnalyze(sg)
  }

  "create /a, exists /a(watch), delete /a" should "trigger the watcher" in {
    val sequence = List(List(create, exists, delete))
    val sg = new ZKSessionGraph(noSync, exists, sequence, null)
    var exception = false
    try {
      ZKSearch.watcherTriggerAnalyze(sg)
    } catch{
      case e:AssertionError => exception = true
    }
    assert(exception)
  }

  "exists /a(watch), create /a" should "trigger the watcher" in {
    val sequence = List(List(exists, create))
    val sg = new ZKSessionGraph(noSync, exists, sequence, null)
    var exception = false
    try {
      ZKSearch.watcherTriggerAnalyze(sg)
    } catch{
      case e:AssertionError => exception = true
    }
    assert(exception)
  }

  "create /a, exists /a(watch), setData /a" should "trigger the watcher" in {
    val sequence = List(List(create, exists, setData))
    val sg = new ZKSessionGraph(noSync, exists, sequence, null)
    var exception = false
    try {
      ZKSearch.watcherTriggerAnalyze(sg)
    } catch{
      case e:AssertionError => exception = true
    }
    assert(exception)
  }

  "create /a, delete /a, getData /a(watch)" should "not trigger the watcher" in {
    val sequence = List(List(create, delete, getData))
    val sg = new ZKSessionGraph(noSync, getData, sequence, null)
    ZKSearch.watcherTriggerAnalyze(sg)
  }

  "create /a, getData /a(watch), delete /a" should "trigger the watcher" in {
    val sequence = List(List(create, getData, delete))
    val sg = new ZKSessionGraph(noSync, getData, sequence, null)
    var exception = false
    try {
      ZKSearch.watcherTriggerAnalyze(sg)
    } catch{
      case e:AssertionError => exception = true
    }
    assert(exception)
  }

  "create /a, getData /a(watch), setData /a" should "trigger the watcher" in {
    val sequence = List(List(create, getData, setData))
    val sg = new ZKSessionGraph(noSync, getData, sequence, null)
    var exception = false
    try {
      ZKSearch.watcherTriggerAnalyze(sg)
    } catch{
      case e:AssertionError => exception = true
    }
    assert(exception)
  }

  "create /a, getChildren /" should "not trigger the watcher" in {
    val sequence = List(List(create, getChildren))
    val sg = new ZKSessionGraph(noSync, getChildren, sequence, null)
    ZKSearch.watcherTriggerAnalyze(sg)
  }

  "getChildren /(watch), create /a" should "trigger the watcher" in {
    val sequence = List(List(getChildren, create))
    val sg = new ZKSessionGraph(noSync, getChildren, sequence, null)
    var exception = false
    try {
      ZKSearch.watcherTriggerAnalyze(sg)
    } catch{
      case e:AssertionError => exception = true
    }
    assert(exception)
  }
}
