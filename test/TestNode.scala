package test

import collection.mutable.Stack
import org.scalatest._
import modbat._
import org.apache.zookeeper.KeeperException
import org.apache.zookeeper.ZooDefs.Ids

class TestNode extends FlatSpec with Matchers with BeforeAndAfter {
  val zkIDs = List(Ids.ANYONE_ID_UNSAFE)

  "The root Node" should "have no Children when it is first created" in {
    new Tree().getChildren("/", zkIDs) should be (Set("zookeeper"))
  }

  it should "not be deletable" in {
    a [KeeperException.BadArgumentsException] should be thrownBy {
      new Tree().delete("/", zkIDs)
    }
  }

  "A Node" should "set its Data and preserve it if set/getData is called" in {
    val t = new Tree
    t.setData("/", "datatest".getBytes, zkIDs)
    t.getData("/", zkIDs) match {
      case Some(d: Seq[Byte]) => d should be ("datatest".getBytes)
      case _ => assert(false)
    }
  }

  it should "create a child node and store it if create is called" in {
    val t = new Tree
    t.create("/a", zkIDs)
    t.getChildren("/", zkIDs) should be (Set("zookeeper","a"))
  }

  it should "create multiple children node and store them if create is called multiple times on the same node" in {
    val t = new Tree
    t.create("/a", zkIDs)
    t.create("/b", zkIDs)
    t.getChildren("/", zkIDs) should be (Set("zookeeper","a","b"))
  }

  it should "throw NodeExistsException if an already existing node should be create" in {
    val t = new Tree
    t.create("/a", zkIDs)
    a [KeeperException.NodeExistsException] should be thrownBy {
      t.create("/a", zkIDs) //shoud throw an exception
    }
    t.getChildren("/", zkIDs) should be (Set("zookeeper","a"))
  }

  it should "create two nodes with the same name" in {
    val t = new Tree
    t.create("/a", zkIDs)
    t.create("/a/b", zkIDs)
    t.create("/a/b/a", zkIDs)
    t.getChildren("/a/b", zkIDs) should be (Set("a"))
  }

  it should "delete a child if delete is called" in {
    val t = new Tree
    t.create("/a", zkIDs)
    t.delete("/a", zkIDs)
    t.getChildren("/", zkIDs) should be (Set("zookeeper"))
  }

  it should "check if a node exist if exists is called" in {
    val t = new Tree
    t.create("/a", zkIDs)
    t.create("/a/b", zkIDs)
    t.getChildren("/a", zkIDs) should be (Set("b"))
    t.exists("/a/b") should be (true)
  }

  it should "remove every nodes except the root if clear is called" in {
    val t = new Tree
    t.create("/a", zkIDs)
    t.create("/a/b", zkIDs)
    t.create("/c", zkIDs)
    t.clear
    t.getChildren("/", zkIDs) should be (Set("zookeeper"))
  }

  it should "throw KeeperException if a node researched doesn't exist" in {
    val t = new Tree
    a [KeeperException] should be thrownBy {
      t.create("/a/b", zkIDs)
    }
  }

  it should "throw KeeperException if a node that should be deleted has at least a child if delete is called" in {
    val t = new Tree
    a [KeeperException] should be thrownBy {
      t.create("/a", zkIDs)
      t.create("/a/b", zkIDs)
      t.delete("/a", zkIDs)
    }
  }

  "Debugging" should "/a/b/c/i should return false" in {
    val t = new Tree
    t.create("/a", zkIDs)
    //t.create("/a/b")
    //t.create("/a/b/c")
    //t.create("/a/b/c/i")

    //t.setData("/b","datatest".getBytes)
    t.setData("/a","datatest".getBytes, zkIDs)
    t.delete("/a", zkIDs)
    t.exists("/a") should be (false)
  }

  "Debugging" should "/a/b/i/c should return true" in {
    val t = new Tree
    t.create("/a", zkIDs)
    //t.create("/a/b")
    //t.create("/a/b/c")
    //t.create("/a/b/c/i")

    //t.setData("/b","datatest".getBytes)
    t.setData("/a","datatest".getBytes, zkIDs)
    t.exists("/a") should be (true)
  }

}
