package test

import events._
import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.Assertions._
import org.apache.zookeeper.{KeeperException, OpResult}
import org.apache.zookeeper.data.Id
import org.apache.zookeeper.ZooDefs.Ids

class PruningTest extends FlatSpec with Matchers {
  Search.SEARCH_ALGO = BFS
  val noSync = List[ZKEvent]()
  val zkIDs = List(Ids.ANYONE_ID_UNSAFE)
  val client1Name = "client1"
  val client2Name = "client2"
  val e11 = ZKEvent.create("/a", Create, zkIDs, client1Name)
  val e21 = ZKEvent.create("/a", Delete, zkIDs, client2Name)
  val e22 = ZKEvent.create("/a", Delete, zkIDs, client2Name)
  val sequence = List(List(e11), List(e21, e22))
//  val results = List(e11 -> (Some("/a"), None), e21 -> (Some(true), None), e22 -> (Some(false), None)).toMap
  val results = List(e11 -> (Some("/a"), None), e21 -> (Some(()), None), e22 -> (Some(()), None)).toMap
//  print(sequence)

  val sg = new ZKSessionGraph(noSync, e22, sequence, results)

  "Pruning" should "find the defect" in {
    var exc = false
    try {
      Search.PRUNING = true
      ZKEvent.order.clear
      ZKSearch.analyze(sg, Some(()), None)
    } catch {
      case e:Throwable => {
          exc = true
//          println(e)
      }
    }
    assert(exc)
  }

  "No-Pruning" should "not find the defect" in {
    Search.PRUNING = false
    ZKEvent.order.clear
    ZKSearch.analyze(sg, Some(()), None)
  }

}
