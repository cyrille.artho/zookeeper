package test

import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.Assertions._
import org.apache.zookeeper.KeeperException
import scala.collection.mutable.ListBuffer
import org.apache.zookeeper.{Watcher, WatchedEvent, ZooKeeper, CreateMode, Transaction, ZooDefs, OpResult}
import org.apache.zookeeper.AsyncCallback.StatCallback
import org.apache.zookeeper.KeeperException.Code
import org.apache.zookeeper.KeeperException.Code._
import java.io.{File, IOException}
import org.apache.zookeeper.server.{ServerConfig, ZooKeeperServer, ServerCnxnFactory}
import org.apache.zookeeper.data.{ACL,Stat,Id}
import org.apache.zookeeper.KeeperException
import org.apache.zookeeper.Watcher.Event.{KeeperState, EventType}
import org.apache.zookeeper.ZooDefs.{Ids, Perms}
import scala.collection.JavaConversions._
import events.Log

class TransactionTest extends FlatSpec with Matchers {

  //Config server
  val numConnections = 5000
  var tickTime = 3000
  val dataDirectory = "./temp"
  var zkPort: Integer = null
  var standaloneServerFactory: ServerCnxnFactory = null
  var zkServer: ZooKeeperServer = null
  var minSessionTimeout = -1
  var maxSessionTimeout = -1
  var zks:ZooKeeperServer = null
  val lock = new Object

  //local variables and checking
  var serverIsConnected = false
  var zk:ZooKeeper = null

  def deleteAll(folder: File) {
    var files = folder.listFiles()
    if(files != null) {
      for(f <- files) {
        if(f.isDirectory()) {
          deleteAll(f)
        } else {
          f.delete()
        }
      }
    }
    folder.delete()
  }

  def startServer {
    val temp = new File(dataDirectory)
    deleteAll(temp)

    var dir = new File(dataDirectory, "zookeeper").getAbsoluteFile()
    zks = new ZooKeeperServer(dir, dir, tickTime)
    standaloneServerFactory = ServerCnxnFactory.createFactory(0, numConnections)
    zkPort = standaloneServerFactory.getLocalPort()
    standaloneServerFactory.startup(zks)
    serverIsConnected = true
  }

  def startClient {
    val host = "127.0.0.1:" + zkPort + "/"
    val SESSIONTIMEOUT = 2000
    var isConnected = false
    val watchConnection: Watcher = new Watcher() {
      override def process(event: WatchedEvent) {
        lock.synchronized {
          event.getState() match {
            case KeeperState.SyncConnected => isConnected = true
            case KeeperState.Disconnected => isConnected = false
            case _ =>
          }
          lock.notify()
        }
      }
    }
    zk = new ZooKeeper(host, SESSIONTIMEOUT, watchConnection)
    lock.synchronized {
      try {
        while (!isConnected) { // let watcher be triggered on slow computers
          lock.wait()
        }
        while(!zk.getState.isConnected()) {
          Thread.sleep(10)
        }
      } catch {
        case _: InterruptedException => // ignore InterruptedException
      }
    }
  }

  def closeServer {
    standaloneServerFactory.shutdown()
    zks.shutdown()
  }

  def closeClient {
    zk.close()
  }

  def getResults(opResults:java.util.List[OpResult]) = {
    opResults.map(x => {
      x match {
        case opResult:OpResult.CreateResult => opResult.getPath()
        case opResult:OpResult.DeleteResult => ()
        case opResult:OpResult.SetDataResult => opResult.getStat()
        case opResult:OpResult.CheckResult => ()
        case opResult:OpResult.ErrorResult => opResult.getErr()
        case _ =>
      }
    }).toList
  }

  "(Create /a/SetData/Check 1/Delete)" should "be (/a,Stat,(),())" in{
    startServer
    startClient
    val tr:Transaction = zk.transaction()
    val name = "/a"
    tr.create(name, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)
    tr.setData(name, "datatest".getBytes, -1)
    tr.check(name, 1) //vesion check
    tr.delete(name, -1)
    val res = tr.commit()
    val res2 = getResults(res)
    assert(res2(0) == "/a" && res2(1).isInstanceOf[Stat] && res2(2).isInstanceOf[Unit] && res2(3).isInstanceOf[Unit])
    Log.log(List(("actualResult", res2)))
    closeClient
    closeServer
  }

  "(Create /a/SetData/Check 2/Delete)" should "throw a BadVersionException" in{
    startServer
    startClient
    val tr:Transaction = zk.transaction()
    val name = "/a"
    tr.create(name, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)
    tr.setData(name, "datatest".getBytes, -1)
    tr.check(name, 2) //vesion check
    tr.delete(name, -1)
    a [KeeperException.BadVersionException] should be thrownBy {
      val res = tr.commit()
    }
    closeClient
    closeServer
  }

  "(Create /a/Create /a)" should "throw a NodeExistsException for /a" in{
    startServer
    startClient
    val tr:Transaction = zk.transaction()
    val name = "/a"
    tr.create(name, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)
    tr.create(name, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)
    val thrown = the [KeeperException.NodeExistsException] thrownBy tr.commit()
    thrown.getPath() should equal (name)
    closeClient
    closeServer
  }

  "(SetData /a)" should "throw a NoNodeException for /a" in{
    startServer
    startClient
    val tr:Transaction = zk.transaction()
    val name = "/a"
    tr.setData(name, "datatest".getBytes, -1)
    val thrown = the [KeeperException.NoNodeException] thrownBy tr.commit()
    thrown.getPath() should equal (name)
    closeClient
    closeServer
  }

  "SetData /a" should "throw a NoNodeException for /a" in{
    startServer
    startClient
    val tr:Transaction = zk.transaction()
    val name = "/a"
    tr.setData(name, "datatest".getBytes, -1)
    val thrown = the [KeeperException.NoNodeException] thrownBy tr.commit()
    thrown.getPath() should equal (name)
    closeClient
    closeServer
  }

  "Create /a/Create /a" should "throw a NodeExistsException for /a" in{
    startServer
    startClient
    val name = "/a"
    zk.create(name, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)
    val thrown = the [KeeperException.NodeExistsException] thrownBy {
      zk.create(name, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)
    }
    thrown.getPath() should equal (name)
    closeClient
    closeServer
  }

  "Create /a/(Create /b/Create /b)/getChildren /" should "return (/a)" in{
    startServer
    startClient
    zk.create("/a", null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)
    val tr:Transaction = zk.transaction()
    val name = "/b"
    tr.create(name, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)
    tr.create(name, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)
    a [KeeperException.NodeExistsException] should be thrownBy {
      val res = tr.commit()
    }
    val res2 = zk.getChildren("/", null)
    Log.log(List(("actualResult", res2)))
    closeClient
    closeServer
  }
}
