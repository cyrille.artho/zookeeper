package modbat

import sys.process._
import modbat.dsl._

import org.apache.zookeeper.Watcher.Event.{KeeperState, EventType}
import org.apache.zookeeper.{Watcher, WatchedEvent, ZooKeeper, ZooDefs}
import events.Log

class ZKWatcher(zk: ZooKeeper) extends Model {
 var event: WatchedEvent = null

 val watchConnection: Watcher  = new Watcher() {
    //zk.register(watchConnection)
   override def process(event: WatchedEvent) {
     val state = event.getState()
     Log.log(List(("state", state), ("eventType", event.getType()),("path", event.getPath())), "ZKWatcher")
     val watcher = new ZKWatcher(zk)
    }
  }

  zk.register(watchConnection)

      /*"connected" -> "connected",
      "connected" -> "nodeChildrenChanged" := {
	require(event == NodeChildrenChanged)
      }
      "connected" -> "nodeCreated" := {
	require(event == NodeCreated)

      }
      "connected" -> "nodeDataChanged" := {
	require(event == NodeDataChanged)

      }
      "connected" -> "nodeDeleted" := {
	require(event == NodeDeleted)

      }
      "connected" -> "none" := {
	require(event == None)

      }
      Set("nodeChildrenChanged", "nodeCreated", "nodeDataChanged", "nodeDeleted", "none") -> "connected" := {
	zk.register(watchEvent)
      }
      "connected" -> "end"*/
      /*"connected" -> "end" := {
	if (event != null)
	  Console.out.println(event.getType())
	val watcher = new ZKWatcher(zk)
	launch(watcher)
      }*/
}
