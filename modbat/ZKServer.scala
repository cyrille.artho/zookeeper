package modbat

import scala.sys.process.ProcessLogger
import sys.process._
import modbat.dsl._

import java.io.{File, IOException}
import java.net.InetSocketAddress

import events.ZKEvent
import scala.collection.mutable.ListBuffer
import org.apache.zookeeper.server.{ServerConfig, ZooKeeperServer, ServerCnxnFactory}
import scala.io.Source

object ZKServer {
  val CLIENTS = Source.fromFile("./settings.log").getLines.toList(3).toInt
}

class ZKServer extends Model {
  val sessions = new ListBuffer[ZKClient]
  val syncEvents = new ListBuffer[ZKEvent]

  //Config server
  val numConnections = 5000
  var tickTime = 500
  val dataDirectory = "./temp"
  var zkPort: Integer = null
  var standaloneServerFactory: ServerCnxnFactory = null
  var zkServer: ZooKeeperServer = null
  var nClients = 0 // the number of launched clients
  var nAliveClients = 0 // the number of clients that is still running.
  var clientId = 0
  var minSessionTimeout = -1
  var maxSessionTimeout = -1

  //local variables and checking
  var serverIsConnected = false

  @After
  def cleanUp {
    shutdownServer
    val temp = new File(dataDirectory)
    // if (!hasFailed)
    deleteAll(temp)
  }

  def deleteAll(folder: File) {
    var files = folder.listFiles()
    if(files != null) {
      for(f <- files) {
        if(f.isDirectory()) {
          deleteAll(f)
        } else {
          f.delete()
        }
      }
    }
    folder.delete()
  }

  def startServer {
    var dir = new File(dataDirectory, "zookeeper").getAbsoluteFile()
    zkServer = new ZooKeeperServer(dir, dir, tickTime)
    standaloneServerFactory = ServerCnxnFactory.createFactory(0, numConnections)
    zkPort = standaloneServerFactory.getLocalPort()
    standaloneServerFactory.startup(zkServer)
    serverIsConnected = true
  }

  def testGet { // test various getter functions
    // we want to make sure that we have at least some simple sanity checks
    // for the return values.
    choose(
      { () => assert(zkServer.isRunning) },
      { () =>
        val id = zkServer.getLastProcessedZxid() // TODO: add oracle
        val numEvents = sessions.map(_.events.size).sum
        if (id == 0) {
          assert(numEvents == 0)
        } else {
          assert(id > 0, { "id = " + id + ", numEvents = " + numEvents })
          // maybe we can only check if the ID is > 0 once a client has done something
        }
      },
      { () => assert(zkServer.getTickTime() == tickTime)},
      { () =>
        if (minSessionTimeout == -1){
          assert(zkServer.getMinSessionTimeout() >= 0)
        }else{
          assert(zkServer.getMinSessionTimeout() == minSessionTimeout)
        }
	// later: add check min <= max
      },
      { () =>
        if (maxSessionTimeout == -1){
          assert(zkServer.getMaxSessionTimeout() >= 0)
        }else{
          assert(zkServer.getMaxSessionTimeout() == maxSessionTimeout)
        }
      },
      { () => assert(zkServer.getState() == "standalone") },
      { () => assert(zkServer.getNumAliveConnections() == nAliveClients) }
    )
  }

  def testSet { // test various setter functions
    choose(
      { () =>
        tickTime = choose(500, 1000)
        zkServer.setTickTime(tickTime)
      },
      { () =>
        minSessionTimeout = choose(-1, 1000)
        zkServer.setMinSessionTimeout(minSessionTimeout)
      },
      { () =>
        maxSessionTimeout = choose(-1, 1000)
        zkServer.setMaxSessionTimeout(maxSessionTimeout)
      }
    )
  }

  def shutdownServer {
    standaloneServerFactory.shutdown()
    serverIsConnected = false
  }

  def startClient {
    Console.out.println("###launch client")
    val client = new ZKClient(this, zkPort, clientId)
    launch(client)
    nClients += 1
    nAliveClients += 1
    clientId += 1
  }

  "init" -> "serverStarted" := startServer
  "serverStarted" -> "startClient" := {
    require(nClients < ZKServer.CLIENTS)
  }
  "startClient" -> "serverStarted" := {
    startClient weight 2
  }
/**  "serverStarted" -> "serverStarted" := testGet*/
// "serverStarted" -> "serverStarted" := testSet
  "serverStarted" -> "stopped" := {
    require(nClients >= ZKServer.CLIENTS)
  }
}
