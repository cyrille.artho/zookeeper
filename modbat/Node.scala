package modbat

import scala.collection.mutable.{HashMap, LinkedHashMap, ListBuffer}
import org.apache.zookeeper.ZooDefs.{Ids, Perms}
import org.apache.zookeeper.data.{Id, ACL}
import scala.collection.JavaConversions._
import events.{Log, ZKEvent}

/** Implements node data for a given node and links node with its parent/children */
object Node {
  def isParent(name1:String, name2:String) : Boolean = {
    // Is name1 a parent of name2?
    if (name1 == "/") {
      name2.startsWith(name1) && name2.lastIndexOf("/") == 0 && name2 != "/"
    } else {
      (name2.startsWith(name1) && name2.lastIndexOf("/") == name1.length)
    }
  }

  def isAncestor(name1:String, name2:String) : Boolean = {
    // is name1 an ancestor of name2?
    name2.startsWith(name1) && name1 != name2
  }
}

class Node(val name: String,
  val children: LinkedHashMap[String, Node] = new LinkedHashMap[String, Node](),
  val acl: ACL = new ACL(Perms.ALL, Ids.ANYONE_ID_UNSAFE)
) {
  var data: Option[Seq[Byte]] = None
  var acls = new LinkedHashMap[Id, Int]
  val data_watches = new ListBuffer[ZKEvent]
  val child_watches = new ListBuffer[ZKEvent]
  acls += (acl.getId() -> acl.getPerms())

  override def equals(that:Any) = {
    if ((that != null) && (that.isInstanceOf[Node])) {
      val other = that.asInstanceOf[Node]
      (children.equals(other.children) && acl.equals(other.acl)
        && data.equals(other.data) && acls.equals(other.acls)
        && data_watches.equals(other.data_watches) && child_watches.equals(other.child_watches))
    } else {
      false
    }
  }

  override def hashCode() = {
    children.hashCode() ^ acl.hashCode() ^ data.hashCode() ^ acls.hashCode()
  }

  def createChild(childName: String) = {
    val childNode = new Node(childName)
    children.put(childName, childNode)
    childNode
  }

  def getChildren = children.keySet.toSet

  def getChild(childName: String) = {
    // Log.log(List(("children", children.mkString(", "))), "getChild")
    children.get(childName)
  }

  def setACL(aclList:ListBuffer[ACL]) ={
    var acls2 = new LinkedHashMap[Id, Int]
    for (acl <- aclList) {acls2 += (acl.getId() -> acl.getPerms())}
    acls = acls2.clone
  }

  def getACL:java.util.List[ACL] = {
    val aclBuffer = new ListBuffer[(Id, Int)]()
    acls.copyToBuffer(aclBuffer)
    aclBuffer.map(t => new ACL(t._2, t._1))
  }

  override def toString = {
    "Node \"" + name + "\", acls = {"+acls+"}children = {" + children.mkString("\n") + "} data_watches {"+data_watches+"} child_watches {"+child_watches+"}"
  }

  override def clone = {
    val n = new Node(name)
    for (child <- children) {
      n.children.put(child._1, child._2.clone.asInstanceOf[Node])
    }
    n.data = data // no clone needed because Seq is read-only?
    // in any case data is never manipulated but only set or read
    n.acls = acls.clone
    for (data_watch <- data_watches) {
      n.data_watches += data_watch
    }
    for (child_watch <- child_watches) {
      n.child_watches += child_watch
    }
    n
  }
}
