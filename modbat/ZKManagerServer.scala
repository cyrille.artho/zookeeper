package modbat

import sys.process._
import modbat.dsl._

import org.apache.zookeeper.server.quorum.QuorumPeer.QuorumServer

import java.util.HashMap

object ZKManagerServer {
  val peers = new HashMap[Long, QuorumServer]
}

class ZKManagerServer extends Model {
    var zkQuorumServer: ZKQuorumServer = null
    val LIMITNBSERVER = 10
    var iteratorPort: Int = 1

  def startLeader() = {
   } 

  def startNewQuorumServer() = {
    if (iteratorPort < LIMITNBSERVER) {
      //val quorumServer = new ZKQuorumServer(iteratorPort)
      val quorumServer: ZKQuorumServer = null
      launch(quorumServer)
      iteratorPort += 1
      quorumServer
    }
  }

  "init" -> "leaderStarted" := startLeader
  "leaderStarted" -> "leaderStarted" := {
    //zkQuorumServer = startNewQuorumServer
  } weight 10
  "leaderStarted" -> "quit"
}
