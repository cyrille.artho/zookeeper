package modbat

import org.apache.zookeeper.{Watcher, WatchedEvent, ZooKeeper, ZooDefs, CreateMode, ClientCnxn, Transaction, Op}
import org.apache.zookeeper.Watcher.Event.{KeeperState, EventType}
import org.apache.zookeeper.data.{Stat, ACL, Id}
import org.apache.zookeeper.server.ZooKeeperServer
import org.apache.zookeeper.server.auth.DigestAuthenticationProvider
import org.apache.zookeeper.ZooKeeper.States
import org.apache.zookeeper.AsyncCallback.{StringCallback, DataCallback, VoidCallback, StatCallback, ChildrenCallback, ACLCallback, MultiCallback}
import org.apache.zookeeper.ZooDefs.{Ids, Perms}
import org.apache.zookeeper.KeeperException
import org.apache.zookeeper.KeeperException.Code
import org.apache.zookeeper.KeeperException.Code._

import java.io.{FileOutputStream, FileInputStream, BufferedOutputStream, BufferedReader, FileReader, OutputStreamWriter, File}
import java.util.{ArrayList, Random, Arrays}
import java.util.Collections.emptySet
import scala.collection.mutable.{HashSet, HashMap, LinkedHashSet, ListBuffer, MultiMap}
import scala.runtime.RichByte
import scala.collection.JavaConversions._
import scala.io.Source
import events.{ZKEvent, ZKException, OpResultList, ZKSessionGraph, ZKSearch, Log}
import events.ZKEvent
import events.ZKException
import events.{Create,Delete,Exists,GetChildren,GetData,SetData,GetACL,SetACL,Check,Sync,CloseSession}

import modbat.dsl._

object ZKClient {
  val API_CALLS = Source.fromFile("./settings.log").getLines.toList(2).toInt
  val CB_DELAY = Source.fromFile("./settings.log").getLines.toList(4).toInt
}

class ZKClient(server: ZKServer, zkPort: Integer, clientId:Integer) extends Model {
  val lock = new Object
  var received = true
  val host = "127.0.0.1:" + zkPort + "/"
  var isConnected = false
  val SESSIONTIMEOUT = 2000
  var nbCallbacksWaited = 0
  val events = new ListBuffer[ZKEvent]
  val trEvents = new ListBuffer[ZKEvent]
  val results = new HashMap[ZKEvent, (Option[Any], Option[Throwable])]
  var trExc:Throwable = null
  var currentNode = "" // used between transitions for sync. action + catches
  server.sessions += this
  val nodes = new LinkedHashSet[String]
  nodes += "/"
  val watchers = new HashMap[ZKEvent, Watcher]() // watchers that can be called
  val calledWatchers = new HashSet[ZKEvent]() // watchers that has been called before(watcher can be called more than once when the state of session changed)
  var nCalled = 0
  val beforeFinish = new ListBuffer[Function0[Any]]() // functions that is executed before finishing the session(this is for watcher test)
  val watchConnection: Watcher = new Watcher() {
    override def process(event: WatchedEvent) {
      lock.synchronized {
        event.getState() match {
          case KeeperState.SyncConnected => isConnected = true
          case KeeperState.Disconnected => isConnected = false
          case _ =>
        }
        lock.notify()
      }
    }
  }

  val zk = new ZooKeeper(host, SESSIONTIMEOUT, watchConnection)
  val clientName = "client-" + clientId
  val clientPwd = "clientPwd"
  val clientDigestId = new Id("digest", DigestAuthenticationProvider.generateDigest(clientName + ":" + clientPwd))
  val zkIDs = List(clientDigestId, Ids.ANYONE_ID_UNSAFE)
  var tr:Transaction = null
  zk.addAuthInfo("digest", (clientName + ":" + clientPwd).getBytes());

  lock.synchronized {
    try {
      while (!isConnected) { // let watcher be triggered on slow computers
        lock.wait()
      }
      while(!zk.getState.isConnected()) {
        Thread.sleep(10)
      }
    } catch {
      case _: InterruptedException => // ignore InterruptedException
    }
  }
  assert(isConnected)

  def finish {
    beforeFinish.foreach(_()) // execute the functions

    if (!watchers.foldLeft(true)((isIn, e) => isIn && calledWatchers.contains(e._1))){
      // if there is a watcher that has not been triggered yet, wait for a while
      Thread.sleep(500)
    }

    for((event, watcher) <- watchers) {
      // check if there is a path such that the watcher is not triggered.
      if (!calledWatchers.contains(event)) {
        watcherTrigger(event)
      }
    }

    zk.close() // TODO: Many side effects (see documentation)
    // assert (server.zkServer.getZKDatabase().getDataTree().getWatchCount() == 0)
    // ZOOKEEPER-2358: Watch count should revert to 0 after client closes conn.
    lock.synchronized {
      server.nAliveClients -= 1
    }
  }

  @After
  def cleanUp {
    zk.close()
  }

  def sync(event: ZKEvent) = {
    server.syncEvents += event
    event.isSync = true
    event
  }

  def addEvent(event: ZKEvent) = {
    events += event
    event
  }

  def addTrEvent(event: ZKEvent) = {
    trEvents += event
  }

  def addResult(e: ZKEvent, result: (Option[Any], Option[Throwable])) = {
    if (watchers.isDefinedAt(e)) {
      result match {
        case (None, Some(_)) =>
          // if the event fails with exception, the watcher is not set. So remove it from watchers.
          watchers.remove(e)
        case _ =>
      }
    }

    e.timestamp_finish = System.currentTimeMillis
    results += e -> result
    result
  }

  def chooseNodeType = {
    if (choose()) { CreateMode.PERSISTENT }
    else { CreateMode.EPHEMERAL }
  }

  def wrapResult(result : Any) : Option[Any] = {
    if (result == null) {
      None
    } else {
      Some(result)
    }
  }

  // upToNode is not safe because delete only works if a node has no children,
  // the existence/absenced of a node may also depend on its children
  def outcome(result: Any, exc: Option[Throwable]) = {
    val event = events.last
    val sessionGraph = server.sessions.map((s: ZKClient) => s.events.toList).toList
    // Log.log(List(("events", events), ("sessionGraph", sessionGraph)), "outcome")
    val sessionResults = server.sessions.foldLeft(new HashMap[ZKEvent, (Option[Any], Option[Throwable])])((m, s) => m ++ s.results).toMap
    val sg = new ZKSessionGraph(server.syncEvents.toList, event, sessionGraph, sessionResults)
    ZKSearch.analyze(sg, wrapResult(result), exc)
  }

  def asyncOutcome(target: ZKEvent, result: Any, exc: Option[Throwable]) = {
    if (result == null) {
      addResult(target, (None, exc))
    } else {
      addResult(target, (Some(result), None))
    }
    val sessionGraph = server.sessions.map((s: ZKClient) => s.events.toList).toList
    val sessionResults = server.sessions.foldLeft(new HashMap[ZKEvent, (Option[Any], Option[Throwable])])((m, s) => m ++ s.results).toMap
    // Log.log(List(("events", events), ("sessionGraph", sessionGraph)), "asyncOutcome")
    val sg = new ZKSessionGraph(server.syncEvents.toList, target, sessionGraph, sessionResults)
    ZKSearch.analyze(sg, wrapResult(result), exc)
  }

  def watcherOutcome(target: ZKEvent, result:Any, exc: Option[Throwable]) = {
    val sessionGraph = server.sessions.map((s: ZKClient) => s.events.toList).toList
    val sessionResults = server.sessions.foldLeft(new HashMap[ZKEvent, (Option[Any], Option[Throwable])])((m, s) => m ++ s.results).toMap
    // TODO : addResult for watcher
    target.timestamp_watcher_triggered = System.currentTimeMillis
    Log.log(List(("target", target), ("sessionResults", results), ("result", result), ("exc", exc)), "watcherOutcome")
    val sg = new ZKSessionGraph(server.syncEvents.toList, target, sessionGraph, sessionResults)
    val minSessionTimeout = server.zkServer.getMinSessionTimeout()
    val maxSessionTimeout = server.zkServer.getMaxSessionTimeout()
    ZKSearch.watcherAnalyze(sg, wrapResult(result), exc, minSessionTimeout, maxSessionTimeout)
  }

  def watcherTrigger(target: ZKEvent) {
    val sessionGraph = server.sessions.map((s: ZKClient) => s.events.toList).toList
    val sessionResults = server.sessions.foldLeft(new HashMap[ZKEvent, (Option[Any], Option[Throwable])])((m, s) => m ++ s.results).toMap
    // TODO : addResult for watcher
    Log.log(List(("target", target), ("sessionResults", results), ("sessionGraph", sessionGraph)), "watcherOutcome")
    println("sessionGraph" + sessionGraph)
    val sg = new ZKSessionGraph(server.syncEvents.toList, target, sessionGraph, sessionResults)
    val minSessionTimeout = server.zkServer.getMinSessionTimeout()
    val maxSessionTimeout = server.zkServer.getMaxSessionTimeout()
    ZKSearch.watcherTriggerAnalyze(sg, minSessionTimeout, maxSessionTimeout)
  }

  def checkResult(actualResult: Any) {
    Log.log(List(("currentNode", currentNode)), "checkResult")
    outcome(actualResult, None)
  }

  def checkExc(e: ZKException) {
    lock.synchronized {
      Log.log(List(("currentNode", currentNode)), "checkExc")
      outcome(null, Some(e))
    }
  }

  class CheckDeleteCallback(val target: ZKEvent) extends VoidCallback {
    override def processResult(rc: Int, path: String, ctx: Object) {
      lock.synchronized {
        // received = true
        // lock.notify()
        val returnedCode = Code.get(rc)
        var e: ZKException = null

        returnedCode match {
          case OK => asyncOutcome(target, (), None)
          case NONODE => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.NONODE, target.name)))
          case NOTEMPTY => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.NOTEMPTY, target.name)))
          case BADARGUMENTS => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.BADARGUMENTS, target.name)))
          case NOAUTH => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.NOAUTH, target.name)))
          case CONNECTIONLOSS => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.CONNECTIONLOSS, null)))
          case _ => assert(false, "An abnormal rc code has been returned "+returnedCode)
        }
      }
    }
  }

  class CheckExistsCallback(val target: ZKEvent) extends StatCallback {
    override def processResult(rc: Int, path: String, ctx: Object, stat: Stat) {
      lock.synchronized {
        // received = true
        // lock.notify()
        val returnedCode = Code.get(rc)
        returnedCode match {
          case OK => {
            if(stat != null) {
              asyncOutcome(target, true, None)
            }
          }
          case NONODE => {
            asyncOutcome(target, false, None)
          }
          // unlike the synchronous version that returns false when no node
          // is present, the asynchronous version throws an exception
          case CONNECTIONLOSS => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.CONNECTIONLOSS, null)))
          case _ => assert(false, "An abnormal rc code has been returned "+returnedCode)
        }
      }
    }
  }

  class CheckExistsWatcher(val target: ZKEvent) extends Watcher {
    override def process(event: WatchedEvent) {
      lock.synchronized {
        // received = true
        // lock.notify()
        Log.log(List(("target", target), ("event", event), ("sessionEvents", server.sessions.map((s: ZKClient) => s.events.toList).toList)),
          "CheckExistsWatcher")
        assert(watchers.isDefinedAt(target), "watchers "+watchers+", target "+ target+", this watcher "+this)
        calledWatchers += target
        if(event.getType() == EventType.None){
          //Zookeeper Session's state has changed
          event.getState() match {
            case KeeperState.SyncConnected => // watcherOutcome(target, event, None)
            case _ : KeeperState => watcherOutcome(target, event, None)
          }
        }else{
          watcherOutcome(target, event, None)
          watchers -= target
          Log.log(List(("target",target), ("this", this), ("watchers", watchers)), "removeBinding")
        }
      }
    }
  }

  class CheckGetDataWatcher(val target: ZKEvent) extends Watcher {
    override def process(event: WatchedEvent) {
      lock.synchronized {
        // received = true
        // lock.notify()
        Log.log(List(("target", target), ("event", event), ("sessionEvents", server.sessions.map((s: ZKClient) => s.events.toList).toList)),
          "CheckGetDataWatcher")
        println("#######watcher is called "+target)
        assert(watchers.isDefinedAt(target), "watchers "+watchers+", target "+ target+", this watcher "+this)
        calledWatchers += target
        if(event.getType() == EventType.None){
          //Zookeeper Session's state has changed
          event.getState() match {
            case KeeperState.SyncConnected =>
            case _ => watcherOutcome(target, event, None)
          }
        }else{
          watcherOutcome(target, event, None)
          watchers -= target
          Log.log(List(("target",target), ("this", this), ("watchers", watchers)), "removeBinding")
        }
      }
    }
  }

  class CheckGetChildrenWatcher(val target: ZKEvent) extends Watcher {
    override def process(event: WatchedEvent) {
      lock.synchronized {
        // received = true
        // lock.notify()
        Log.log(List(("target", target), ("event", event), ("sessionEvents", server.sessions.map((s: ZKClient) => s.events.toList).toList)),
          "CheckGetChildrenWatcher")

        assert(watchers.isDefinedAt(target), "watchers "+watchers+", target "+ target+", this watcher "+this)
        calledWatchers += target
        if(event.getType() == EventType.None){
          //Zookeeper Session's state has changed
          event.getState() match {
            case KeeperState.SyncConnected => //TODO
            case _ => watcherOutcome(target, event, None)
          }
        }else{
          watcherOutcome(target, event, None)
          watchers -= target
          Log.log(List(("target",target), ("this", this), ("watchers", watchers)), "removeBinding")
        }
      }
    }
  }

  class CheckChildrenCallback(val target: ZKEvent) extends ChildrenCallback {
    override def processResult(rc: Int, path: String, ctx: Object, children: java.util.List[String]) {
      lock.synchronized {
        // received = true
        // lock.notify()
        val returnedCode = Code.get(rc)
        var e: ZKException = null
        returnedCode match {
          case OK => asyncOutcome(target, children.toSet, None)
          case NONODE => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.NONODE, target.name)))
          case NOAUTH => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.NOAUTH, target.name)))
          case CONNECTIONLOSS => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.CONNECTIONLOSS, null)))
          case _ => assert(false, "An abnormal rc code has been returned "+returnedCode)
        }
      }
    }
  }

  class CheckCreateCallback(val target: ZKEvent) extends StringCallback {
    override def processResult(rc: Int, path: String, ctx: Object, name: String) {
      lock.synchronized {
        // received = true
        // lock.notify()
        val returnedCode = Code.get(rc)
        var e: ZKException = null

        returnedCode match {
          case OK => asyncOutcome(target, path, None)
          case NONODE => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.NONODE, target.name)))
          case NODEEXISTS => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.NODEEXISTS, target.name)))
          case NOAUTH => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.NOAUTH, target.name)))
          case CONNECTIONLOSS => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.CONNECTIONLOSS, null)))
          case _ => assert(false, "An abnormal rc code has been returned "+returnedCode)
        }
      }
    }
  }

  class CheckSetDataCallback(val target: ZKEvent) extends StatCallback {
    override def processResult(rc: Int, path: String, ctx: Object, stat: Stat) {
      lock.synchronized {
        // received = true
        // lock.notify()
        val returnedCode = Code.get(rc)
        var e: ZKException = null
        returnedCode match {
          case OK => asyncOutcome(target, (), None)
          case NONODE => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.NONODE, target.name)))
          case NOAUTH => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.NOAUTH, target.name)))
          case CONNECTIONLOSS => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.CONNECTIONLOSS, null)))
          case _ => assert(false, "An abnormal rc code has been returned "+returnedCode)
        }
      }
    }
  }

  def wrapData(data: Array[Byte]) = {
    if (data == null) {
      None
    } else {
      Some(data.toSeq)
    }
  }

  class CheckGetDataCallback(val target: ZKEvent) extends DataCallback {
    override def processResult(rc: Int, path: String, ctx: Object, data: Array[Byte], stat: Stat) {
      lock.synchronized {
        // received = true
        // lock.notify()
        val returnedCode = Code.get(rc)
        var e: ZKException = null
        returnedCode match {
          case OK => asyncOutcome(target, wrapData(data), None)
          case NONODE => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.NONODE, target.name)))
          case CONNECTIONLOSS => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.CONNECTIONLOSS, null)))
          case NOAUTH => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.NOAUTH, target.name)))
          case _ => assert(false, "An abnormal rc code has been returned "+returnedCode)
        }
      }
    }
  }

  class CheckSetACLCallback(val target: ZKEvent) extends StatCallback {
    override def processResult(rc: Int, path: String, ctx: Object, stat: Stat) {
      lock.synchronized {
        // received = true
        // lock.notify()
        val returnedCode = Code.get(rc)
        returnedCode match {
          case OK => asyncOutcome(target, (), None)
          case NONODE => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.NONODE, target.name)))
          case NOAUTH => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.NOAUTH, target.name)))
          case CONNECTIONLOSS => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.CONNECTIONLOSS, null)))
          case _ => assert(false, "An abnormal rc code has been returned "+returnedCode)
        }
      }
    }
  }

  class CheckGetACLCallback(val target: ZKEvent) extends ACLCallback {
    override def processResult(rc: Int, path: String, ctx: Object, acl: java.util.List[ACL], stat: Stat) {
      lock.synchronized {
        // received = true
        // lock.notify()
        val returnedCode = Code.get(rc)
        returnedCode match {
          case OK => asyncOutcome(target, acl, None)
          case NONODE => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.NONODE, target.name)))
          case CONNECTIONLOSS => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.CONNECTIONLOSS, null)))
          case _ => assert(false, "An abnormal rc code has been returned "+returnedCode)
        }
      }
    }
  }

  class CheckMultiCallback(val target: ZKEvent) extends MultiCallback {
    override def processResult(rc: Int, path: String, ctx: Object, opResults: java.util.List[org.apache.zookeeper.OpResult]) {
      lock.synchronized {
        // received = true
        // lock.notify()
        val returnedCode = Code.get(rc)
        val orl = OpResultList(opResults.toList)
        // Log.log(List("path", path), "Multi Callback")
        returnedCode match {
          case OK => asyncOutcome(target, orl, None)
          case NODEEXISTS => asyncOutcome(target, orl, Some(ZKException.create(KeeperException.Code.NODEEXISTS, null)))
          case NOAUTH => asyncOutcome(target, orl, Some(ZKException.create(KeeperException.Code.NOAUTH, null)))
          case BADARGUMENTS => asyncOutcome(target, orl, Some(ZKException.create(KeeperException.Code.BADARGUMENTS, null)))
          case NONODE => asyncOutcome(target, orl, Some(ZKException.create(KeeperException.Code.NONODE, null)))
          case CONNECTIONLOSS => asyncOutcome(target, null, Some(ZKException.create(KeeperException.Code.CONNECTIONLOSS, null)))
          case _ => assert(false, "An abnormal rc code has been returned "+returnedCode)
        }
      }
    }
  }

  def addChild {
    var node = nodes.toArray.apply(choose(0, nodes.size))
    if (!node.equals("/")) {
      node = node + "/"
    }
    node = node + ('a' + choose(0, 5)).asInstanceOf[Char] // 5 = range of child nodes ('a'...'e')
    nodes += node
  }

  def chooseNode:String  = {
    maybe (addChild)
    val node = nodes.toArray.apply(choose(0, nodes.size))
    node
  }

  def chooseACL(id : Id):ACL = { //create Random ACL
    val permsList = List(Perms.ADMIN, Perms.CREATE, Perms.DELETE, Perms.READ, Perms.WRITE)
    val perms = permsList.foldLeft(0)((x, y) => if(choose(0, 2) == 0) x else x | y)
    var acl = new ACL()
    acl.setPerms(perms)
    acl.setId(id)
    acl
  }


  def chooseACLs : ListBuffer[ACL] = {
    val idList = List(Ids.ANYONE_ID_UNSAFE) :::
      (0 to (server.clientId - 1)).toList.map(num => new Id("digest", DigestAuthenticationProvider.generateDigest("client-" + num + ":" + clientPwd)))
    val acls = new ListBuffer[ACL]()
    for(id <- idList) {
      if (choose(0,2) == 0) { // choose to add 'id''s acl
        acls += chooseACL(id)
      }
    }
    acls
  }

  def getOps: ArrayList[Op] = {
    //gets the list of Op from Transaction's private field "ops"
    val c = tr.getClass
    val f = c.getDeclaredField("ops")
    f.setAccessible(true)
    val ops:ArrayList[Op] = f.get(tr).asInstanceOf[ArrayList[Op]]
    ops
  }

  // sync. create
  "connected" -> "counting" := {
    lock.synchronized {
      try {
        // while(!received){
        //   lock.wait()
        // }
        currentNode = chooseNode
        val e = addEvent(sync(ZKEvent.create(currentNode, Create, zkIDs, clientName)))
        val res = zk.create(currentNode, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)
        addResult(e, (Some(res), None))
        checkResult(res)
      } catch {
        case _: InterruptedException => // ignore InterruptedException
      }
    }
  } label "sync. create" catches ("NodeExistsException" -> "checkExists", "NoNodeException" -> "checkNotExists", "ConnectionLossException" -> "connectionLoss", "NoAuthException" -> "checkNoAuth")

  "checkExists" -> "counting" := {
    val exc = trExc match {
      case e:KeeperException.NodeExistsException => ZKException.create(KeeperException.Code.NODEEXISTS, e.getPath())
      case _ => ZKException.create(KeeperException.Code.NODEEXISTS, currentNode)
    }
    addResult(events.last, (None, Some(exc)))
    checkExc(exc)
    trExc = null
  }

  //sync. delete
  "connected" -> "counting" := {
    lock.synchronized {
      try {
        // while(!received){
        //   lock.wait()
        // }
        currentNode = chooseNode
        val e = addEvent(sync(ZKEvent.create(currentNode, Delete, zkIDs, clientName)))
        val res = zk.delete(currentNode,-1)
        addResult(e, (Some(res), None))
        checkResult(res)
      } catch {
        case _: InterruptedException => // ignore InterruptedException
      }
    }
  } label "sync. delete" catches ("NoNodeException" -> "checkNotExists", "NotEmptyException" -> "checkNotEmpty", "BadArgumentsException" -> "checkBadArgs", "ConnectionLossException" -> "connectionLoss", "NoAuthException" -> "checkNoAuth")

  "checkNotExists" -> "counting" := {
    val exc = trExc match {
      case e:KeeperException.NoNodeException => ZKException.create(KeeperException.Code.NONODE, e.getPath())
      case _ => ZKException.create(KeeperException.Code.NONODE, currentNode)
    }
    addResult(events.last, (None, Some(exc)))
    checkExc(exc)
    trExc = null
  }

  "checkNotEmpty" -> "counting" := {
    val exc = trExc match {
      case e:KeeperException.NotEmptyException => ZKException.create(KeeperException.Code.NOTEMPTY, e.getPath())
      case _ => ZKException.create(KeeperException.Code.NOTEMPTY, currentNode)
    }
    addResult(events.last, (None, Some(exc)))
    checkExc(exc)
    trExc = null
  }

  "checkBadArgs" -> "counting" := {
    val exc = trExc match {
      case e:KeeperException.BadArgumentsException => ZKException.create(KeeperException.Code.BADARGUMENTS, e.getPath())
      case _ => ZKException.create(KeeperException.Code.BADARGUMENTS, currentNode)
    }
    addResult(events.last, (None, Some(exc)))
    checkExc(exc)
    trExc = null
  }

  "checkNoAuth" -> "counting" := {
    val exc = trExc match {
      case e:KeeperException.NoAuthException => ZKException.create(KeeperException.Code.NOAUTH, e.getPath())
      case _ => ZKException.create(KeeperException.Code.NOAUTH, currentNode)
    }
    addResult(events.last, (None, Some(exc)))
    checkExc(exc)
    trExc = null
  }

  "checkInvalidACL" -> "counting" := {
    val exc = trExc match {
      case e:KeeperException.NoNodeException => ZKException.create(KeeperException.Code.INVALIDACL, e.getPath())
      case _ => ZKException.create(KeeperException.Code.INVALIDACL, currentNode)
    }
    addResult(events.last, (None, Some(exc)))
    checkExc(exc)
    trExc = null
  }
  // probabilistically returns a new Watcher or null
  def existsWatcher(e: ZKEvent): Watcher = {
//    if (choose(0, 2) == 0) {
    val w = new CheckExistsWatcher(e)
    assert (! watchers.isDefinedAt(e))
    watchers += e -> w
    w
//    } else {
//      null
//    }
  }

  def getDataWatcher(e: ZKEvent): Watcher = {
//    if (choose(0, 2) == 0) {
    val w = new CheckGetDataWatcher(e)
    assert (! watchers.isDefinedAt(e))
    watchers += e -> w
    w
//    } else {
//      null
//    }
  }

    def getChildrenWatcher(e: ZKEvent): Watcher = {
//    if (choose(0, 2) == 0) {
      val w = new CheckGetChildrenWatcher(e)
      assert (! watchers.isDefinedAt(e))
      watchers += e -> w
      w
//    } else {
//      null
//    }
  }

  //sync. exists
  "connected" -> "counting" := {
    lock.synchronized {
      try {
        // while(!received){
        //   lock.wait()
        // }
        currentNode = chooseNode
        val e = addEvent(sync(ZKEvent.create(currentNode, Exists, zkIDs, clientName, true)))
        val res = (zk.exists(currentNode, existsWatcher(e)) != null)

        addResult(e, (Some(res), None))
        checkResult(res)
      } catch {
        case _: InterruptedException => // ignore InterruptedException
      }
    }
  } label "sync. exists" //Add a catch here for exist exception if needed

  //sync. getchildren
  "connected" -> "counting" := {
    lock.synchronized {
      try {
        // while(!received){
        //   lock.wait()
        // }
        currentNode = chooseNode
        val e = addEvent(sync(ZKEvent.create(currentNode, GetChildren, zkIDs, clientName, true)))
        val res = zk.getChildren(currentNode, getChildrenWatcher(e)).toSet
	Thread.sleep(ZKClient.CB_DELAY)

        addResult(e, (Some(res), None))
        checkResult(res)
      } catch {
        case _: InterruptedException => // ignore InterruptedException
      }
    }
  } label "sync. getChildren" catches ("NoNodeException" -> "checkNotExists", "ConnectionLossException" -> "connectionLoss", "NoAuthException" -> "checkNoAuth")

  //sync. setdata
  "connected" -> "counting" := {
    lock.synchronized {
      try {
        // while(!received){
        //   lock.wait()
        // }
        currentNode = chooseNode
        val e = addEvent(sync(ZKEvent.create(currentNode, SetData("datatest".getBytes), zkIDs, clientName)))
        val res = zk.setData(currentNode, "datatest".getBytes, -1)

        addResult(e, (Some(()), None))
        //TODO:"datatest" is the only possible data at thte moment as their is
        // no parameter in the model function.
      } catch {
        case _: InterruptedException => // ignore InterruptedException
      }
    }
  } label "sync. setData" catches ("NoNodeException" -> "checkNotExists", "ConnectionLossException" -> "connectionLoss", "NoAuthException" -> "checkNoAuth")

  //sync. getdata
  "connected" -> "counting" := {
    lock.synchronized {
      try {
        // while(!received){
        //   lock.wait()
        // }
        currentNode = chooseNode
        val e = addEvent(sync(ZKEvent.create(currentNode, GetData, zkIDs, clientName, true)))
        val res = ( zk.getData(currentNode, getDataWatcher(e), new Stat))
	    Thread.sleep(ZKClient.CB_DELAY)
        addResult(e, (Some(wrapData(res)), None))
        checkResult(wrapData(res))
      } catch {
        case _: InterruptedException => // ignore InterruptedException
      }
    }
  } label "sync. getData" catches ("NoNodeException" -> "checkNotExists", "ConnectionLossException" -> "connectionLoss", "NoAuthException" -> "checkNoAuth")

  //sync. getACL
  "connected" -> "counting" := {
    lock.synchronized {
      try {
        // while(!received){
        //   lock.wait()
        // }
        currentNode = chooseNode
        val e = addEvent(sync(ZKEvent.create(currentNode, GetACL, zkIDs, clientName)))
        val res = zk.getACL(currentNode, new Stat)

        addResult(e, (Some(res), None))
        checkResult(res)
      } catch {
        case _: InterruptedException => // ignore InterruptedException
      }
    }
  } label "sync. getACL" catches ("NoNodeException" -> "checkNotExists", "ConnectionLossException" -> "connectionLoss")

  //sync. setACL
  "connected" -> "counting" := {
    lock.synchronized {
      try {
        // while(!received){
        //   lock.wait()
        // }
        currentNode = chooseNode
        val acls = chooseACLs
        val e = addEvent(sync(ZKEvent.create(currentNode, SetACL(acls), zkIDs, clientName)))
        val res = zk.setACL(currentNode, acls, -1)

        addResult(e, (Some(()), None))
      } catch {
        case _: InterruptedException => // ignore InterruptedException
      }
    }
  } label "sync. setACL" catches ("NoNodeException" -> "checkNotExists", "ConnectionLossException" -> "connectionLoss", "NoAuthException" -> "checkNoAuth", "InvalidACLException" -> "checkInvalidACL")

  //async. create
  "connected" -> "counting" := {
    lock.synchronized {
      try {
        // while(!received){
        //   lock.wait()
        // }
        currentNode = chooseNode
        val target = ZKEvent.create(currentNode, Create, zkIDs, clientName)
        addEvent(target)
        // received=false
        zk.create(currentNode, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT, new CheckCreateCallback(target), null)
	Thread.sleep(ZKClient.CB_DELAY)
      } catch {
        case _: InterruptedException => // ignore InterruptedException
      }
    }
  }label "async. create"

  //async. delete
  "connected" -> "counting" := {
    lock.synchronized {
      try {
        // while(!received){
        //   lock.wait()
        // }
        currentNode = chooseNode
        val target = ZKEvent.create(currentNode, Delete, zkIDs, clientName)
        addEvent(target)
        // received=false
        zk.delete(currentNode, -1, new CheckDeleteCallback(target), null)
	Thread.sleep(ZKClient.CB_DELAY)
      } catch {
        case _: InterruptedException => // ignore InterruptedException
      }
    }
  }label "async. delete"

  //async. exists
  "connected" -> "counting" := {
    lock.synchronized {
      try {
        // while(!received){
        //   lock.wait()
        // }
        currentNode = chooseNode
        val target = ZKEvent.create(currentNode, Exists, zkIDs, clientName, true)
        addEvent(target)
        // received=false
        zk.exists(currentNode, false, new CheckExistsCallback(target), null)
	Thread.sleep(ZKClient.CB_DELAY)
      } catch {
        case _: InterruptedException => // ignore InterruptedException
      }
    }
  }label "async. exists"

  //async. getChildren
  "connected" -> "counting" := {
    lock.synchronized {
      try {
        // while(!received){
        //   lock.wait()
        // }
        currentNode = chooseNode
        val target = ZKEvent.create(currentNode, GetChildren, zkIDs, clientName, true)
        addEvent(target)
        // received=false
        zk.getChildren(currentNode, false, new CheckChildrenCallback(target), null)
	Thread.sleep(ZKClient.CB_DELAY)
      } catch {
        case _: InterruptedException => // ignore InterruptedException
      }
    }
  }label "async. getChildren"

  //async. setData
  "connected" -> "counting" := {
    lock.synchronized {
      try {
        // while(!received){
        //   lock.wait()
        // }
        currentNode = chooseNode
        val target = ZKEvent.create(currentNode, SetData("datatest".getBytes), zkIDs, clientName)
        addEvent(target)
        // received=false
        zk.setData(currentNode, "datatest".getBytes, -1, new CheckSetDataCallback(target), null)
	Thread.sleep(ZKClient.CB_DELAY)
        // TODO: Use randomized data
      } catch {
        case _: InterruptedException => // ignore InterruptedException
      }
    }
  }label "async. setData"

  //async. getData
  "connected" -> "counting" := {
    lock.synchronized {
      try {
        // while(!received){
        //   lock.wait()
        // }
        currentNode = chooseNode
        val target = ZKEvent.create(currentNode, GetData, zkIDs, clientName, true)
        addEvent(target)
        // received=false
        zk.getData(currentNode, false, new CheckGetDataCallback(target), null)
	Thread.sleep(ZKClient.CB_DELAY)
      } catch {
        case _: InterruptedException => // ignore InterruptedException
      }
    }
  }label "async. getData"

  //async. getACL
  "connected" -> "counting" := {
    lock.synchronized {
      try {
        // while(!received){
        //   lock.wait()
        // }
        currentNode = chooseNode
        val target = ZKEvent.create(currentNode, GetACL, zkIDs, clientName)
        addEvent(target)
        // received=false
        zk.getACL(currentNode, new Stat, new CheckGetACLCallback(target), null)
	Thread.sleep(ZKClient.CB_DELAY)
      } catch {
        case _: InterruptedException => // ignore InterruptedException
      }
    }
  } label "async. getACL"

  // close session
  "connected" -> "closing" := {
    val session_id = zk.getSessionId()
    val e = addEvent(ZKEvent.create("", CloseSession, zkIDs, clientName))
    server.zkServer.closeSession(session_id)
    addResult(e, (Some(null), None)) // dummy result
  }


  "counting" ->"counted" := {
    require(nCalled < ZKClient.API_CALLS)
  }

  "counted" -> "connected" := {
    nCalled += 1
  }

  "counting" -> "closing" := {
    require(nCalled >= ZKClient.API_CALLS)
  }

  /*
  //async. setACL
  "connected" -> "counting" := {
    lock.synchronized {
      try {
//        while(!received){
//          lock.wait()
//        }
        currentNode = chooseNode
        val acls = chooseACLs
        val target = ZKEvent.create(currentNode, SetACL(acls), zkIDs, clientName)
        addEvent(target)
        received=false
        zk.setACL(currentNode, acls, -1, new CheckSetACLCallback(target), null)
	Thread.sleep(ZKClient.CB_DELAY)
        Console.out.println("##### setACL "+ target)
      } catch {
        case _: InterruptedException => // ignore InterruptedException
      }
    }
  } label "async. setACL"
   */
  /*
  "connected" -> "transaction" := {
    tr = zk.transaction()
    // also maintain our own list as ops is private in Transaction
  }

  "transaction" -> "transaction" := {
    currentNode = chooseNode
    addTrEvent(ZKEvent.create(currentNode, Create, zkIDs))
    tr.create(currentNode, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)
  } label "transaction create"

  "transaction" -> "transaction" := {
    currentNode = chooseNode
    addTrEvent(ZKEvent.create(currentNode, Delete, zkIDs))
    tr.delete(currentNode, -1)
  } label "transaction delete"

  "transaction" -> "transaction" := {
    currentNode = chooseNode
    addTrEvent(ZKEvent.create(currentNode, Check, zkIDs))
    tr.check(currentNode, -1) // value -1 always passes this check
    //TODO: check the current vesion of currentNode
  } label "transaction check"

  "transaction" -> "transaction" := {
    currentNode = chooseNode
    addTrEvent(ZKEvent.create(currentNode, SetData("datatest".getBytes), zkIDs))
    tr.setData(currentNode, "datatest".getBytes, -1)
  } label "transaction setData"

  "transaction" -> "counting" := {
    try {
      val event = ZKEvent.transaction(zkIDs, trEvents.toList)
      var res:java.util.List[org.apache.zookeeper.OpResult] = null
      addEvent(sync(event))
      trEvents.clear()
      maybe {
        res = tr.commit()
      } or_else {
        //transaction commit using ZooKeeper.multi
        res = zk.multi(getOps)
      }
      addResult(event, (Some(OpResultList(res.toList)), None))
      checkResult(OpResultList(res.toList))
    } catch {
      case e:Throwable => {
        trExc = e
        throw e
      }
    }
  } label "sync. transaction commit" catches ("NodeExistsException" -> "checkExists", "NoNodeException" -> "checkNotExists", "ConnectionLossException" -> "connectionLoss", "NoAuthException" -> "checkNoAuth", "BadArgumentsException" -> "checkBadArgs")


  "transaction" -> "counting" := {
    try {
      val event = ZKEvent.transaction(zkIDs, trEvents.toList)
      addEvent(event)
      trEvents.clear()
      received = false
      zk.multi(getOps, new CheckMultiCallback(event), null)
	Thread.sleep(ZKClient.CB_DELAY)
    } catch {
      case e:Throwable => {
        trExc = e
        throw e
      }
    }
  } label "async. transaction commit" catches ("NodeExistsException" -> "checkExists", "NoNodeException" -> "checkNotExists", "ConnectionLossException" -> "connectionLoss", "NoAuthException" -> "checkNoAuth", "BadArgumentsException" -> "checkBadArgs")
  */

//Connection Loss
  "connectionLoss" -> "closed" :={
    Console.err.println("Connection Loss")
    finish
  }
  //Close
  "closing" -> "closed" := {
    finish
  } weight 0.2
  "closed" -> "closed" := {
    // Console.out.println("#####called2"+nCalled)
    choose(
      { () => zk.setData(chooseNode, (choose(5,10).toString).getBytes(), -1) },
      { () => zk.delete(chooseNode, -1) },
      { () => zk.create(chooseNode, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, chooseNodeType) }
    )
  } throws ("SessionExpiredException") label "client conn. closed"
  "closed" -> "finished" := {} weight 0.5
}
