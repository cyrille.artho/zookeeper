package modbat

import org.apache.zookeeper.KeeperException
import org.apache.zookeeper.data.{ACL, Id}
import org.apache.zookeeper.ZooDefs.{Perms, Ids}
import scala.collection.mutable.{ListBuffer}
import events.{ZKEvent, Log}
import collection.mutable.{HashMap, MultiMap}

/** Front end that provides operations to Nodes on a given path */
class Tree {
  override def clone = {
    val t = new Tree
    t.root = root.clone
    existsWatches.foreach {
      case (key, value) =>
        t.existsWatches.update(key, value)
    }
    t
  }

  override def toString = root.toString

  var root = new Node("/")
  val existsWatches = new HashMap[String, collection.mutable.Set[ZKEvent]] with MultiMap[String, ZKEvent]
  root.data = Some(Seq())
  create("/zookeeper", List(Ids.ANYONE_ID_UNSAFE))

  def clear {
    root = new Node("/")
    root.data = Some(Seq())
    create("/zookeeper", List(Ids.ANYONE_ID_UNSAFE))
  }

  override def equals(that:Any) = {
    if ((that != null) && (that.isInstanceOf[Tree])) {
      val other = that.asInstanceOf[Tree]
      root.equals(other.root) && existsWatches.equals(other.existsWatches)
    } else {
      false
    }
  }

  override def hashCode() = {
    root.hashCode()
  }

  def create(name: String, authInfoList:List[Id] = List(Ids.ANYONE_ID_UNSAFE)): String = {
    var parentName = name.substring(0, name.lastIndexOf('/'))
    lookup(parentName) match {
      case Some(node: Node) =>{
        checkACL(node, Perms.CREATE, authInfoList, name)
        if (exists(name)) {
          throw KeeperException.create(KeeperException.Code.NODEEXISTS, name)
        }
        node.createChild(name.substring(name.lastIndexOf('/') + 1))
        // Create triggers data watches for the node being created and child watches for the parent node
        Log.log(List(("node", node)), "clear child_watches")
        node.child_watches.clear()
      }
      case None => throw KeeperException.create(KeeperException.Code.NONODE, name)
    }
    name
  }

  def delete(name: String, authInfoList:List[Id] = List(Ids.ANYONE_ID_UNSAFE)) { //not removing
    if (name == "/") {
      throw KeeperException.create(KeeperException.Code.BADARGUMENTS, name)
    }
    lookup(name) match {
      case None => throw KeeperException.create(KeeperException.Code.NONODE, name)
      case Some(node: Node) => {
        val parentName = name.substring(0, name.lastIndexOf('/')) //Can be done in one line if needed
        lookup(parentName) match {
          case None => throw KeeperException.create(KeeperException.Code.NONODE, name)
          case Some(parentNode: Node) =>{
            checkACL(parentNode, Perms.DELETE, authInfoList, name)
            if (!node.getChildren.isEmpty){
              throw KeeperException.create(KeeperException.Code.NOTEMPTY, name)
            }else {
              // Delete triggers data watches for the node being deleted and child watches for the node being deleted and the parent  node
              Log.log(List(("node", node)), "clear data_watches and child_watches")
              node.data_watches.clear()
              node.child_watches.clear()
              Log.log(List(("node", parentNode)), "clear data_watches and child_watches")
              parentNode.child_watches.clear()
              parentNode.children.remove(name.substring(name.lastIndexOf('/') + 1))
            }
          }
        }
      }
    }
  }

  def exists(event: ZKEvent): Boolean = {
    lookup(event.name) match {
      case Some(node: Node) => {
        if (event.watcher) {
          node.data_watches += event
          Log.log(List(("event", event), ("data_watches", node.data_watches)), "a watche is set")
        }
        true
      }
      case None => false
    }
  }

  def exists(name: String): Boolean = {
    // check the entire tree, not safe for the moment : name == root.getChildren
    lookup(name) match {
      case Some(node: Node) => true
      case None => false
    }
  }

  def check(name: String, authInfoList:List[Id] = List(Ids.ANYONE_ID_UNSAFE)): Unit = {
    // only check if the node exists
    lookup(name) match {
      case Some(node: Node) =>{
        checkACL(node, Perms.READ, authInfoList, name)
        ()
      }
      case None => throw KeeperException.create(KeeperException.Code.NONODE, name)
    }
  }

  def getChildren(name: String, authInfoList:List[Id]): Set[String] = {
    // check the entire tree, not safe for the moment : name == root.getChildren
    lookup(name) match {
      case Some(node: Node) => {
        checkACL(node, Perms.READ, authInfoList, name)
        node.getChildren
      }
      case None => throw KeeperException.create(KeeperException.Code.NONODE, name)
    }
  }

  def getChildren(event: ZKEvent, authInfoList:List[Id]): Set[String] = {
    // check the entire tree, not safe for the moment : name == root.getChildren
    lookup(event.name) match {
      case Some(node: Node) => {
        checkACL(node, Perms.READ, authInfoList, event.name)
        node.child_watches += event
        node.getChildren
      }
      case None => throw KeeperException.create(KeeperException.Code.NONODE, event.name)
    }
  }

  def lookup(name: String): Option[Node] = {
    var cur = root
    for (pathElement <- name.split('/').drop(1)) {
      cur.getChild(pathElement) match {
	case Some(node: Node) => cur = node
	case None => return None
      }
    }
    Some(cur)
  }

  def wrapData(dataSet: Seq[Byte]) = {
    if (dataSet == null) {
      None
    } else {
      Some(dataSet)
    }
  }

  def setData(name: String, dataSet: Array[Byte], authInfoList:List[Id] = List(Ids.ANYONE_ID_UNSAFE)) = {
    lookup(name) match {
      case Some(node: Node) => {
        checkACL(node, Perms.WRITE, authInfoList, name)
	    node.data = dataSet match {
	      case null => None
	      case a: Array[Byte] => {
            // SetData triggers data watches for the node being set
            Log.log(List(("node", node)), "clear data_watches")
            node.data_watches.clear()
            Some(a.toSeq)
          }
	    }
      }
      case None => throw KeeperException.create(KeeperException.Code.NONODE, name)
    }
  }

  def getData(event: ZKEvent, authInfoList:List[Id]): Option[Seq[Byte]] = {
    lookup(event.name) match {
      case Some(node: Node) => {
        checkACL(node, Perms.READ, authInfoList, event.name)
        node.data_watches += event
        node.data
      }
      case None => throw KeeperException.create(KeeperException.Code.NONODE, event.name)
    }
  }

  def getData(name: String, authInfoList:List[Id]): Option[Seq[Byte]] = {
    //var nodeData = Array[Byte](192.toByte)
    lookup(name) match {
      case Some(node: Node) => {
        checkACL(node, Perms.READ, authInfoList, name)
        node.data
      }
      case None => throw KeeperException.create(KeeperException.Code.NONODE, name)
    }
  }

  def setACL(name: String, acls: ListBuffer[ACL], authInfoList:List[Id]  = List(Ids.ANYONE_ID_UNSAFE)) = {
    if (acls != null && acls.size > 0) {
      lookup(name) match {
        case Some(node: Node) => {
          checkACL(node, Perms.ADMIN, authInfoList, name)
          node.setACL(acls)
        }
        case None => throw KeeperException.create(KeeperException.Code.NONODE, name)
      }
    } else {
      throw KeeperException.create(KeeperException.Code.INVALIDACL, name)
    }
  }

  def getACL(name: String, authInfoList:List[Id] = List(Ids.ANYONE_ID_UNSAFE)):java.util.List[ACL] = {
    lookup(name) match {
      case Some(node: Node) => node.getACL
      case None => throw KeeperException.create(KeeperException.Code.NONODE, name)
    }
  }

  def checkACL(node:Node, perm:Int, authInfoList:List[Id], targetNode: String){
    if(authInfoList.foldLeft(0)((x,y) => x | (node.acls.getOrElse(y, 0) & perm)) == 0){
      // Log.log(List(("acl", node.acls), ("perm", perm)), "checkACLExc")
      var name = node.name
      if(node.name.length > 0 && node.name(0) != '/'){
        name = "/"+node.name
      }
      throw KeeperException.create(KeeperException.Code.NOAUTH, targetNode)
    }
  }
}

object Tree {
  def main(args: Array[String]) {
    val t = new Tree()
    val authInfoList = List(Ids.ANYONE_ID_UNSAFE)
    t.create("/a")
    t.setData("/a", "testData".getBytes())
    t.create("/a/b")
    t.create("/c")
    assert(t.exists("/a/b"))
    assert(!t.exists("/b"))
    assert(t.exists("/c"))
    val children = t.root.getChildren
    assert(children.contains("a"))
    assert(children.contains("c"))
    assert(children.size == 2)
    t.delete("/a/b")
    assert(t.exists("/a"))
    t.clear
    assert(!t.exists("/x"))
    assert(t.getData("/a", authInfoList).contains(Some("testData".getBytes())))
  }
}
