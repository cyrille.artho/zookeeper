package modbat

import sys.process._
import modbat.dsl._

import org.apache.zookeeper.{Watcher, WatchedEvent, ZooKeeper, ZooDefs, CreateMode, ClientCnxn}
import org.apache.zookeeper.Watcher.Event.{KeeperState, EventType}
import org.apache.zookeeper.data.Stat
import org.apache.zookeeper.server.ZooKeeperServer
import org.apache.zookeeper.ZooKeeper.States
import org.apache.zookeeper.AsyncCallback.{StringCallback, DataCallback, VoidCallback, StatCallback, ChildrenCallback}
import org.apache.zookeeper.KeeperException.Code
import org.apache.zookeeper.KeeperException.Code._
import org.apache.zookeeper.ZooDefs.Ids

import java.io.{FileOutputStream, FileInputStream, BufferedOutputStream, BufferedReader, FileReader, OutputStreamWriter, File}
import java.util.{ArrayList, Random, Arrays}

import scala.collection.mutable.{HashSet, HashMap, ListBuffer}
import scala.runtime.RichByte

object ZKQuorumClient {
  val globalListZnodeEph = new HashSet[String]()

  val nodeInfo = new HashMap[String, Int]()
  val nodeInfoData = new HashMap[String, Int]()
  val nodeInfoChildren = new HashMap[String, Int]()
}

class ZKQuorumClient(zkserver: ZooKeeperServer, zkPort: Integer) extends Model {
  import ZKQuorumClient._

  val SESSIONTIMEOUT = 2000
  var event: WatchedEvent = null
  val mainPathTemp: String = "/temporary"
  var mainPath: String = mainPathTemp
  val verifPath: String = "temp/verif"
  var listOfEphNodes = new ListBuffer[String]()

  var isConnected: Boolean = false
  var isExpired: Boolean = false
  var exists: Boolean = false
  var ephemeral: Boolean = false
  var empty: Boolean = false
  var pathExists: Boolean = false

  var isOKEmpty: Boolean = true
  var isOKExists: Boolean = true

  var nodeTypeAsync: CreateMode = null
  var dataAsync: Array[Byte] = null
  var mainPathAsync: String = null
  var ctx: Object = null
  var choice: String = ""

  val client = this
  val watchConnection: Watcher = new Watcher() {
    override def process(event: WatchedEvent) {

      //Console.err.println(event.getState())
      client.synchronized {
	event.getState() match {
	  case KeeperState.SyncConnected => isConnected = true
	  case KeeperState.Disconnected => isConnected = false
	  case KeeperState.Expired => isExpired = true
	  case _ =>
	}
	client.notify()
      }
      //Console.out.println(event.getType() + ", path: " + event.getPath())
      //val watcher = new ZKWatcher(zk)
      //launch(watcher)
      //process(event)
    }
  }

  val port = "localhost:" + zkPort + "/"
  val zk = new ZooKeeper(port, SESSIONTIMEOUT, watchConnection)
  this.synchronized {
    try {
      while (!isConnected) { // let watcher be triggered on slow computers
	wait()
      }
      while(!zk.getState.isConnected()) {
	Thread.sleep(10)
      }
    } catch {
      case _: InterruptedException => // ignore InterruptedException
    }
  }

/*
  @States("...") def checkCallBacks {
    for (n <- callbackNodes) {
      if (n.exists() assert (...))
    }
  }
*/

  var createCallback = new StringCallback() {
    override def processResult(rc: Int, path: String, ctx: Object, name: String) {
      val temp = Code.get(rc)
      if (temp == OK){
	if (nodeTypeAsync == CreateMode.EPHEMERAL) {
	  listOfEphNodes += path
	  globalListZnodeEph += path
	}
      (new File(verifPath + path)).mkdirs()
      }
      if (temp == NONODE) {
	exists = checkExists(mainPath)
	checkIfNoPendingAction(!exists, isOKExists)
      }
      if (temp == NODEEXISTS) {
	exists = checkExists(path)
	checkIfNoPendingAction(exists, isOKExists)
      }
      if (temp == NOCHILDRENFOREPHEMERALS) {
	assert(isEphemeral(mainPath))
      }
      removePending(path, nodeInfo)
      removePending(mainPathAsync, nodeInfoChildren)
      removePending(path, nodeInfoData)
    }
  }

  var deleteCallback = new VoidCallback() {
    override def processResult(rc: Int, path: String, ctx: Object) {
      val temp = Code.get(rc)
      if (temp == OK){
	assert(zk.exists(path, false) != null)
	val file = new File(verifPath + path)
	var files = file.listFiles()
	  if(!files.isEmpty) {
	    for(f <- files) {
	      f.delete()
	    }
	  }
	file.delete()
	if(globalListZnodeEph.contains(path))
	  globalListZnodeEph -= path
      }
      if (temp == NONODE) {
	exists = checkExists(path)
	checkIfNoPendingAction(!exists, isOKExists)
      }
      if (temp == NOTEMPTY) {
	empty = checkEmpty(path)
	checkIfNoPendingAction(!empty, isOKEmpty)
      }
      removePending(path, nodeInfo)
      removePending(mainPathAsync, nodeInfoChildren)
      removePending(path, nodeInfoData)
    }
  }

  var setDataCallback = new StatCallback() {
    override def processResult(rc: Int, path: String, ctx: Object, stat: Stat) {
      val temp = Code.get(rc)
      if (temp == OK){
        setDataInFile(path, dataAsync)
      }
      if (temp == NONODE) {
	exists = checkExists(mainPath)
	checkIfNoPendingAction(!exists, isOKExists)
      }
      removePending(path, nodeInfoData)
    }
  }
 
  var getDataCallback = new DataCallback() {
    override def processResult(rc: Int, path: String, ctx: Object, data: Array[Byte], stat: Stat) {
      val temp = Code.get(rc)
      if (temp == OK){
        val file = new File(verifPath + path + "/data.bin")
        if(file.isFile()) {
          var input = new FileInputStream(file)
          var dataFile = new Array[Byte](input.available())
          input.read(dataFile)
          input.close()
          assert(Arrays.equals(data, dataFile))
        }
      }
      if (temp == NONODE) {
	exists = checkExists(mainPath)
	checkIfNoPendingAction(!exists, isOKExists)
      }
    }
  }

  var existsCallback = new StatCallback() {
    override def processResult(rc: Int, path: String, ctx: Object, stat: Stat) {
      val temp = Code.get(rc)
      if (temp == OK){
	assert((new File(verifPath + path).isDirectory) == (stat != null))
      }
    }
  }

  var getChildrenCallback = new ChildrenCallback() {
    override def processResult(rc: Int, path: String, ctx: Object, children: java.util.List[String]) {
      val temp = Code.get(rc)
      if (temp == OK){
	val folder = new File(verifPath + path)
	val files = folder.listFiles()
	var nb = 0
	val nbChildren = children.size()
	if(files == null) {
	  assert(nbChildren == 0, {"Expected nb children = 0, got: " + nbChildren} )
	} else {
	  for(f <- files) {
	    if(f.isDirectory)
	      nb += 1
	  }
	assert(nbChildren == nb, {"Expected same size, expected: " + nb + " , got: " + nbChildren} )
	}
      }
      if (temp == NONODE) {
	exists = checkExists(mainPath)
	checkIfNoPendingAction(!exists, isOKExists)
      }
    }
  }

  @After
  def cleanUp {
    deleteEphemeral
    zk.close()
  }

  def deleteEphemeral {
    if(!listOfEphNodes.isEmpty) {
      for(path <- listOfEphNodes) {
        if(globalListZnodeEph.contains(path)) {
          val file = new File(verifPath + path)
          var files = file.listFiles()
          if(!files.isEmpty) {
	    for(f <- files) {
	      assert(f.isFile())
	      f.delete()
	    }
          }
          file.delete()
          globalListZnodeEph -= path
	}
      }
    }
  }

  def isEphemeral(path: String) = globalListZnodeEph.contains(path)

  def pickRandomChild: String = {
    var listChildren = zk.getChildren(mainPath, false)
    require(!listChildren.isEmpty())
    val randomizer = new Random()
    var random: String = null
    var path: String = null

    do {
    random  = (listChildren.get(randomizer.nextInt(listChildren.size()))).toString
    path = mainPath + "/" + random
    } while (!(new File(verifPath + path)).isDirectory())
    assert(zk.exists(path, false) != null, {"Expected node exists but failed"} )
    path
  }

  def cdChild {
    require(isConnected)
    var listChildren = zk.getChildren(mainPath, false)
    if (listChildren.isEmpty())
      mainPath = mainPath + "/" + String.valueOf(choose(0,3))
    else
      mainPath = pickRandomChild
  }

  def cdParent {
    require(isConnected)
    if(mainPath != mainPathTemp)
      mainPath = mainPath.substring(0, mainPath.length()-2)
  }

  def create(path: String, nodeType: CreateMode) {
    require(isConnected)
    zk.create(path, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, nodeType)
    if (nodeType == CreateMode.EPHEMERAL) {
      listOfEphNodes += path
      globalListZnodeEph += path
    }
    (new File(verifPath + path)).mkdirs()
  }

  def chooseNodeType = {
    if (choose()) { CreateMode.PERSISTENT }
    else { CreateMode.EPHEMERAL }
  }

  def delete(path: String) {
    require(isConnected)
    zk.delete(path,-1)
    val file = new File(verifPath + path)
    var files = file.listFiles()
    if(!files.isEmpty) {
      for(f <- files) {
        f.delete()
      }
    }
    file.delete()
    if(globalListZnodeEph.contains(path))
      globalListZnodeEph -= path
  }

  def setData(path: String)  {
    var data = (choose(5,10).toString).getBytes()
    zk.setData(path, data, -1)
    setDataInFile(path, data)
  }

  def setDataInFile(path: String, data: Array[Byte]) {
    val tempPath = verifPath + path + "/data.bin"
    assert((new File(verifPath + path)).isDirectory(), { "Fail" } )
    var output = new BufferedOutputStream(new FileOutputStream(new File(tempPath)))
    output.write(data)
    output.close()
  }

  def getData(path: String) {
    val stat = new Stat()
    val data = zk.getData(path, false, stat)
    val file = new File(verifPath + path + "/data.bin")
    if (file.isFile()) {
      var input = new FileInputStream(file)
      var dataFile = new Array[Byte](input.available())
      input.read(dataFile)
      input.close()
      assert(Arrays.equals(data, dataFile), {"datas are not equal"} )
    }
  }

  def exists(path: String) = {
    assert((new File(verifPath + path).isDirectory) == (zk.exists(path,false) != null), {"Fail"} )
    zk.exists(path,false) != null
  }

  def getChildren(path: String) = {
    val folder = new File(verifPath + path)
    val files = folder.listFiles()
    var nb = 0
    val nbChildren = zk.getChildren(path,false).size
    if(files == null) {
      assert(nbChildren == 0, {"Expected nb children = 0, got: " + nbChildren} )
    } else {
      for(f <- files) {
	if(f.isDirectory)
	  nb += 1
      }
    assert(nbChildren == nb, {"Expected same size, expected: " + nb + " , got: " + nbChildren} )
    }
  }

  def addPending(path: String, hashMap: HashMap[String, Int]) {
    val count = hashMap.getOrElse(path, 0)
    hashMap.put(path, count + 1)
  }

  def removePending(path: String, hashMap: HashMap[String, Int]) {
    var value = hashMap.getOrElse(path, 0)
    assert(value > 0)
    value -= 1
    if(value == 0)
      hashMap.remove(path)
    else
      hashMap.put(path,value)
  }

  def isSafeToCheck(path: String, hashMap: HashMap[String, Int]) = !(hashMap.contains(path))

  def checkGetChildren(path: String) {
    if(isSafeToCheck(path, nodeInfoChildren) && isSafeToCheck(path, nodeInfo))
      getChildren(path)
  }

  def checkGetData(path: String) {
    if((isSafeToCheck(path, nodeInfoData)) && (isSafeToCheck(path, nodeInfo)) && (isSafeToCheck(path, nodeInfoChildren)))
     getData(path)
  }

  def checkEmpty(path: String) = {
    if(isSafeToCheck(path, nodeInfoChildren) && isSafeToCheck(path, nodeInfo)) {
      if(zk.exists(path, false) != null) {
        zk.getChildren(choice, false).isEmpty
      } else {
	true
      }
    } else {
      isOKEmpty = false
      true
    }
  }
	
  def checkExists(path: String) = {
    if(isSafeToCheck(path, nodeInfo)) {
      exists(path)
    }
    else {
      isOKExists = false
      true
    }
  }

  def checkTreeNode(folder: File, path: String) {
    val files = folder.listFiles()
    if(isSafeToCheck(path, nodeInfo)) {
      assert(zk.exists(path, false) != null, {"File with the past: " + path + ", does not exist"} )
    }
    checkGetChildren(path)
    if(files != null) {
      for(f <- files) {
	if(f.isDirectory()) {
          val path2 = path + "/" + f.getName()
          if(isSafeToCheck(path2, nodeInfo))
	    exists(path2)
          checkTreeNode(f,path2)
	} else {
	  checkGetData(path)
	}
      }
    }
  }

  def checkTreeNodeGlobal {
    val mainFolder = new File(verifPath + mainPathTemp)
    checkTreeNode(mainFolder, mainPathTemp)
  }

  def checkIfNoPendingAction(predicate: Boolean, isOK: Boolean) {
    if (isOK) {
      assert(predicate)
    }
    if (isOK == isOKExists)
      isOKExists = true
    if (isOK == isOKEmpty)
      isOKEmpty = true
  }

//Connection
      "connecting" -> "connected" := {
	require(isConnected, {"Connection has failed"} )
	if((zk.exists(mainPathTemp, false) == null))
          create(mainPath, CreateMode.PERSISTENT)
	assert((zk.exists(mainPathTemp, false) != null))
	(new File(verifPath + mainPathTemp)).mkdirs()
	checkTreeNodeGlobal
      }

//Cd
  //CdChild
      "connected" -> "connected" := {
	require(isConnected)
	pathExists = (zk.exists(mainPath, false) != null)
	cdChild
        checkTreeNodeGlobal
      } catches ("NoNodeException" -> "checkNoNode")
  //CdParent
      "connected" -> "connected" := {
	require(isConnected)
	pathExists = (zk.exists(mainPath, false) != null)
	cdParent
        checkTreeNodeGlobal
      }
//Exceptions
      "connected" -> "connected" := {
      //Removing the root
	delete("/")
      } throws ("BadArgumentsException") weight 0.1
  //Session Expired
      /*"connected" -> "sessionExpired" := {
        require(isConnected)
        zk.disconnect()
        Thread.sleep(SESSIONTIMEOUT + 100)
        zk.connect()
        Thread.sleep(100)
        //isExpired = true // FIXME: Watcher is not reliably triggered, not sure why
        assert(isExpired, {"Session is not expired"} )
        deleteEphemeral
        checkTreeNodeGlobal
      }
      "sessionExpired" -> "sessionExpired" := {
        require(isExpired)
        choose (
          { () => create(mainPath + "/test", chooseNodeType) }
          //{ () => zk.setData(mainPath, data, -1) },
          //{ () => delete(mainPath) }
        )
      } throws ("SessionExpiredException"),
      "sessionExpired" -> "close" := {
        require(isExpired)
      }*/

//Synchronous actions
  //Read
      "connected" -> "tryRead" := {
	require(isConnected)
	checkTreeNodeGlobal
      }
    //GetData, getChildren, exists
      "tryRead" -> "connected" := {
	require(isConnected)
	pathExists = checkExists(mainPath)
	choose (
	  { () => checkGetData(mainPath) },
	  { () => checkGetChildren(mainPath) }
	)
      } catches ("NoNodeException" -> "checkNoNode")
  //Write
      "connected" -> "tryWrite" := {
	require(isConnected)
	choice = mainPath + "/" + String.valueOf(choose(0,3))
	checkTreeNodeGlobal
	}
    //Create
      "tryWrite" -> "connected" := {
	exists = checkExists(choice)
	pathExists = checkExists(mainPath)
	ephemeral = isEphemeral(mainPath)
	create(choice, chooseNodeType)
	checkIfNoPendingAction(!exists, isOKExists)
      } catches ("NoNodeException" -> "checkNoNode", "NodeExistsException" -> "checkExists", "NoChildrenForEphemeralsException" -> "checkEphemeral")
    //Delete
      "tryWrite" -> "connected" := {
	exists = checkExists(choice)
	empty = checkEmpty(choice)
        delete(choice)
	checkIfNoPendingAction(exists, isOKExists)
	checkIfNoPendingAction(empty, isOKEmpty)
      } catches ( "NotEmptyException" -> "checkNotEmpty", "NoNodeException" -> "checkNotExists")
    //SetData
      "tryWrite" -> "connected" := {
	require(isConnected)
	pathExists = checkExists(choice)
	setData(choice)
      } catches ("NoNodeException" -> "checkNoNode")

//CheckExceptions
      "checkNotEmpty" -> "connected" := {
	checkIfNoPendingAction(exists, isOKExists)
	checkIfNoPendingAction(!empty, isOKEmpty)
      }
      "checkNotExists" -> "connected" := {
	checkIfNoPendingAction(!exists, isOKExists)
      }
      "checkExists" -> "connected" := {
	checkIfNoPendingAction(exists, isOKExists)
      }
      "checkEphemeral" -> "connected" := {
	assert(ephemeral)
	checkIfNoPendingAction(!exists, isOKExists)
      }
      "checkNoNode" -> "connected" := {
	checkIfNoPendingAction(!pathExists, isOKExists)
	mainPath = mainPathTemp
      }

//Asynchronous Action
  //Asynchronous read
     "connected" -> "tryReadAsync" := {
	require(isConnected)
	ctx = null
	checkTreeNodeGlobal
      } weight 4.0
  //getData
      "tryReadAsync" -> "connected" := {
	require(isConnected) 
        zk.getData(mainPath, false, getDataCallback, ctx) 
      }

  //exists
      "tryReadAsync" -> "connected" := {
	require(isConnected)
        zk.exists(mainPath, false, existsCallback, ctx)
      }
  //getChildren
      "tryReadAsync" -> "connected" := {
	require(isConnected)
        zk.getChildren(mainPath, false, getChildrenCallback, ctx)
      }
  //Asynchronous write
      "connected" -> "tryWriteAsync" := {
	require(isConnected)
	ctx = null
	choice = mainPath + "/" + String.valueOf(choose(0,3))
	checkTreeNodeGlobal
     }
    //create
      "tryWriteAsync" -> "connected" := {
	require(isConnected)
	mainPathAsync = mainPath
	addPending(choice, nodeInfo)
	addPending(mainPathAsync, nodeInfoChildren)
	addPending(choice, nodeInfoData)
        nodeTypeAsync = chooseNodeType
        zk.create(choice, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, nodeTypeAsync, createCallback, ctx)
      }
    //delete
      "tryWriteAsync" -> "connected" := {
	require(isConnected)
	mainPathAsync = mainPath
	addPending(choice, nodeInfo)
	addPending(mainPathAsync, nodeInfoChildren)
	addPending(choice, nodeInfoData)	
        zk.delete(choice, -1, deleteCallback, ctx)
      }
    //setData
      "tryWriteAsync" -> "connected" := {
	require(isConnected)
	addPending(choice, nodeInfoData)
        dataAsync = (choose(5,10).toString).getBytes()
        zk.setData(choice, dataAsync, -1, setDataCallback, ctx)
      }

//Close
      "connected" -> "close" := {
        require(isConnected)
	checkTreeNodeGlobal
	deleteEphemeral
	zk.close()
      } weight 0.2
      "close" -> "close" := {
	choose(
	  { () => getData(mainPath) },
	  { () => setData(mainPath) },
	  { () => delete(mainPath) },
	  { () => create(mainPath, chooseNodeType) }
	)
      }throws ("SessionExpiredException")
}
