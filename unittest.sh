#!/bin/sh
export LD_LIBRARY_PATH="lib"
ZKLIBS=`ls zookeeper*/*.jar | head -1`:`ls -d zookeeper*/conf`:`ls zookeeper*/lib/*.jar | tr '\n' :`
CLASSPATH=${ZKLIBS}.:zookeeper-3.4.8/zookeeper-3.4.8.jar:lib/scalatest_2.11-2.2.4.jar:lib/openmodbat.jar scala -J-ea org.scalatest.run test.WatcherTriggerTest
CLASSPATH=${ZKLIBS}.:zookeeper-3.4.8/zookeeper-3.4.8.jar:lib/scalatest_2.11-2.2.4.jar:lib/openmodbat.jar scala -J-ea org.scalatest.run test.PruningTest

# remove Zookeeper transactions logs
rm -f temp/zookeeper/version-2/log.*
