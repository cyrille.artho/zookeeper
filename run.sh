#!/bin/sh
#!/bin/bash

# help message
usage() {
  echo "Usage: ./run.sh [OPTIONS]"
  echo
  echo "options:"
  echo "  -h, --help"
  echo "  -n <integer> the number of test cases (default 1000)"
  echo "  -c, --clients <integer> the number of clients (default 4)"
  echo "  --api <integer> the number of API calls (default 4)"
  echo "  -a, --algorithm [\"bfs\"|\"heuristics\"|\"dfs\"|\"create\"|\"finish\"|\"create_strict\"|\"finish_strict\"] search algorithm (default \"bfs\")"
  echo "  -s, --seed <integer> random seed (default 1)"
  echo "  -p, --pruning pruning"
  echo "  -np, --no_pruning no pruning (default)"
  echo "  -d, -delay, delay after an async. call in ms (default 10)"
  echo
  exit 1
}

#defalut values
N=1000
LOOP_LIMIT=100 #dammy
CLIENTS=4
API_CALLS=4
SEED=1
ALGO="bfs"
PRUNING=0
DELAY=10
#parse options
for OPT in "$@"
do
  case "$OPT" in
    # help message
    '-h'|'--help' )
      usage
      exit 1
      ;;
    # search algorithm
    '-a'|'--algorithm' )
      ALGO="$2"
      shift 2
      ;;
    # number of test cases
    '-n' )
      N=$2
      shift 2
      ;;
    # number of API calls
    '--api' )
      API_CALLS=$2
      shift 2
      ;;
    # number of clients
    '-c'|'--clients' )
      CLIENTS=$2
      shift 2
      ;;
    # random seed
    '-s'|'--seed' )
      SEED=$2
      shift 2
      ;;
    # pruning
    '-p'|'--pruning' )
      PRUNING=1
      shift 1
      ;;
    # no pruning
    '-np'|'--no_pruning' )
      PRUNING=0
      shift 1
      ;;
    # delay
    '-d'|'--delay' )
      DELAY=$2
      shift 2
      ;;
    -*)
      echo "wrong options "$1 1>&2
      exit 1
      ;;
  esac
done
# echo "LOOP_LIMIT="$LOOP_LIMIT
# echo "N="$N
# echo "ALGO="$ALGO
# echo "SEED="$SEED
# echo "PRUNING="$PRUNING
echo $ALGO > settings.log
echo $PRUNING >> settings.log
echo $API_CALLS >> settings.log
echo $CLIENTS >> settings.log
echo $DELAY >> settings.log
ZKLIBS=`ls zookeeper*/*.jar | head -1`:`ls -d zookeeper*/conf`:`ls zookeeper*/lib/*.jar | tr '\n' :`
export JAVA_OPTS="-Xmx48G -Xms4G"
export LD_LIBRARY_PATH="lib"

CLASSPATH=${ZKLIBS}. \
time scala \
	-Dlog4j.configDebug=true \
	-Dlog4j.debug=true \
        -Dlog4j.configuration="log4j.properties" \
	-Dzookeeper.preAllocSize=1024 \
	lib/openmodbat.jar \
        -s=$SEED \
        -n=$N \
        --loop-limit=$LOOP_LIMIT \
	--dotify-coverage \
	--no-redirect-out \
	--log-level=fine \
        modbat.ZKServer

# remove Zookeeper transactions logs
rm -f temp/zookeeper/version-2/log.*
