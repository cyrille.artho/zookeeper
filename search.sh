#!/bin/bash
touch search.log
rm search.log
search_algo=("bfs" "heuristics" "dfs" "finish" "create")
test_num=(4 5 6 7)
for i in ${test_num[@]}
do
    for a in ${search_algo[@]}
    do
        echo "1000 times, ${i} sessions/API calls, ${a}" >> search.log
        for j in {1..3}
        do
            echo "[${j}] 1000 times, ${i} sessions/API calls, ${a}"
            (time ./run.sh -n 1000 -c $i --api $i -s $j -a $a -np) 1> /dev/null 2> tmp.log
            tail -n 3 tmp.log >> search.log
            sleep 2
        done
        echo "1000 times, ${i} sessions/API calls, ${a}_pruning" >> search.log
        for j in {1..3}
        do
            echo "[${j}] 1000 times, ${i} sessions/API calls, ${a}_pruning"
            (time ./run.sh -n 1000 -c $i --api $i -s $j -a $a -p) 1> /dev/null 2> tmp.log
            tail -n 3 tmp.log >> search.log
            sleep 2
        done
    done
done
#output average
awk '{if ($1 == "user"){gsub("m", " ", $2); gsub("s", "", $2);}else if($1=="1000"){gsub(",", "", $0);};print $0}' search.log | awk '{if ($1=="1000" ){if(num>0){print sec/num; sec=0; num=0;}printf "%d %s ", $3, $6;}else if($1=="user"){sec=sec+60*$2+$3; num=num+1;}}END{print sec/num}' | awk 'BEGIN{n=0;printf ",";}{arr[$2][$1] = $3; if($1>n){printf "%s, ", $1; n = $1;}}END{for(alg in arr){printf "\n%s, ", alg; for(i in arr[alg]){printf "%s, ", arr[alg][i]}}}' > average.log
