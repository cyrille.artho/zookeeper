#!/bin/sh
. modbat.sh
ZKLIBS=`ls zookeeper*/*.jar | head -1`:`ls -d zookeeper*/conf`:`ls zookeeper*/lib/*.jar | tr '\n' :`
CLASSPATH=${ZKLIBS}. \
scala \
	-Dlog4j.configDebug=true \
	-Dlog4j.debug=true \
        -Dlog4j.configuration="log4j.properties" \
	-Dzookeeper.preAllocSize=1024 \
        ${MODBAT} \
        -s=7c0cac0915e706be \
        -n=200 \
        --loop-limit=7 \
	--dotify-coverage \
	--no-redirect-out \
        modbat.ZKServer

# remove Zookeeper transactions logs

rm -f temp/zookeeper/version-2/log.*
