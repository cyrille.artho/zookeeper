#!/bin/bash

# TimeLimitConfgFile="../TimeLimit.config"

# the cut dependecy library path, for evosuite we set it in evosuite.properties file
# CUT_LIB_PATH=../../../../benchmark/a4j/lib/jox116.jar:../../../../benchmark/a4j/lib/log4j-1.2.4.jar
CUT_LIB_PATH=../../../../benchmark/zookeeper-3.4.8/lib/jline-0.9.94.jar:../../../../benchmark/zookeeper-3.4.8/lib/log4j-1.2.16.jar:../../../../benchmark/zookeeper-3.4.8/lib/netty-3.7.0.Final.jar:../../../../benchmark/zookeeper-3.4.8/lib/slf4j-api-1.6.1.jar:../../../../benchmark/zookeeper-3.4.8/lib/slf4j-log4j12-1.6.1.jar

# ************Set by User************************
# the path or directory of the SUT
CUT_Home_Path=../../../../benchmark/zookeeper-3.4.8/
CUT_PATH=$CUT_Home_Path/build/classes
CUT_SRC_PATH=$CUT_Home_Path/src/java/main
# this impurity results is not the one for zookeeper but tiny sql to make it work.
CUT_Purity_PATH=$CUT_Home_Path/infer-output
# ************Set by User************************

Evosuite_TOOL_PATH=
Randoop_TOOL_PATH=
GRT_TOOL_PATH=../../../../test-tool/GRT.jar

JUnit_Output_Dir=./GRT-tests

# the time budget for each class, set automatically when initializing project directory structure.
# TimeLimit=$(cat $TimeLimitConfgFile)

# the memory size for evosuite client process
ClientMem=4096

echo "*time limit for each class: $TimeLimit seconds"
