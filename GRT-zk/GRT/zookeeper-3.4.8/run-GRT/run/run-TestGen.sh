#!/bin/bash

. ./run-testGen-config.sh

echo " * starting GRT test generation..."

Timelimit=60

# $CUT_Purity_PATH \
java -Xms512m -Xmx4096m -cp $GRT_TOOL_PATH:$CUT_LIB_PATH:$CUT_PATH mttest.main.OptimizedGenTests \
$CUT_PATH \
--mptest_seedOn=true \
--mptest_seedSizeRestriction=true \
--mptest_seed_primitive_size=1000 \
	\
--mptest_time_as_weight=true \
	\
--mptest_dynamicComponentType=true \
	\
--mptest_fuzzingOn=true \
	\
--mptest_TopoOrder_InputDriven=true \
	\
--mptest_CoverageGuide=true \
	\
--mptest_enableCoverageIntervalUpdate=true \
--mptest_coverageUpdateInterval=50000 \
--mptest_coverageDataSaveToFile=true \
--mptest_enableCoverageDataIntervalOutput=true --dont_output_tests=true --output_tests=all \
--junit_output_dir=$JUnit_Output_Dir \
--junit_package_name=grtTest \
--mptest_debug=true \
--mptest_debug_output_ComponentManager=false \
--public_only=false \
--mptest_OutputJunitByReflection=true \
--timelimit=$Timelimit \
--mptest_time_for_randomseed=true \
--mptest_use_securityManager=true \
--mptest_CUT_SRC_dir=$CUT_SRC_PATH

#tar the coverage report 
if [ -d "coverageReport" ]; 
	then
	tar czvf "coverageReport-${Timelimit}.tgz" coverageReport
fi

shopt -s extglob
rm -r !(*.sh|*.tgz)


# --timelimit_perclass=$TimeLimit \
# --inputlimit=100000 \
# --timelimit=100
# --randomseed=0 