package events

import modbat.Tree

import scala.collection.mutable.ListBuffer
import org.apache.zookeeper.{OpResult, WatchedEvent}
import org.apache.zookeeper.data.Stat
import org.apache.zookeeper.Watcher.Event.{KeeperState, EventType}
case class OpResultList(orl: List[OpResult]) //for pattern match

class ZKState(var syncAction: Int, var systemState: Tree, var pos: List[Int])
    extends State {
  // var transaction = -1 // currently outside transaction if -1
  // value >= 0 means pending transaction on session x
  var sessionState = KeeperState.SyncConnected

  override def clone = {
    new ZKState(syncAction, systemState.clone, pos)
  }

  override def toString = {
    "ZKState with sync. action index " + syncAction + " and state " + systemState
  }

  override def equals(that:Any) = {
    if ((that != null) && (that.isInstanceOf[ZKState])) {
      val other = that.asInstanceOf[ZKState]
      (syncAction.equals(other.syncAction) && systemState.equals(other.systemState) && pos.equals(other.pos))
    } else {
      false
    }
  }

  override def hashCode() = {
    systemState.hashCode() ^ pos.hashCode() ^ syncAction
  }

  def wrapData(data:Any, action:ZKAction):Option[OpResult] = {
    (data, action) match {
      case (str:String, Create) => Some(new OpResult.CreateResult(str))
      case ((), Delete) => Some(new OpResult.DeleteResult())
      case ((), Check) => Some(new OpResult.CheckResult())
      case (_, SetData(_)) => Some(new OpResult.SetDataResult(new Stat(0,0,0,0,0,0,0,0,0,0,0)))
      case _ => None
    }
  }

  def exec(e: Event) = {
    var result: Any = null
    var exc: Option[Throwable] = None
    try {
      val event = e.asInstanceOf[ZKEvent]
      event.action match {
        case Create => {
          result = systemState.create(event.name, event.zkIDs)
          // 'create node' triggers the data watches of the node(child watches of the parent node are removed in Tree.scala)
          systemState.existsWatches.remove(event.name)
        }
        case Delete => result = systemState.delete(event.name, event.zkIDs)
        case Exists => {
          result = systemState.exists(event)
          if (!result.asInstanceOf[Boolean] && event.watcher) {
            // the watch is set to the node that does not exists
            systemState.existsWatches.addBinding(event.name, event)
          }
        }
        case GetChildren => result = systemState.getChildren(event, event.zkIDs)
        case SetData(data) => result = systemState.setData(event.name, data, event.zkIDs)
        case GetData => {
	      /* Console.err.println("result = " + systemState.getData(event.name));*/
	      result = systemState.getData(event, event.zkIDs)
        }
        case SetACL(acls) => result = systemState.setACL(event.name, acls, event.zkIDs)
        case GetACL => result = systemState.getACL(event.name, event.zkIDs)
        case Check => result = systemState.check(event.name, event.zkIDs)
	    case Multi(ops) => {
          val (results, multiexc, nextZKState) = ops.foldLeft((new ListBuffer[OpResult](), None:Option[Throwable], this.clone))(multi _)
          multiexc match {
            case Some(multiexc2) =>
              if(multiexc2.isInstanceOf[ZKException]){
                throw multiexc2.asInstanceOf[ZKException].exception
              }else{
                throw multiexc2
              }
            case None => {
              systemState = nextZKState.systemState.clone
              result = OpResultList(results.toList)
            }
          }
        }
        case CloseSession => {
          sessionState = KeeperState.Disconnected
        }
        case _ => assert(false)
      }
    } catch {
      case e: Throwable => exc = Some(new ZKException(e))
    }
    (wrapResult(result), exc)
  }

  def wrapResult(result : Any) : Option[Any] = {
    if (result == null) {
      None
    } else {
      Some(result)
    }
  }

  def calcWatchedEvent(watcherEvent : ZKEvent, execEvent : ZKEvent) : Option[WatchedEvent]= {
    // if execEvent can trigger the watcher that is set by watcherEvent
    // then returns Some(watchedEvent)
    // else returns None
    var isTriggered = false
    var eventType:EventType = null
    var path:String = null
    var nextSessionState = sessionState
    val isEqual = watcherEvent.name == execEvent.name
    val lastIndex = execEvent.name.lastIndexOf("/")
    val isChild = if (lastIndex > 0) {
      watcherEvent.name == execEvent.name.slice(0, execEvent.name.lastIndexOf("/"))
    } else {
      watcherEvent.name == "/"
    }
    if (execEvent.action == CloseSession) {
      // if watcherEvent and execEvent are called in the same session, the watcher should be triggered
      if (watcherEvent.clientName == execEvent.clientName) {
        isTriggered = true
        eventType = EventType.None
        nextSessionState = KeeperState.Disconnected
      } else {
        isTriggered = false
      }
    } else {
      path = watcherEvent.name
      systemState.lookup(watcherEvent.name) match {
        case Some(nodeWithWatch) => {
          val isDataWatch = watcherEvent.action == Exists || watcherEvent.action == GetData
          val isChildWatch = watcherEvent.action == GetChildren
          val isSet = nodeWithWatch.data_watches.contains(watcherEvent) || nodeWithWatch.child_watches.contains(watcherEvent)
	  // isSet = node has watchers set that match given event
          Log.log(List(("nodeWithWatch", watcherEvent.name), ("nodeChanged", execEvent.name),
            ("execEvent", execEvent), ("existsWatches", systemState.existsWatches), ("isDataWatch", isDataWatch),
            ("isChildWatch", isChildWatch), ("isEqual", isEqual), ("data_watches", nodeWithWatch.data_watches),
            ("child_watches", nodeWithWatch.child_watches), ("isSet", isSet)), "exec2")
          if (isSet){
            execEvent.action match {
              case Delete => // Delete triggers data watches for the node being deleted and child watches for the node being deleted and the parent node
                if (isEqual && (isDataWatch || isChildWatch)){
                  isTriggered = true
                  eventType = EventType.NodeDeleted
                } else if (isChild && isChildWatch) {
                  isTriggered = true
                  eventType = EventType.NodeChildrenChanged
                }
              case SetData(_) => // SetData triggers data watches for the node being set
                if (isEqual && isDataWatch){
                  isTriggered = true
                  eventType = EventType.NodeDataChanged
                }
              case Create => // Create triggers data watches for the node being created (handled below) and child watches for the parent node (handled here)
                if (isChild && isChildWatch) {
                  isTriggered = true
                  eventType = EventType.NodeChildrenChanged
                }
              case _ => // the watcher is not triggered
            }
          }
        }
        case None => {
          Log.log(List(("existsWatches", systemState.existsWatches)))
          if (systemState.existsWatches.getOrElse(watcherEvent.name, collection.mutable.Set[ZKEvent]()).contains(watcherEvent) &&
            isEqual && execEvent.action == Create) {
            // Create triggers data watches for the node being created (handled here) and child watches for the parent node (handled above)
            isTriggered = true
            eventType = EventType.NodeCreated
          }
        }
      }
    }
    if (isTriggered) {
      Some(new WatchedEvent(eventType, nextSessionState, path))
    } else {
      None
    }
  }

  def exec2(watcherEvent: ZKEvent, execEvent: ZKEvent, watcher_flg: Boolean) = {
    // A wrapper function of ZKState.exec.
    // If the execEvent triggers the watch, it  returns (true, 'the result of watchedEvent').
    // Otherwise, it returns (false, 'the result of exec(execEvent)')
    if (watcher_flg) {
      val watchedEvent = calcWatchedEvent(watcherEvent, execEvent)
      val (result, exc) = exec(execEvent)
      (watchedEvent, exc) match {
        case (Some(we), None) => { // execEvent succeeds and the watcher is triggered
          (true, (wrapResult(we), None))
        }
        case _ => (false, (result, exc)) // the watcher is not triggered
      }
    } else {
      (false, exec(execEvent))
    }
  }

  def multi(rez:(ListBuffer[OpResult], Option[Throwable], ZKState), op:ZKEvent) = {
    val (results, exc, zkState) = rez
    exc match {
      case Some(_) =>
        // An exception was thrown before
        rez
      case None => {
        // No exception so far
	    val (res, e) = zkState.exec(op)
        e match {
          case Some(_) => {
            (null, e, zkState)
          }
          case None => {
            wrapData(res, op.action) match {
              case Some(opr:OpResult) => results +=  opr
              case None => assert(false)
            }
            (results, exc, zkState)
          }
        }
      }
    }
  }
}
