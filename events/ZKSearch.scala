package events
import org.apache.zookeeper.{WatchedEvent}
import org.apache.zookeeper.Watcher.Event.{KeeperState, EventType}
import org.apache.zookeeper.{KeeperException}

object ZKSearch {
  def analyze(sg: ZKSessionGraph, result: Option[Any], exc: Option[Throwable]) = {
    new ZKSearch(sg, false).outcomes(result, exc)
  }

  def watcherAnalyze(sg: ZKSessionGraph, result: Option[Any], exc: Option[Throwable],
    minSessionTimeout:Int = 0, maxSessionTimeout:Int = 0) = {
    new ZKSearch(sg, true, minSessionTimeout, maxSessionTimeout).outcomes(result, exc)
  }

  def watcherTriggerAnalyze(sg: ZKSessionGraph, minSessionTimeout:Int = 0, maxSessionTimeout:Int = 0) = {
    new ZKSearch(sg, true, minSessionTimeout, maxSessionTimeout).checkWatchNotTriggered()
  }
}

class ZKSearch(sg:ZKSessionGraph, watcher_flg: Boolean,
  minSessionTimeout:Int = 0, maxSessionTimeout:Int = 0) extends Search[ZKEvent](sg) {

  override def outcomes(result: Option[Any], exc: Option[Throwable]) = {
    if (Search.SEARCH_ALGO != NoOracle) {
      if ((watcher_flg && (isExpected((result, exc), (Some(new WatchedEvent(EventType.None, KeeperState.Disconnected, null)), None), null, true))) ||
        (!watcher_flg && exc.equals(Some(ZKException.create(KeeperException.Code.CONNECTIONLOSS, null))))) {
        if (!hasConnectionLoss(sg.target)) {
          assert(false, "an unexpected connection loss happened. result = " + result + " targetEvent =  " +sg.target + " sessionEvents = "+ sg.sessionEvents)
        }
      } else if ((watcher_flg && (isExpected((result, exc), (Some(new WatchedEvent(EventType.None, KeeperState.Expired, null)), None), null, true))) ||
        (!watcher_flg && exc.equals(Some(ZKException.create(KeeperException.Code.SESSIONEXPIRED, null))))) {
        if (!hasSessionExpired(sg.target)) {
          assert(false, "an unexpected session expired happened. result = " + result + " targetEvent =  " +sg.target + " sessionEvents = "+sg.sessionEvents)
        }
      } else {
//        println("watcher_flg" +watcher_flg)
        calcOutcomes(result, exc)
      }
    }
  }

  def checkConnectionLossTime(current_timestamp: Long,
                              close_timestamp: Long) = {
    Log.log(List(("current_timestamp", current_timestamp), ("close_timestamp", close_timestamp),
        ("maxsessiontimeout", maxSessionTimeout)), "hasConnectionLoss")
    (current_timestamp - close_timestamp) <= maxSessionTimeout
  }

  def hasConnectionLoss(event:ZKEvent) =
    checkCloseSession(event, checkConnectionLossTime)
    // return whether 'event' can receive CONNECTIONLOSS.
    // TODO: Check the order of events in case we model reopenSession;
    // in that case, CloseSession must precede the "Disconnected" event,
    // which must precede subsequent reconnection attempts.

  def checkCloseSession(event: ZKEvent,
			checkTime:((Long, Long) => Boolean)) = {
    val idx1 = sg.sessionEvents.indexWhere(_.contains(event))
    val idx2 = sg.sessionEvents(idx1).indexWhere(_.action.equals(CloseSession))
    if (idx2 >= 0) {
      val close_timestamp = sg.sessionEvents(idx1)(idx2).timestamp_create
      val current_timestamp = if (watcher_flg) {
        event.timestamp_watcher_triggered
      } else {
        event.timestamp_finish
      }
      checkTime(current_timestamp, close_timestamp)
    } else {
      false
    }
  }

  def checkSessionExpirationTime(current_timestamp: Long,
				 close_timestamp: Long) = {
    Log.log(List(("current_timestamp", current_timestamp), ("close_timestamp", close_timestamp),
      ("minsessiontimeout", minSessionTimeout)), "hasSessionExpired")
    (current_timestamp - close_timestamp) >= minSessionTimeout
  }

  def hasSessionExpired(event:ZKEvent) =
    // return whether 'event' can receive EXPIRED.
    checkCloseSession(event, checkSessionExpirationTime)

  def watchedEventEquals(a:Any, b:Any) = {
    if (a != null && a.isInstanceOf[WatchedEvent] && b != null && b.isInstanceOf[WatchedEvent]) {
      val we1 = a.asInstanceOf[WatchedEvent]
      val we2 = b.asInstanceOf[WatchedEvent]
      we1.getPath() == we2.getPath() && we1.getState() == we2.getState() && we1.getType() == we2.getType()
    } else {
      false || (a == null && b == null)
    }
  }

  def isExpected(calculatedResult: (Option[Any],  Option[Throwable]),
    actualResult : (Option[Any], Option[Throwable]), action : Action,  isTriggered : Boolean) : Boolean = {
    val res = calculatedResult match {
      case (Some(result), None) => {
        actualResult match {
          case (Some(result2), None) =>
            if (action == null || action.isRelevant) {
              if (isTriggered) {
                watchedEventEquals(result, result2)
              } else {
                result.equals(result2)
              }
            } else {
              true
            }
          case _ => false
        }
      }
      case (None, Some(exc)) => {
        actualResult match {
          case (None, Some(exc2)) => exc.equals(exc2)
          case _ => false
        }
      }
      case (None, None) => {
        actualResult match {
          case (None, None) => true
          case _ => false
        }
      }
      case _ => {
        assert(false, "calculatedResult is illegal "+calculatedResult)
        false
      }
    }
    if (!res) {
      Log.log(List(("calc", calculatedResult), ("actual", actualResult), ("action", action)), "not equal")
    }
    res
  }

  override def exploreAction(state: State, i: Int): Boolean = {
    exploreAction(state.asInstanceOf[ZKState], i)
  }

  // check if there is a path such that the watch(that is set by sg.target) is not triggered.
  def checkWatchNotTriggered() = {
    var notTriggered = false
    assert(!hasSessionExpired(sg.target), "the session should expire and the watcher should be triggered")
    val initState = sg.target.initState(sg.sessionEvents)
    setFrontier(initState)
    while (!frontier.isEmpty && !notTriggered) {
      val (state, index) = frontier.get()
      notTriggered = notTriggered || exploreActionForWatcher(state, index)
    }
    assert(notTriggered, "ZK should trigger watcher "+sg.target)
  }

  def exploreActionForWatcher(state: State, i: Int) : Boolean = {
    exploreActionForWatcher(state.asInstanceOf[ZKState], i)
  }

  // it returns false if ZK cannot execute the event (in which case the
  // watcher cannot be triggered) or the watcher is triggered (in which case
  // everything is confirmed) or some events remained unexecuted (in which
  // case the search has to continue),
  // otherwise(i.e. there is no unexecuted events and the watcher has not been triggered yet) returns true.
  def exploreActionForWatcher(state: ZKState, i: Int) : Boolean = {
    val eventIdx = state.pos(i)
    val event = sg.sessionEvents(i)(eventIdx)
    if (!event.isSync || (event eq sg.syncEvents(state.syncAction))) {
      // Log.log(List("event", event), "exploreAction")
      val newState = state.clone
      val (isTriggered, result) = newState.exec2(sg.target, event, watcher_flg)
      if (isTriggered) {
        return false
      } else {
	    if (event.isSync) {
          newState.syncAction = state.syncAction + 1
	    }
	    newState.pos = newState.pos.updated(i, eventIdx + 1)
        if (checkAllEventsExecuted(newState)) {
          return true
        } else if (!visitedStates.contains(newState)) {
          visitedStates += newState
          setFrontier(newState)
	  // continue search
        }
      }
    }
    return false
  }

  // returns true if there is no unexecuted event.
  def checkAllEventsExecuted(state :ZKState): Boolean = {
    (0 until sg.sessionEvents.size).foldLeft(true)((flg, index) => {
      flg && (state.pos(index) >= sg.sessionEvents(index).size)
    })
  }

  def exploreAction(state: ZKState, i: Int): Boolean = {
    val eventIdx = state.pos(i)
    val event = sg.sessionEvents(i)(eventIdx)
    if (!event.isSync || (event eq sg.syncEvents(state.syncAction))) {
      // Log.log(List("event", event), "exploreAction")
      val newState = state.clone
      val (isTriggered, result) = newState.exec2(sg.target, event, watcher_flg)
      if (Search.debug) {
        ZKEvent.saveOrder(newState, i)
      }

      if (event.equals(sg.target) && !watcher_flg) {
        // hit_num += 1
	    // target event
        //Console.err.println("Adding result " + result + " at pos. " + event.tidx)
        if (isExpected(result, (expectedResult, expectedExc), event.action, false)) {
          return true
        }
	    allResults += result
	    // Collect final result or list of results for each transaction step.
	    // TODO: Remember that watch is set from this point onwards
	    // TODO: If watch is set, check if result affects watch
      } else {
        // TODO: Also continue if watcher is set
  	    // TODO: Do not continue if watcher has to be notified after change
        // Log.log(List(("newState", newState)), "Adding successor state")
        if (isTriggered) {
          // Console.out.println("####watcher is triggered")
          if (isExpected(result, (expectedResult, expectedExc), event.action, true)){
            Log.log(List(("actualResult", expectedResult), ("calculatedResult", result._1)), "watcher is triggered")
            return true
          }
	      allResults += result
        } else {
	      if (event.isSync) {
            newState.syncAction = state.syncAction + 1
	      }
	      // Mark the end of a transaction if we reached it
          //Console.err.println("event " + event.id + ", sg.target " + sg.target.id + ", transaction " + event.transaction)
	      newState.pos = newState.pos.updated(i, eventIdx + 1)
          if (Search.PRUNING) {
            val eventResult = sg.results.get(event)
            eventResult match {
              case Some(actualResult) =>
                if (!isExpected(result, actualResult, event.action, false)) {
                  Log.log(List(("event", event), ("result", result), ("actualResult", actualResult)), "pruning")
                  return false
                }
              case _ =>
            }
          }
          if (!visitedStates.contains(newState)) {
            visitedStates += newState
            setFrontier(newState)
	      }
        }
      }
    }
    false
  }
}
