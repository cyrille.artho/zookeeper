package events

class TargetFirstHeuristics(val targetIdx: Int)  extends Heuristics {
  def score(state: State, i: Int) = state.pos(targetIdx)
}
