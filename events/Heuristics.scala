package events

trait Heuristics {
  def score(state: State, i: Int): Int
}
