package events

import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.Assertions._
import org.apache.zookeeper.{KeeperException, OpResult}
import org.apache.zookeeper.data.Id
import org.apache.zookeeper.ZooDefs.Ids

class OutcomeTest extends FlatSpec with Matchers {
  val noSync = List[ZKEvent]()
  val zkIDs = List(Ids.ANYONE_ID_UNSAFE)
  val testData = "testData".getBytes
  val client1Name = "client1"
  val client2Name = "client2"
  Search.PRUNING = false

  "The Outcome of CREATE/SETDATA/DELETE/EXISTS*" should "be false" in {
    val a = ZKEvent.create("/a", Create, zkIDs, client1Name)
    val b = ZKEvent.create("/a", SetData(testData), zkIDs, client1Name)
    val c = ZKEvent.create("/a", Delete, zkIDs, client1Name)
    val Target= ZKEvent.create("/a", Exists, zkIDs, client1Name)
    val sequence = List(List(a,b,c,Target))
    val sg = new ZKSessionGraph(noSync, Target, sequence, null)
    ZKSearch.analyze(sg, Some(false), None)
  }

  "The Outcome of CREATE/SETDATA/EXISTS*/DELETE" should "be true" in {
    val a = ZKEvent.create("/a", Create, zkIDs, client1Name)
    val b = ZKEvent.create("/a", SetData(testData), zkIDs, client1Name)
    val Target= ZKEvent.create("/a", Exists, zkIDs, client1Name)
    val c = ZKEvent.create("/a", Delete, zkIDs, client1Name)
    val sequence = List(List(a,b,Target,c))
    val sg = new ZKSessionGraph(noSync, Target, sequence, null)
    ZKSearch.analyze(sg, Some(true), None)
  }

  "The Outcome of CREATE/EXIST/CREATE/SETDATA/DELETE/EXISTS*" should "be false" in {
    val a = ZKEvent.create("/a", Create, zkIDs, client1Name)
    val a2 = ZKEvent.create("/a", Create, zkIDs, client1Name)
    val b = ZKEvent.create("/a", SetData(testData), zkIDs, client1Name)
    val c = ZKEvent.create("/a", Delete, zkIDs, client1Name)
    val Target= ZKEvent.create("/a", Exists, zkIDs, client1Name)
    val x = ZKEvent.create("/a", Exists, zkIDs, client1Name)
    val sequence = List(List(a,x,a2,b,c,Target))
    val sg = new ZKSessionGraph(noSync, Target, sequence, null)
    ZKSearch.analyze(sg, Some(false), None)
  }

  "The Outcome of CREATE/SETDATA/EXISTS*/DELETE/EXISTS/CREATE" should "be true" in {
    val a = ZKEvent.create("/a", Create, zkIDs, client1Name)
    val b = ZKEvent.create("/a", SetData(testData), zkIDs, client1Name)
    val Target= ZKEvent.create("/a", Exists, zkIDs, client1Name)
    val c = ZKEvent.create("/a", Delete, zkIDs, client1Name)
    val x = ZKEvent.create("/a", Exists, zkIDs, client1Name)
    val a2 = ZKEvent.create("/a", Create, zkIDs, client1Name)
    val sequence = List(List(a,b,Target,c,x,a2))
    val sg = new ZKSessionGraph(noSync, Target, sequence, null)
    ZKSearch.analyze(sg, Some(true), None)
  }

  "The Outcome of DELETE*, case where the node doesn't exist" should "throw a NONODE exception" in {
    val Name = "/a"
    val Target= ZKEvent.create(Name, Delete, zkIDs, client1Name)
    val sequence = List(List(Target))
    val sg = new ZKSessionGraph(noSync, Target, sequence, null)
    ZKSearch.analyze(sg, None, Some(ZKException.create(KeeperException.Code.NONODE, Name)))
  }

  "The Outcome of DELETE*, case where the parent node doesn't exist" should "throw an exception" in {
    val Name = "/a/b"
    val Target= ZKEvent.create(Name, Delete, zkIDs, client1Name)
    val sequence = List(List(Target))
    val sg = new ZKSessionGraph(noSync, Target, sequence, null)
    ZKSearch.analyze(sg, None, Some(ZKException.create(KeeperException.Code.NONODE, Name)))
  }

  "The Outcome of CREATE*" should "be /a,None" in {
    val Name = "/a"
    val Target= ZKEvent.create(Name, Create, zkIDs, client1Name)
    val sequence = List(List(Target))
    val sg = new ZKSessionGraph(noSync, Target, sequence, null)
    ZKSearch.analyze(sg, Some("/a"), None)
  }

  "The Outcome of DELETE* and CREATE/DELETE*" should "be sometimes a NONODE exception" in {
    val Name = "/a"
    val Del = ZKEvent.create(Name, Delete, zkIDs, client1Name)
    val a = ZKEvent.create(Name, Create, zkIDs, client1Name)
    val Target= ZKEvent.create(Name, Delete, zkIDs, client1Name)
    val sequence = List(List(Del),List(a,Target))
    val sg = new ZKSessionGraph(noSync, Target, sequence, null)
    ZKSearch.analyze(sg, Some(()), Some(ZKException.create(KeeperException.Code.NONODE, Name))) // () = Unit return value if there is no exception
  }

  "The Outcome of DELETE*" should "be Unit" in {
    val Name = "/a"
    val a = ZKEvent.create(Name, Create, zkIDs, client1Name)
    val Target= ZKEvent.create(Name, Delete, zkIDs, client1Name)
    val sequence = List(List(a,Target))
    val sg = new ZKSessionGraph(noSync, Target, sequence, null)
    ZKSearch.analyze(sg, Some(()), None)
  }

  "The Outcome of CREATE*" should "be a NONODE exception" in {
    val Name = "/a/b"
    val Target= ZKEvent.create(Name, Create, zkIDs, client1Name)
    val sequence = List(List(Target))
    val sg = new ZKSessionGraph(noSync, Target, sequence, null)
    ZKSearch.analyze(sg, None, Some(ZKException.create(KeeperException.Code.NONODE, Name)))
  }

  "The Outcome of DELETE/DELETE/CREATE*" should "be a node" in {
    val Name = "/a"
    val d = ZKEvent.create(Name, Delete, zkIDs, client1Name)
    val d2 = ZKEvent.create(Name, Delete, zkIDs, client1Name)
    val Target = ZKEvent.create(Name, Create, zkIDs, client1Name)
    val sequence = List(List(d, d2, Target))
    val sg = new ZKSessionGraph(noSync, Target, sequence, null)
    ZKSearch.analyze(sg, Some("/a"), None)
  }

  "The Outcome of CREATE/CREATE*" should "be a NODEEXISTS exception" in {
    val Name = "/a"
    val a = ZKEvent.create(Name, Create, zkIDs, client1Name)
    val Target= ZKEvent.create(Name, Create, zkIDs, client1Name)
    val sequence = List(List(a,Target))
    val sg = new ZKSessionGraph(noSync, Target, sequence, null)
    ZKSearch.analyze(sg, None, Some(ZKException.create(KeeperException.Code.NODEEXISTS, Name)))
  }

  "The outcome of this test" should "include {/d} in the sets of outcomes" in {
    val e4 = ZKEvent.create("/a", Create, zkIDs, client2Name)
    val e5 = ZKEvent.create("/a/d", Create, zkIDs, client1Name)
    val e6 = ZKEvent.create("/a", GetChildren, zkIDs, client1Name)
    val e7 = ZKEvent.create("/a/e", Create, zkIDs, client2Name)
    val paths = List(List(e5, e6), List(e4, e7))
    val sg = new ZKSessionGraph(noSync, e6, paths, null)
    ZKSearch.analyze(sg, Some(Set("d")), None)
  }

  "The Outcome of DELETE and (CREATE/DELETE)*" should "be always ok" in {
    val Name = "/a"
    val Del = ZKEvent.create(Name, Delete, zkIDs, client1Name)
    val Crt = ZKEvent.create(Name, Create, zkIDs, client1Name)
    val Target = ZKEvent.create(Name, Delete, zkIDs, client1Name)
//    ZKEvent.transaction(Crt, Target)
//    val sequence = List(List(Del), List(Crt, Target))
//    val outcome = new SessionGraph(noSync, Target, sequence).txOutcomes
//    outcome should be (List("/a", ()), List(None,None))
    // () = Unit return value if there is no exception
  }

  "The Outcome of (CREATE/DELETE/CREATE/DELETE) and (CREATE/DELETE/CREATE)*" should "be always ok" in {
    val Name = "/a"
    val Crt0 = ZKEvent.create(Name, Create, zkIDs, client1Name)
    val Del0 = ZKEvent.create(Name, Delete, zkIDs, client1Name)
    val Crt0_ = ZKEvent.create(Name, Create, zkIDs, client1Name)
    val Del0_ = ZKEvent.create(Name, Delete, zkIDs, client1Name)
    val Crt = ZKEvent.create(Name, Create, zkIDs, client1Name)
    val Del = ZKEvent.create(Name, Delete, zkIDs, client1Name)
    val Target = ZKEvent.create(Name, Create, zkIDs, client1Name)
//    ZKEvent.transaction(Crt0, Del0, Crt0, Del0_)
    // the longer transaction should not be stored in result list,
    // as it does not contain the target
//    ZKEvent.transaction(Crt, Del, Target)
//    val sequence = List(List(Del0, Crt0, Del0_), List(Crt, Del, Target))
//    val outcome = new SessionGraph(noSync, Target, sequence).txOutcomes
 //   outcome should be (List("/a", (), "/a"), List(None,None,None))
    // () = Unit return value if there is no exception
  }

  "The Outcome of (CREATE/DELETE)" should "List(\"/a\", ())" in {
    val Name = "/a"
    val a = ZKEvent.create(Name, Create, zkIDs, client1Name)
    val b = ZKEvent.create(Name, Delete, zkIDs, client1Name)
    val Target = ZKEvent.transaction(zkIDs, client1Name, List(a,b))
    val sequence = List(List(Target))
    val sg = new ZKSessionGraph(noSync, Target, sequence, null)
    ZKSearch.analyze(sg, Some(OpResultList(List(new OpResult.CreateResult("/a"), new OpResult.DeleteResult()))), None)
  }

  "The Outcome of (CREATE/CREATE)" should "throw a NodeExistsException" in {
    val Name = "/a"
    val a = ZKEvent.create(Name, Create, zkIDs, client1Name)
    val b = ZKEvent.create(Name, Create, zkIDs, client1Name)
    val Target = ZKEvent.transaction(zkIDs, client1Name, List(a,b))
    val sequence = List(List(Target))
    val sg = new ZKSessionGraph(noSync, Target, sequence, null)
    ZKSearch.analyze(sg, None, Some(ZKException.create(KeeperException.Code.NODEEXISTS, Name)))
  }
}
