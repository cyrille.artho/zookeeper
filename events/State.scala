package events

trait State extends Cloneable {
  var syncAction: Int
  var pos: List[Int] // active event in connection i
  // var transaction: Int // currently outside transaction if -1
  // value >= 0 means pending transaction on session x

  override def clone: State = throw new CloneNotSupportedException

  def exec(e: Event): (Option[Any], Option[Throwable])
}
