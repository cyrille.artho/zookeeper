package events

import modbat.Tree
import org.apache.zookeeper.data.Id

import scala.collection.mutable.{HashSet, LinkedHashSet, ListBuffer, Queue}

object SessionGraph {
  // repeated events cannot be filtered out in general as interleaved
  // events may interact with them.
  // def uniq(xs: List[ZKEvent]) = (List[Int]() /: xs) {(ys, y) => if (ys.size > 0 && ys.last == y) ys else ys :+ y}

  def toStr(ref: Any) = {
    if (ref == null) "(null)" else ref.toString
  }
}

trait SessionGraph[E <: Event]{
  import SessionGraph._
  val target: E
  val results: Map[E, (Option[Any], Option[Throwable])]
  def ppGraph(syncEvents: List[Event], graph: List[List[Event]]) {
    var i = 0
    for (events <- graph) {
      i += 1
      Console.out.println("Connection " + i + ": " + events.mkString(", "))
    }
    Console.out.println("Sync events: " + syncEvents.mkString(", "))
  }
  val sessionEvents : List[List[E]]
  val syncEvents : Array[E]
}
