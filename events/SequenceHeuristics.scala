package events

class SequenceHeuristics(val ids: List[Int])  extends Heuristics {
  def score(state: State, i: Int) = {
    if (ids(state.pos.sum) == ids(state.pos(i))) 1 else 0
  }
}
