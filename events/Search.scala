package events

import modbat.Tree

import scala.collection.mutable.{HashSet, LinkedHashSet, ListBuffer, PriorityQueue, Stack, Queue}
import org.apache.zookeeper.KeeperException
import java.io.{PrintWriter, FileOutputStream,OutputStreamWriter}
import scala.io.Source

sealed abstract class SearchAlgorithm
case object NoOracle extends SearchAlgorithm
case object DFS extends SearchAlgorithm
case object BFS extends SearchAlgorithm
case object HG_Create extends SearchAlgorithm // Hänsel und Gretel(use timestamp_create)
case object HG_Finish extends SearchAlgorithm // Hänsel und Gretel(use timestamp_finish)
case object HG_Create_Strict extends SearchAlgorithm // Hänsel und Gretel(use timestamp_create)
case object HG_Finish_Strict extends SearchAlgorithm // Hänsel und Gretel(use timestamp_finish)
case object Combined extends SearchAlgorithm // combined heuristics
import modbat.dsl._

object Search {
  private val algo_str = Source.fromFile("./settings.log").getLines.toList(0)
  var SEARCH_ALGO =
    algo_str match {
      case "nooracle" => NoOracle
      case "dfs" => DFS
      case "bfs" => BFS
      case "create" => HG_Create
      case "finish" => HG_Finish
      case "create_strict" => HG_Create_Strict
      case "finish_strict" => HG_Finish_Strict
      case _ => BFS
    }
  Log.log(List(("algo", SEARCH_ALGO)))
  var PRUNING = Source.fromFile("./settings.log").getLines.toList(1) == "1"

  def toStr(ref: Any) = {
    if (ref == null) "(null)" else ref.toString
  }
  var debug = false
}

class Search[E <: Event](val sg: SessionGraph[E]) extends Model { // TODO: remove 'extends Model' after modbat's update
  import SessionGraph._
  val currentStates = new Stack[State]()
  val currentStates2 = new Stack[(State, Int)]()
  val currentStates3 = new PriorityQueue[(State, Long)]()(Ordering.by(_._2))
  val visitedStates = new HashSet[State]()
  val allResults = new LinkedHashSet[(Any, Option[Throwable])]
  var expectedResult: Option[Any] = None
  var expectedExc: Option[Throwable] = None
  val ids = sg.sessionEvents.flatten.map(_.id).sorted
  val targetIdx = sg.sessionEvents.indexWhere(_.contains(sg.target))
  val heuristics = new CombinedHeuristics(targetIdx, ids)
  val frontier = createFrontier

  abstract class Frontier {
    def get() : (State, Int)
    def set(s:(State, Int)) : Unit
    def isEmpty : Boolean
    def toString : String
  }

  class DFSFrontier extends Frontier{
    val stack = new Stack[(State, Int)]()
    def get() = stack.pop
    def set(s : (State, Int)) = stack.push(s)
    def isEmpty = stack.isEmpty
    override def toString = stack.toString
  }

  class BFSFrontier extends Frontier{
    val queue = new Queue[(State, Int)]()
    def get() = queue.dequeue
    def set(s : (State, Int)) = queue += s
    def isEmpty = queue.isEmpty
    override def toString = queue.toString
  }

  class HG_Frontier(ordering: Ordering[(State, Int)]) extends Frontier{
    val stack = new Stack[(State, Int)]()
    val temporary_queue =  new PriorityQueue[(State, Int)]()(ordering)
    def get() = {
      /* prefer successor events of most recently explored event;
	 hence, copy all successors into a stack and let new successors
	 (which will be added after the newly returned state is explored)
	 take priority of other events in the current frontier */
      while(!temporary_queue.isEmpty) {
        stack.push(temporary_queue.dequeue)
      }
      stack.pop
    }
    def set(s : (State, Int)) = {
      temporary_queue += s
    }
    def isEmpty = stack.isEmpty && temporary_queue.isEmpty
    override def toString = "stack: "+stack.map(x => ((x._1.pos, x._2))).toString + "queue :"+temporary_queue.map(x => ((x._1.pos, x._2))).toString
  }

  class HG_Frontier_Strict(ordering: Ordering[(State, Int)]) extends HG_Frontier(ordering) {
    // use only a priority queue
    override val temporary_queue =  new PriorityQueue[(State, Int)]()(ordering.reverse)
    override def get() = {
      // println(this)
      temporary_queue.dequeue
    }
  }

  class HG_CreateFrontier extends HG_Frontier(Ordering.by[(State, Int), Long](x => sg.sessionEvents(x._2)(x._1.pos(x._2)).timestamp_create))
  class HG_FinishFrontier extends HG_Frontier(Ordering.by[(State, Int), Long](x => sg.sessionEvents(x._2)(x._1.pos(x._2)).timestamp_finish))
  class HG_CreateFrontier_Strict extends HG_Frontier_Strict(Ordering.by[(State, Int), Long](x => sg.sessionEvents(x._2)(x._1.pos(x._2)).timestamp_create))
  class HG_FinishFrontier_Strict extends HG_Frontier_Strict(Ordering.by[(State, Int), Long](x => sg.sessionEvents(x._2)(x._1.pos(x._2)).timestamp_finish))


  def txOutcomes(result: Option[Any], exc: Option[Throwable]) = {
    calcOutcomes(result, exc)
  }

  def outcomes(result: Option[Any], exc: Option[Throwable]) = {
//  def outcomes(result: Any, exc: Option[Throwable]) = {
    // not a transaction: one set of results
    Log.log(List(("search_algo", Search.SEARCH_ALGO)), "outcomes")
    if (Search.SEARCH_ALGO != NoOracle) {
      calcOutcomes(result, exc)
    }
  }

  def predictedResults = {
    "predicted results (return value, exception):\n" +
    allResults.map(x => "(" + toStr(x._1) + ", " + {
	  x._2 match {
		case None => "(no exception)"
		case Some(e: Throwable) => e.toString
	  } }
	  + ")").
	  mkString(",\n")
  }

  def calcOutcomes(result: Option[Any], exc: Option[Throwable]) {
    val initState = sg.target.initState(sg.sessionEvents)

    var found = false
    var l = sg.sessionEvents.size
    expectedResult = result
    expectedExc = exc
    Log.log(List(("target", sg.target), ("result", result), ("exc", exc), ("sessionEvents", sg.sessionEvents), ("results", sg.results)), "calcOutcomes")
    setFrontier(initState)
    // println("###frontier "+ frontier.toString)
    while (!frontier.isEmpty && !found) {
      val (state, index) = frontier.get()
      found = found || exploreAction(state, index)
    }

    if (!found) {
      exc match {
	    case Some(e) => {
          Log.log(List(("target", sg.target)), "assert")
          assert(false, "Actual exception: " + e + ", " + predictedResults)
	    }
        case None => {
          assert(false, "Actual result: " + toStr(result) + "(no exception), " + predictedResults+", target "+sg.target)
        }
      }
    }
  }

  // check if the calculated result is eqauls to the actual result
  def isExpected(calculatedResult: (Option[Any],  Option[Throwable]),
    actualResult : (Option[Any], Option[Throwable]), action : Action) : Boolean = {
    calculatedResult match {
      case (Some(result), None) => {
        actualResult match {
          case (Some(result2), None) =>
            if (action.isRelevant) {
              result.equals(result2)
            } else {
              true
            }
          case _ => false
        }
      }
      case (None, Some(exc)) => {
        actualResult match {
          case (None, Some(exc2)) => exc.equals(exc2)
          case _ => false
        }
      }
      case (None, None) => {
        actualResult match {
          case (None, None) => true
          case _ => false
        }
      }
      case _ => {
        assert(false, "calculatedResult is illegal")
        false
      }
    }
  }



  def exploreAction(state: State, i: Int): Boolean = {
    val eventIdx = state.pos(i) // get index of current event in connection i
    val event = sg.sessionEvents(i)(eventIdx) // get current event in conn. i
    if (!event.isSync || (event eq sg.syncEvents(state.syncAction))) {
      // Log.log(List("event", event), "exploreAction")
      val newState = state.clone
      val result = newState.exec(event)
      if (event.equals(sg.target)) {
	    // target event
        // Log.log(List(("result", result), ("pos", pos)), "adding result at pos")
        if (isExpected(result, (expectedResult, expectedExc), event.action)) {
          return true
        }
	    allResults += result
	    // Collect final result or list of results for each transaction step.
      }
      if (event ne sg.target) {
        // Log.log(List(("newState", newState)), "Adding successor state")
        if (event.isSync) {
          newState.syncAction = state.syncAction + 1
	}
	    // Mark the end of a transaction if we reached it
        // Log.log(List(("event", event.id), ("target", sg.target.id), ("trasanction", event.transactionb)), "transaction")
	newState.pos = newState.pos.updated(i, eventIdx + 1)
	// update index of event at connection i
        if (Search.PRUNING) {
            val eventResult = sg.results.get(event)
            eventResult match {
              case Some(actualResult) =>
                if (!isExpected(result, actualResult, event.action)) {
                  Log.log(List(("event", event), ("result", result), ("actualResult", actualResult)), "pruning")
                  return false
                }
              case _ =>
            }
        }
        if (!visitedStates.contains(newState)) {
          visitedStates += newState
          setFrontier(newState)
	}
      }
    }
    false
  }

  /* generate all successors of new state: for each connection,
     generate an entry with the given new state and new index/position */
  def setFrontier(s : State) {
    val l = sg.sessionEvents.size
    var i = 0
    while (i < l){ // for each connection
      if (s.pos(i) < sg.sessionEvents(i).size) {
	// more events belonging to that connection
        frontier.set((s, i)) // add new state at connection i
      } // skip new state if event is past last event
      i += 1
    }
  }

  def createFrontier: Frontier = {
    Search.SEARCH_ALGO match {
      case DFS => new DFSFrontier()
      case HG_Create => new HG_CreateFrontier()
      case HG_Finish => new HG_FinishFrontier()
      case HG_Create_Strict => new HG_CreateFrontier_Strict()
      case HG_Finish_Strict => new HG_FinishFrontier_Strict()
      case _ => new BFSFrontier()
    }
  }
}
