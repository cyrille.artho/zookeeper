package events

class ZKSessionGraph(synchEvents: List[ZKEvent], val target: ZKEvent,
  graph: List[List[ZKEvent]], val results: Map[ZKEvent, (Option[Any], Option[Throwable])])
    extends SessionGraph[ZKEvent] {
  private val f2 = (target.filter2 _).curried(graph)
  val sessionEvents = graph.map(_.filter(f2)).filter(! _.isEmpty)
  val syncEvents = synchEvents.filter(f2).toArray
}
