package events

import org.apache.zookeeper.KeeperException

object ZKException {
  var useRefEq = false

  def excEquals(thisExc: Throwable, thatExc: Throwable): Boolean = {
    var equal = thisExc.getClass.equals(thatExc.getClass)
    if (thisExc.getCause != null) {
      equal = equal && thisExc.getCause.equals(thatExc.getCause)
    } else if (thatExc.getCause != null) {
      equal = false
    }
    if (thisExc.getMessage != null) {
      equal = equal && thisExc.getMessage.equals(thatExc.getMessage)
    } else if (thatExc.getCause != null) {
      equal = false
    }
    if (thisExc.isInstanceOf[KeeperException]) {
      if (thatExc.isInstanceOf[KeeperException]) {
        if (thisExc.asInstanceOf[KeeperException].getPath != null) {
          equal = equal && thisExc.asInstanceOf[KeeperException].getPath.equals(thatExc.asInstanceOf[KeeperException].getPath)
        } else if (thatExc.asInstanceOf[KeeperException].getPath != null) {
          equal = false
        }
      } else {
        equal = false
      }
    }

    return equal
  }

  def create(code: KeeperException.Code, path: String) = new ZKException(KeeperException.create(code, path))

  def useRefEquality(setting: Boolean) {
    useRefEq = setting
  }
}

class ZKException(val exception: Throwable) extends Exception {
  override def toString = exception.toString

  override def equals(that : Any) = {
    if (ZKException.useRefEq) {
      this == that
    } else that match {
      case that: ZKException => (that canEqual this) && ZKException.excEquals(exception, that.exception)
      case _ => false
    }
  }

  def canEqual(other: Any) = other.isInstanceOf[ZKException]

  override def hashCode: Int = {
    if (exception.isInstanceOf[KeeperException]) {
      return exception.asInstanceOf[KeeperException].getPath.##
    }
    else {
      return exception.getCause.## ^ exception.getMessage.##
    }
  }
}
