package events

import org.apache.zookeeper.data.{ACL, Id}
import org.apache.zookeeper.Op
import org.apache.zookeeper.ZooDefs.OpCode
import modbat.{Tree, Node}
import scala.collection.mutable.{ListBuffer}
import org.apache.zookeeper.data.Id
import org.apache.zookeeper.Watcher.Event.{KeeperState, EventType}

sealed abstract class ZKAction extends Action {
  def isRelevant : Boolean = {
    this match {
      case SetData(_) => false // we do not check the result of setData
      case SetACL(_) => false // we do not check the result of setACL
      case _ => true
    }
  }
}

case object Create extends ZKAction
case object Delete extends ZKAction
case object Exists extends ZKAction
case object GetChildren extends ZKAction
case class SetData(data: Array[Byte]) extends ZKAction
case object GetData extends ZKAction
case object GetACL extends ZKAction
case class SetACL(aclBuffer: ListBuffer[ACL]) extends ZKAction
case object Check extends ZKAction
case object Sync extends ZKAction
case class Multi(ops: List[ZKEvent]) extends ZKAction
case object CloseSession extends ZKAction
case object Undef extends ZKAction

object ZKEvent {
  var n = 0

  def toZKEvent(op: Op, zkIDs: List[Id], clientName: String) = {
    op.getType() match {
      case OpCode.create => create(op.getPath(), Create, zkIDs, clientName)
      case OpCode.delete => create(op.getPath(), Delete, zkIDs, clientName)
      case OpCode.setData => create(op.getPath(), SetData("testData".getBytes), zkIDs, clientName)
      // TODO: op.data as is private, use reflection to access
    }
  }

  def transaction(zkIDs: List[Id], clientName: String, actions: Op*) = {
    ZKEvent.create("", Multi(actions.toList.map(toZKEvent(_, zkIDs, clientName))), zkIDs, clientName)
  }

  def transaction(zkIDs: List[Id], clientName: String, ops: List[ZKEvent]) = {
    ZKEvent.create("", Multi(ops), zkIDs, clientName)
  }

  def create (name: String, action: ZKAction, zkIDs: List[Id], clientName : String, watcher : Boolean = false) = {
    val e = new ZKEvent
    e.name = name
    e.action = action
    e.zkIDs = zkIDs
    e.watcher = watcher
    e.clientName = clientName
    e
  }

  def initState(sessionEvents: List[List[Event]]) =
    new ZKState(0, new Tree, List.fill(sessionEvents.size)(0))

  // For testing search orders
  val order = new ListBuffer[(List[Int], Int)]() // the order of search
  def saveOrder(s : ZKState, sessionIdx : Int) = { // save the order of search
    order += ((s.pos, sessionIdx))
  }
}

class ZKEvent extends Event {
  var name: String = _
  var action: ZKAction = _; def action_=(a:Action) = ???
  var data: Array[Byte] = null
  ZKEvent.n += 1
  var zkIDs:List[Id] = null
  val id = ZKEvent.n
  var timestamp_create = System.currentTimeMillis
  var timestamp_finish:Long = 0
  var timestamp_watcher_triggered:Long = 0
  var watcher:Boolean = _
  var clientName:String = _

  def isPure = (action == Exists || action == GetChildren || action == GetData || action == GetACL)
  // TODO: Add code for Multi, is pure if all ops are pure

  def initState(sessionEvents: List[List[Event]]) =
    ZKEvent.initState(sessionEvents)

  def filter2(graph : List[List[ZKEvent]], e: ZKEvent) : Boolean = {
    // if there is a Delete event for an ancestor of 'this.name', we do not use 'filter'.
    if (graph.flatten.indexWhere(e2 => e2.action == Delete && Node.isAncestor(e2.name, this.name)) >= 0) {
      (e == this) || (!e.isPure)
    } else {
      filter(e)
    }
  }

  /** Filter out event e under certain circumstances.
    * 1. Keep this event (target).
    * 2. Keep events that are not pure (change the state) and that affect
    *    the same node as this event, or an ancestor of the target node.
    *    In case of GetChildren and Delete, we also have to preserve the
    *    descendants, as the result of the target operation depends on them. */
  def filter(e: ZKEvent) = {
    if (this.action == GetChildren || this.action == Delete) {
      (e == this) || (!e.isPure && (Node.isAncestor(e.name, this.name) || Node.isAncestor(this.name, e.name) || e.name == this.name))
    } else {
      (e == this) || (!e.isPure && (Node.isAncestor(e.name, this.name) || e.name == this.name))
    }
  }

  def filter(e: Event) = {
    if (!e.isInstanceOf[ZKEvent]) false
    else {
      filter(e.asInstanceOf[ZKEvent])
    }
  }

  override def toString = {
    if (isSync) {
      // "Synchronous " + action + " " + name + ", id = " + id+", ts_create = "+ timestamp_create+", ts_finish = "+timestamp_finish
      "Synchronous " + action+" "+name + ", id = " + id
    } else {
      // action + " " + name + ", id = " + id+", ts_create = "+ timestamp_create+", ts_finish = "+timestamp_finish
      action + " " + name + ", id = " + id
    }
  }


  override def equals(that: Any) = {
    if ((that != null) && (that.isInstanceOf[ZKEvent])) {
      val other = that.asInstanceOf[ZKEvent]
      other.id == id // each id is unique
      // update modbat.ZKClient.addEvent if id is not used anymore in equals
    } else {
      false
    }
  }

  override def hashCode() = {
    action.hashCode() ^ super.hashCode() ^ id
  }

  def getAction = {
    action
  }
}
