package events

trait Action {
  def isRelevant : Boolean
}

trait Event {
  var isSync: Boolean = false
  val id: Int
  var action : Action
  var timestamp_create : Long
  var timestamp_finish : Long
  def filter(e: Event): Boolean

  def initState(sessionEvents: List[List[Event]]): State
  /** Return new init. state that is compatible with semantics of
      subclassed events. */
}
