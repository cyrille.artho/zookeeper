package events

class CombinedHeuristics(targetIdx: Int, ids: List[Int]) extends Heuristics {
  val tfh = new TargetFirstHeuristics(targetIdx)
  val sh = new SequenceHeuristics(ids)
  def score(state: State, i: Int) = tfh.score(state, i) + 2 * sh.score(state, i)
}
