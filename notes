== Notes on test models ==

-- Testing asynchronous actions --

Various possibilities:

(1) Just call but don't verify (crash testing).
    Simple but still requires checks against non-interference with
    other (synchronous) operations that verify their result.

(2) Call, sleep, verify: Reduces problem to synchronous case.
    Does not test cases of interfering operations.

(3) Restrict operations to "private" node namespace for each model
    instance (client).
    Still has possible issues with multiple operations in the same model.
    Does not seem to solve the main problem.

(4) Operations on given nodes are only monotonically increasing (decreasing)
    or idempotent.
    Examples: Increment counter value as data but never delete node;
    start with large value, decrement or delete but never add/increment.

(5) Keep a set of pending operations for each node; whenever there are any
    pending operations, do not verify result.
