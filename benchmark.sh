#!/bin/sh
# benchmark ZK model

J=2
while [ $J -le 4 ]
do
	I=1
	while [ $I -le 10 ]
	do
		echo Size $J, run $I...
		./run.sh 10000 $J $I >& run-w-opt-10000-$J-$I.log
		gzip run-w-opt-10000-$J-$I.log
		I=`expr $I + 1`
		sleep 2
	done
	J=`expr $J + 1`
done
