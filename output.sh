#!/bin/sh
# gather output of ZK model benchmark

J=2
while [ $J -le 4 ]
do
	I=1
	echo Size $J
	gunzip -c run-w-opt-10000-$J-$I.log.gz | \
		grep '^.INFO' | grep 'executed\|covered'
	while [ $I -le 10 ]
	do
		gunzip -c run-w-opt-10000-$J-$I.log.gz | tail -1
		I=`expr $I + 1`
	done
	J=`expr $J + 1`
done
