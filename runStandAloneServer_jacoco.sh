testCount=10000

ZKLIBS=`ls -d zookeeper*/conf`:`ls zookeeper*/lib/*.jar | tr '\n' :`
export CLASSPATH=${ZKLIBS}.:jacoco_target/instr:jacoco_lib/jacocoagent.jar:zookeeper-3.4.8/zookeeper-3.4.8.jar
echo ${CLASSPATH}
time scala \
	-Djacoco-agent.destfile="./jacoco_target/jacoco.exec${testCount}" \
	-Djacoco-agent.append=true \
        -Dlog4j.configDebug=true \
	-Dlog4j.debug=true \
        -Dlog4j.configuration="log4j.properties" \
	-Dzookeeper.preAllocSize=1024 \
	lib/openmodbat.jar \
        -s=1 \
        -n="${testCount}" \
        --loop-limit=4 \
        --print-stack-trace \
        --auto-labels \
	--dotify-coverage \
	--log-level=fine \
	--no-redirect-out \
        modbat.ZKServer
# --stop-on-failure \
# remove Zookeeper transactions logs

rm -f temp/zookeeper/version-2/log.*


# generate coverage report of modbat and arvhive it

if [ -f "./jacoco_target/jacoco.exec${testCount}" ]; 
	then 
	mv "./jacoco_target/jacoco.exec${testCount}" ./jacoco_target/jacoco.exec
	ant report
	tar czvf "./jacoco_target/report_${testCount}.tgz" ./jacoco_target/report
	rm -r ./jacoco_target/report ./jacoco_target/jacoco.exec 
fi
