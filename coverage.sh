#!/bin/sh
# convert dot files to PDF

for D in *.dot
do
	P="`echo $D | sed -e 's/\.dot$//'`"
	dot -Tps < "$D" > "$P.ps"
	ps2epsi $P.ps
	mv $P.eps{i,}
	epstopdf $P.eps
done
#	%s/penwidth = "3.0"
