#!/bin/sh
# convert dot files to PDF

for D in *.dot
do
	dot -Tps < "JavanioClientModel.dot" > "JavanioClientModel.ps"
	ps2epsi JavanioClientModel.ps
	mv JavanioClientModel.eps{i,}
	epstopdf JavanioClientModel.eps
done
#	%s/penwidth = "3.0"
