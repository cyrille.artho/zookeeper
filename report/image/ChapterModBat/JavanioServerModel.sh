#!/bin/sh
# convert dot files to PDF

for D in *.dot
do
	dot -Tps < "JavanioServerModel.dot" > "JavanioServerModel.ps"
	ps2epsi JavanioServerModel.ps
	mv JavanioServerModel.eps{i,}
	epstopdf JavanioServerModel.eps
done
#	%s/penwidth = "3.0"
