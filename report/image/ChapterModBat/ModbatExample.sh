#!/bin/sh
# convert dot files to PDF

for D in *.dot
do
	dot -Tps < "ModbatExample.dot" > "ModbatExample.ps"
	ps2epsi ModbatExample.ps
	mv ModbatExample.eps{i,}
	epstopdf ModbatExample.eps
done
#	%s/penwidth = "3.0"
