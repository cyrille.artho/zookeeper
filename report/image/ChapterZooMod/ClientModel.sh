#!/bin/sh
# convert dot files to PDF

for D in *.dot
do
	dot -Tps < "ClientModel.dot" > "ClientModel.ps"
	ps2epsi ClientModel.ps
	mv ClientModel.eps{i,}
	epstopdf ClientModel.eps
done
#	%s/penwidth = "3.0"
