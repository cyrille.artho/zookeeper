#!/bin/sh
# convert dot files to PDF

for D in *.dot
do
	dot -Tps < "ClientModelComplex.dot" > "ClientModelComplex.ps"
	ps2epsi ClientModelComplex.ps
	mv ClientModelComplex.eps{i,}
	epstopdf ClientModelComplex.eps
done
#	%s/penwidth = "3.0"
