#!/bin/sh
# convert dot files to PDF

for D in *.dot
do
	dot -Tps < "ClientConnectionModel.dot" > "ClientConnectionModel.ps"
	ps2epsi ClientConnectionModel.ps
	mv ClientConnectionModel.eps{i,}
	epstopdf ClientConnectionModel.eps
done
#	%s/penwidth = "3.0"
