\chapter{Zookeeper}
Apache ZooKeeper is an effort to develop and maintain an open-source server which enables highly reliable distributed coordination.
 It is a centralized service for maintaining configuration information, naming, providing distributed synchronization, and providing group services.
 All of these kinds of services are used in some form or another by distributed applications.
 Each time they are implemented there is a lot of work that goes into fixing the bugs and race conditions that are inevitable.

Because of the difficulty of implementing these kinds of services,
 applications initially usually skimp on them, which make them brittle in the presence of change and difficult to manage.
 Even when done correctly, different implementations of these services lead to management complexity when the applications are deployed.

ZooKeeper is used Ebay, Yahoo, Rackspace, Akka, Zynga, etc.

	\section{Principles}
ZooKeeper exposes a simple set of primitives that distributed applications can build upon to implement higher level services for synchronization,
 configuration maintenance, and groups and naming.
 It is designed to be easy to program to, and uses a data model styled after the familiar directory tree structure of file systems.
 It runs in Java and has bindings for both Java and C.

Coordination services are notoriously hard to get right.
 They are especially prone to errors such as race conditions and deadlock.
 The motivation behind ZooKeeper is to relieve distributed applications the responsibility of implementing coordination services from scratch.

		\subsection{ZooKeeper structure}

\begin{figure}[h]
\centering
\includegraphics{image/ChapterZooKeeper/zkservice.jpg}
\caption{Strucuture of ZooKeeper}
\end{figure}

A ZooKeeper service is composed of a lot of servers that share the same data structure.
 One of this server is the main server, it has to propagate the information for each server in order to have the same data.

All the server except the main server can handle the connection of several clients.
 Because of this structure, the service is resilient to the crash of a server.
 Indeed, in one of the server crash, the clients that were connected to his server just have to be connected again to another server,
 and as all the server share the same data structure, we do not loose any information.

		\subsection{Data model}
The name space provided by ZooKeeper is much like that of a standard file system.
 A name is a sequence of path elements separated by a slash (/).
 Every node in ZooKeeper's name space is identified by a path.
 The data structure can be seen as a tree.

A particularity of this tree is that each time we try to set a data inside a node,
 all the data that is contains in this node is deleted and replaced by the new one.

Also, ZooKeeper allows two particular kind of node: permanent and ephemeral nodes.
 The ephemeral node do not allow the creation of a child,
 and once the client disconnect,
 all the ephemeral nodes that this client has created are deleted, with all the data inside.

\begin{figure}[h]
\centering
\includegraphics{image/ChapterZooKeeper/zknamespace.jpg}
\caption{Data structure of ZooKeeper}
\end{figure}

	\section{The actions performed}

One of the design goals of ZooKeeper is to provide a very simple programming interface.
 As a result, it supports only these operations:
\begin{itemize}
\item {\bfseries create}: creates a node at a location in the tree
\item {\bfseries delete}: deletes a node
\item {\bfseries exists}: tests if a node exists at a location
\item {\bfseries get data}: reads the data from a node
\item {\bfseries set data}: writes data to a node
\item {\bfseries get children}: retrieves a list of children of a node
\item {\bfseries sync}: waits for data to be propagated
\end{itemize}

		\subsection{Exceptions}
To understand better the next step,
 it is important to define the different exceptions that our 6 actions could throw.
 the server, before performing the action called,
 will check the data structure and throw a ZooKeeper exception if the requirements to perform the action are not respected.

\begin{itemize}
  \item {\bfseries create}
    \begin{itemize}
      \item {NoNode}: The node inside we try to create a new node does not exist.
      \item {NodeAlreadyExists}: The node that we try to create already exists.
      \item {NoNodeForEphemerals}: The parent node is an ephemeral node and does not accept the creation of a node inside his path.
    \end{itemize}
\item {\bfseries delete}
    \begin{itemize}
      \item {NoNode}: The node we try to delete does not exist.
      \item {NodeNotEmpty}: The node is not empty.
    \end{itemize}
\item {\bfseries set data}
    \begin{itemize}
      \item {NoNode}: The node does not exist, the data cannot be set.
    \end{itemize}
\item {\bfseries exists}
\item {\bfseries get data}
    \begin{itemize}
      \item {NoNode}: The node does not exist.
    \end{itemize}
\item {\bfseries get children}
    \begin{itemize}
      \item {NoNode}: The node does not exist.
    \end{itemize}
\end{itemize}

		\subsection{Synchronous and asynchronous actions}
We choose to use all the actions of ZooKeeper except sync, so we have 3 writing actions and 3 reading actions.
 For each of these actions we have the choice to execute them asynchronously, or synchronously.

In ZooKeeper the synchronous action(Figure 3.3) set the client in the blocking mode,
 in other word he will wait for the callback of his previous action called before calling another action.
 the client cannot call for another action, synchronous or asynchronous, until the callback.

\begin{figure}[h]
\centering
\includegraphics[scale=0.70]{image/ChapterZooKeeper/Synchronous_action.pdf}
\caption{Synchronous action}
\end{figure}

With the asynchronous action(Figure 3.4), the client can call for another action without waiting for the callback of the previous action called.
 It means that he can call as many asynchronous action as he wants.

\begin{figure}[h]
\centering
\includegraphics[scale=0.70]{image/ChapterZooKeeper/Asynchronous_action.pdf}
\caption{Asynchronous action}
\end{figure}

		\subsection{Guarantees}
ZooKeeper is very fast and very simple. Since its goal, though,
 is to be a basis for the construction of more complicated services,
 such as synchronization, it provides a set of guarantees. These are:
\begin{itemize}
\item Sequential Consistency - Updates from a client will be applied in the order that they were sent.
 So, if a client call a lot of asynchronous action,
 they will be perform by the server in the same order that they were sent(Figure 3.5).
\item Atomicity - Updates either succeed or fail. No partial results.
\item Single System Image - A client will see the same view of the service regardless of the server that it connects to.
\item Reliability - Once an update has been applied, it will persist from that time forward until a client overwrites the update.
\item Timeliness - The clients view of the system is guaranteed to be up-to-date within a certain time bound.
\end{itemize}

\begin{figure}[h]
\centering
\includegraphics[scale=0.75]{image/ChapterZooKeeper/GuaranteeAsyncAction.pdf}
\caption{Several asynchronous actions performed by one client}
\end{figure}

		\subsection{A simple example}
Here a simple example using ZooKeeper, with two nodes:
 the root and the node /a that will be the initial state of our data.

\begin{figure}[!h]
\centering
\includegraphics[scale=0.75]{image/ChapterZooKeeper/InitialState.pdf}
\end{figure}

From this state, two different clients connected to the same server will perform a writing synchronous action,
 that will change the data of the server. Of course the actions could be more complex,
 we will see in the next chapter a lot of possibility and why this will be a problem to check the model that we will create.

\begin{figure}[!h]
\centering
\includegraphics[scale=0.75]{image/ChapterZooKeeper/SimpleExZooKeeper.pdf}
\caption{A use of ZooKeeper}
\end{figure}