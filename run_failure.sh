#!/bin/sh
ZKLIBS=`ls zookeeper*/*.jar | head -1`:`ls -d zookeeper*/conf`:`ls zookeeper*/lib/*.jar | tr '\n' :`
CLASSPATH=${ZKLIBS}. \
time scala \
	-Dlog4j.configDebug=true \
	-Dlog4j.debug=true \
        -Dlog4j.configuration="log4j.properties" \
	-Dzookeeper.preAllocSize=1024 \
	lib/openmodbat.jar \
        -s=4e41a9e42b0dbf1a \
        -n=1 \
        --loop-limit=4 \
	--dotify-coverage \
	--no-redirect-out \
	--stop-on-failure \
	--log-level=fine \
        modbat.ZKServer

# remove Zookeeper transactions logs
rm -f temp/zookeeper/version-2/log.*
