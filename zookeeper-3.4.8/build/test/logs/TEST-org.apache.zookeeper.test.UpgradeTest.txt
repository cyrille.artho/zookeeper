Testsuite: org.apache.zookeeper.test.UpgradeTest
Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.627 sec
------------- Standard Output ---------------
2016-08-23 01:26:20,404 [myid:] - INFO  [main:PortAssignment@32] - assigning port 11221
2016-08-23 01:26:20,482 [myid:] - INFO  [main:ZKTestCase$1@50] - STARTING testUpgrade
2016-08-23 01:26:20,487 [myid:] - INFO  [main:JUnit4ZKTestRunner$LoggedInvokeMethod@53] - RUNNING TEST METHOD testUpgrade
2016-08-23 01:26:20,490 [myid:] - INFO  [main:UpgradeMain@79] - Creating previous version data dir /Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/data/upgrade/version-1
2016-08-23 01:26:20,490 [myid:] - INFO  [main:UpgradeMain@85] - Creating previous version snapshot dir /Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/data/upgrade/version-1
2016-08-23 01:26:20,490 [myid:] - INFO  [main:UpgradeMain@92] - Creating current data dir /Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/data/upgrade/version-2
2016-08-23 01:26:20,490 [myid:] - INFO  [main:UpgradeMain@98] - Creating current snapshot dir /Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/data/upgrade/version-2
2016-08-23 01:26:20,490 [myid:] - INFO  [main:UpgradeMain@122] - Renaming /Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/data/upgrade/log.100000001 to /Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/data/upgrade/version-1/log.100000001
2016-08-23 01:26:20,491 [myid:] - INFO  [main:UpgradeMain@122] - Renaming /Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/data/upgrade/log.100001bf0 to /Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/data/upgrade/version-1/log.100001bf0
2016-08-23 01:26:20,491 [myid:] - INFO  [main:UpgradeMain@122] - Renaming /Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/data/upgrade/snapshot.100000000 to /Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/data/upgrade/version-1/snapshot.100000000
2016-08-23 01:26:20,491 [myid:] - INFO  [main:UpgradeMain@122] - Renaming /Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/data/upgrade/snapshot.100001bec to /Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/data/upgrade/version-1/snapshot.100001bec
2016-08-23 01:26:20,506 [myid:] - INFO  [main:UpgradeMain@158] - Creating new data tree
2016-08-23 01:26:20,557 [myid:] - INFO  [main:UpgradeSnapShotV1@183] - Processing log file: /Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/data/upgrade/version-1/log.100000001
2016-08-23 01:26:20,681 [myid:] - INFO  [main:UpgradeSnapShotV1@183] - Processing log file: /Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/data/upgrade/version-1/log.100001bf0
2016-08-23 01:26:20,747 [myid:] - INFO  [main:UpgradeMain@162] - snapshotting the new datatree
2016-08-23 01:26:20,747 [myid:] - INFO  [main:FileTxnSnapLog@240] - Snapshotting: 0x100002730 to /Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/data/upgrade/version-2/snapshot.100002730
2016-08-23 01:26:20,801 [myid:] - INFO  [main:UpgradeMain@165] - Upgrade is complete
2016-08-23 01:26:20,819 [myid:] - INFO  [main:Environment@100] - Server environment:zookeeper.version=3.4.8--1, built on 08/22/2016 16:05 GMT
2016-08-23 01:26:20,819 [myid:] - INFO  [main:Environment@100] - Server environment:host.name=192.168.1.9
2016-08-23 01:26:20,819 [myid:] - INFO  [main:Environment@100] - Server environment:java.version=1.8.0_45
2016-08-23 01:26:20,819 [myid:] - INFO  [main:Environment@100] - Server environment:java.vendor=Oracle Corporation
2016-08-23 01:26:20,819 [myid:] - INFO  [main:Environment@100] - Server environment:java.home=/Library/Java/JavaVirtualMachines/jdk1.8.0_45.jdk/Contents/Home/jre
2016-08-23 01:26:20,819 [myid:] - INFO  [main:Environment@100] - Server environment:java.class.path=/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/src/java/lib/ivy-2.2.0.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/lib/jline-0.9.94.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/lib/log4j-1.2.16.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/lib/netty-3.7.0.Final.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/lib/slf4j-api-1.6.1.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/lib/slf4j-log4j12-1.6.1.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/classes:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/lib/antlr-2.7.6.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/lib/checkstyle-5.0.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/lib/commons-beanutils-core-1.7.0.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/lib/commons-cli-1.0.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/lib/commons-collections-3.2.2.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/lib/commons-lang-1.0.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/lib/commons-logging-1.0.3.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/lib/google-collections-0.9.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/lib/junit-4.8.1.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/lib/mockito-all-1.8.2.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/classes:/usr/local/Cellar/ant/1.9.4/libexec/lib/ant.jar:/Users/malei/local/aspectj1.8/lib/aspectjrt.jar:/Users/malei/local/aspectj1.8/lib/aspectjtools.jar:/Users/malei/local/aspectj1.8/lib/aspectjweaver.jar:/Users/malei/local/aspectj1.8/lib/org.aspectj.matcher.jar:/usr/local/Cellar/ant/1.9.4/libexec/lib/ant-launcher.jar:/usr/local/Cellar/ant/1.9.4/libexec/lib/ant-junit.jar:/usr/local/Cellar/ant/1.9.4/libexec/lib/ant-junit4.jar:/var/folders/f3/d9q0sfvn6sdd7lsr4sgy2tq40000gn/T/jacocoagent1567422376819360909.jar
2016-08-23 01:26:20,819 [myid:] - INFO  [main:Environment@100] - Server environment:java.library.path=/Users/malei/Library/Java/Extensions:/Library/Java/Extensions:/Network/Library/Java/Extensions:/System/Library/Java/Extensions:/usr/lib/java:.
2016-08-23 01:26:20,819 [myid:] - INFO  [main:Environment@100] - Server environment:java.io.tmpdir=/var/folders/f3/d9q0sfvn6sdd7lsr4sgy2tq40000gn/T/
2016-08-23 01:26:20,819 [myid:] - INFO  [main:Environment@100] - Server environment:java.compiler=<NA>
2016-08-23 01:26:20,819 [myid:] - INFO  [main:Environment@100] - Server environment:os.name=Mac OS X
2016-08-23 01:26:20,819 [myid:] - INFO  [main:Environment@100] - Server environment:os.arch=x86_64
2016-08-23 01:26:20,819 [myid:] - INFO  [main:Environment@100] - Server environment:os.version=10.10.2
2016-08-23 01:26:20,820 [myid:] - INFO  [main:Environment@100] - Server environment:user.name=malei
2016-08-23 01:26:20,820 [myid:] - INFO  [main:Environment@100] - Server environment:user.home=/Users/malei
2016-08-23 01:26:20,820 [myid:] - INFO  [main:Environment@100] - Server environment:user.dir=/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8
2016-08-23 01:26:20,825 [myid:] - INFO  [main:ZooKeeperServer@170] - Created server with tickTime 3000 minSessionTimeout 6000 maxSessionTimeout 60000 datadir /Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/data/upgrade/version-2 snapdir /Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/data/upgrade/version-2
2016-08-23 01:26:20,848 [myid:] - INFO  [main:NIOServerCnxnFactory@89] - binding to port 0.0.0.0/0.0.0.0:11221
2016-08-23 01:26:20,852 [myid:] - INFO  [main:FileSnap@83] - Reading snapshot /Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/data/upgrade/version-2/snapshot.100002730
2016-08-23 01:26:20,951 [myid:] - INFO  [main:UpgradeTest@68] - starting up the zookeeper server .. waiting
2016-08-23 01:26:20,957 [myid:] - INFO  [main:FourLetterWordMain@62] - connecting to 127.0.0.1 11221
2016-08-23 01:26:20,965 [myid:] - INFO  [NIOServerCxn.Factory:0.0.0.0/0.0.0.0:11221:NIOServerCnxnFactory@192] - Accepted socket connection from /127.0.0.1:50287
2016-08-23 01:26:21,009 [myid:] - INFO  [NIOServerCxn.Factory:0.0.0.0/0.0.0.0:11221:NIOServerCnxn@827] - Processing stat command from /127.0.0.1:50287
2016-08-23 01:26:21,012 [myid:] - INFO  [Thread-2:NIOServerCnxn$StatCommand@663] - Stat command output
2016-08-23 01:26:21,013 [myid:] - INFO  [Thread-2:NIOServerCnxn@1008] - Closed socket connection for client /127.0.0.1:50287 (no session established for client)
2016-08-23 01:26:21,020 [myid:] - INFO  [main:Environment@100] - Client environment:zookeeper.version=3.4.8--1, built on 08/22/2016 16:05 GMT
2016-08-23 01:26:21,020 [myid:] - INFO  [main:Environment@100] - Client environment:host.name=192.168.1.9
2016-08-23 01:26:21,020 [myid:] - INFO  [main:Environment@100] - Client environment:java.version=1.8.0_45
2016-08-23 01:26:21,021 [myid:] - INFO  [main:Environment@100] - Client environment:java.vendor=Oracle Corporation
2016-08-23 01:26:21,021 [myid:] - INFO  [main:Environment@100] - Client environment:java.home=/Library/Java/JavaVirtualMachines/jdk1.8.0_45.jdk/Contents/Home/jre
2016-08-23 01:26:21,021 [myid:] - INFO  [main:Environment@100] - Client environment:java.class.path=/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/src/java/lib/ivy-2.2.0.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/lib/jline-0.9.94.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/lib/log4j-1.2.16.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/lib/netty-3.7.0.Final.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/lib/slf4j-api-1.6.1.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/lib/slf4j-log4j12-1.6.1.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/classes:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/lib/antlr-2.7.6.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/lib/checkstyle-5.0.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/lib/commons-beanutils-core-1.7.0.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/lib/commons-cli-1.0.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/lib/commons-collections-3.2.2.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/lib/commons-lang-1.0.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/lib/commons-logging-1.0.3.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/lib/google-collections-0.9.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/lib/junit-4.8.1.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/test/lib/mockito-all-1.8.2.jar:/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8/build/classes:/usr/local/Cellar/ant/1.9.4/libexec/lib/ant.jar:/Users/malei/local/aspectj1.8/lib/aspectjrt.jar:/Users/malei/local/aspectj1.8/lib/aspectjtools.jar:/Users/malei/local/aspectj1.8/lib/aspectjweaver.jar:/Users/malei/local/aspectj1.8/lib/org.aspectj.matcher.jar:/usr/local/Cellar/ant/1.9.4/libexec/lib/ant-launcher.jar:/usr/local/Cellar/ant/1.9.4/libexec/lib/ant-junit.jar:/usr/local/Cellar/ant/1.9.4/libexec/lib/ant-junit4.jar:/var/folders/f3/d9q0sfvn6sdd7lsr4sgy2tq40000gn/T/jacocoagent1567422376819360909.jar
2016-08-23 01:26:21,021 [myid:] - INFO  [main:Environment@100] - Client environment:java.library.path=/Users/malei/Library/Java/Extensions:/Library/Java/Extensions:/Network/Library/Java/Extensions:/System/Library/Java/Extensions:/usr/lib/java:.
2016-08-23 01:26:21,021 [myid:] - INFO  [main:Environment@100] - Client environment:java.io.tmpdir=/var/folders/f3/d9q0sfvn6sdd7lsr4sgy2tq40000gn/T/
2016-08-23 01:26:21,021 [myid:] - INFO  [main:Environment@100] - Client environment:java.compiler=<NA>
2016-08-23 01:26:21,022 [myid:] - INFO  [main:Environment@100] - Client environment:os.name=Mac OS X
2016-08-23 01:26:21,022 [myid:] - INFO  [main:Environment@100] - Client environment:os.arch=x86_64
2016-08-23 01:26:21,022 [myid:] - INFO  [main:Environment@100] - Client environment:os.version=10.10.2
2016-08-23 01:26:21,022 [myid:] - INFO  [main:Environment@100] - Client environment:user.name=malei
2016-08-23 01:26:21,022 [myid:] - INFO  [main:Environment@100] - Client environment:user.home=/Users/malei
2016-08-23 01:26:21,022 [myid:] - INFO  [main:Environment@100] - Client environment:user.dir=/Users/malei/Documents/MyProject/modbat/zookeeper-new-repo/zookeeper-3.4.8
2016-08-23 01:26:21,023 [myid:] - INFO  [main:ZooKeeper@438] - Initiating client connection, connectString=127.0.0.1:11221 sessionTimeout=30000 watcher=org.apache.zookeeper.test.UpgradeTest@7d0614f
2016-08-23 01:26:21,039 [myid:] - INFO  [main-SendThread(127.0.0.1:11221):ClientCnxn$SendThread@1032] - Opening socket connection to server 127.0.0.1/127.0.0.1:11221. Will not attempt to authenticate using SASL (unknown error)
2016-08-23 01:26:21,040 [myid:] - INFO  [NIOServerCxn.Factory:0.0.0.0/0.0.0.0:11221:NIOServerCnxnFactory@192] - Accepted socket connection from /127.0.0.1:50288
2016-08-23 01:26:21,041 [myid:] - INFO  [main-SendThread(127.0.0.1:11221):ClientCnxn$SendThread@876] - Socket connection established to 127.0.0.1/127.0.0.1:11221, initiating session
2016-08-23 01:26:21,043 [myid:] - INFO  [NIOServerCxn.Factory:0.0.0.0/0.0.0.0:11221:ZooKeeperServer@900] - Client attempting to establish new session at /127.0.0.1:50288
2016-08-23 01:26:21,044 [myid:] - INFO  [SyncThread:0:FileTxnLog@199] - Creating new log file: log.100002731
2016-08-23 01:26:21,075 [myid:] - INFO  [SyncThread:0:ZooKeeperServer@645] - Established session 0x156b312fb5c0000 with negotiated timeout 30000 for client /127.0.0.1:50288
2016-08-23 01:26:21,075 [myid:] - INFO  [main-SendThread(127.0.0.1:11221):ClientCnxn$SendThread@1299] - Session establishment complete on server 127.0.0.1/127.0.0.1:11221, sessionid = 0x156b312fb5c0000, negotiated timeout = 30000
2016-08-23 01:26:21,076 [myid:] - INFO  [main-EventThread:UpgradeTest@98] - Event:SyncConnected None null
2016-08-23 01:26:21,086 [myid:] - INFO  [ProcessThread(sid:0 cport:11221)::PrepRequestProcessor@489] - Processed session termination for sessionid: 0x156b312fb5c0000
2016-08-23 01:26:21,087 [myid:] - INFO  [main-EventThread:ClientCnxn$EventThread@519] - EventThread shut down for session: 0x156b312fb5c0000
2016-08-23 01:26:21,087 [myid:] - INFO  [main:ZooKeeper@684] - Session: 0x156b312fb5c0000 closed
2016-08-23 01:26:21,087 [myid:] - INFO  [NIOServerCxn.Factory:0.0.0.0/0.0.0.0:11221:NIOServerCnxn@1008] - Closed socket connection for client /127.0.0.1:50288 which had sessionid 0x156b312fb5c0000
2016-08-23 01:26:21,087 [myid:] - INFO  [NIOServerCxn.Factory:0.0.0.0/0.0.0.0:11221:NIOServerCnxnFactory@219] - NIOServerCnxn factory exited run method
2016-08-23 01:26:21,087 [myid:] - INFO  [main:ZooKeeperServer@469] - shutting down
2016-08-23 01:26:21,088 [myid:] - INFO  [main:SessionTrackerImpl@225] - Shutting down
2016-08-23 01:26:21,088 [myid:] - INFO  [main:PrepRequestProcessor@767] - Shutting down
2016-08-23 01:26:21,088 [myid:] - INFO  [main:SyncRequestProcessor@209] - Shutting down
2016-08-23 01:26:21,088 [myid:] - INFO  [ProcessThread(sid:0 cport:11221)::PrepRequestProcessor@143] - PrepRequestProcessor exited loop!
2016-08-23 01:26:21,088 [myid:] - INFO  [SyncThread:0:SyncRequestProcessor@187] - SyncRequestProcessor exited!
2016-08-23 01:26:21,088 [myid:] - INFO  [main:FinalRequestProcessor@415] - shutdown of request processor complete
2016-08-23 01:26:21,089 [myid:] - INFO  [main:FourLetterWordMain@62] - connecting to 127.0.0.1 11221
2016-08-23 01:26:21,089 [myid:] - INFO  [main:JUnit4ZKTestRunner$LoggedInvokeMethod@58] - Memory used 65625
2016-08-23 01:26:21,089 [myid:] - INFO  [main:JUnit4ZKTestRunner$LoggedInvokeMethod@63] - Number of threads 6
2016-08-23 01:26:21,089 [myid:] - INFO  [main:JUnit4ZKTestRunner$LoggedInvokeMethod@78] - FINISHED TEST METHOD testUpgrade
2016-08-23 01:26:21,089 [myid:] - INFO  [main:ZKTestCase$1@60] - SUCCEEDED testUpgrade
2016-08-23 01:26:21,089 [myid:] - INFO  [main:ZKTestCase$1@55] - FINISHED testUpgrade
------------- ---------------- ---------------

Testcase: testUpgrade took 0.611 sec
