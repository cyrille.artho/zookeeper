Testsuite: org.apache.zookeeper.server.quorum.FLECompatibilityTest
Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.158 sec
------------- Standard Output ---------------
2016-08-23 01:07:53,165 [myid:] - INFO  [main:ZKTestCase$1@50] - STARTING testForwardCompatibility
2016-08-23 01:07:53,172 [myid:] - INFO  [Thread-1:JUnit4ZKTestRunner$LoggedInvokeMethod@53] - RUNNING TEST METHOD testForwardCompatibility
2016-08-23 01:07:53,175 [myid:] - INFO  [Thread-1:PortAssignment@32] - assigning port 11221
2016-08-23 01:07:53,175 [myid:] - INFO  [Thread-1:PortAssignment@32] - assigning port 11222
2016-08-23 01:07:53,197 [myid:] - INFO  [Thread-1:QuorumPeer$QuorumServer@149] - Resolved hostname: 0.0.0.0 to address: /0.0.0.0
2016-08-23 01:07:53,212 [myid:] - INFO  [Thread-1:PortAssignment@32] - assigning port 11223
2016-08-23 01:07:53,212 [myid:] - INFO  [Thread-1:PortAssignment@32] - assigning port 11224
2016-08-23 01:07:53,212 [myid:] - INFO  [Thread-1:PortAssignment@32] - assigning port 11225
2016-08-23 01:07:53,213 [myid:] - INFO  [Thread-1:QuorumPeer$QuorumServer@149] - Resolved hostname: 0.0.0.0 to address: /0.0.0.0
2016-08-23 01:07:53,213 [myid:] - INFO  [Thread-1:PortAssignment@32] - assigning port 11226
2016-08-23 01:07:53,213 [myid:] - INFO  [Thread-1:PortAssignment@32] - assigning port 11227
2016-08-23 01:07:53,213 [myid:] - INFO  [Thread-1:PortAssignment@32] - assigning port 11228
2016-08-23 01:07:53,213 [myid:] - INFO  [Thread-1:QuorumPeer$QuorumServer@149] - Resolved hostname: 0.0.0.0 to address: /0.0.0.0
2016-08-23 01:07:53,214 [myid:] - INFO  [Thread-1:PortAssignment@32] - assigning port 11229
2016-08-23 01:07:53,239 [myid:] - INFO  [Thread-1:NIOServerCnxnFactory@89] - binding to port 0.0.0.0/0.0.0.0:11223
2016-08-23 01:07:53,293 [myid:] - INFO  [WorkerReceiver[myid=0]:FastLeaderElection@600] - Notification: 1 (message format version), 2 (n.leader), 0x1 (n.zxid), 0x1 (n.round), LOOKING (n.state), 2 (n.sid), 0x1 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:07:53,295 [myid:] - INFO  [Thread-1:JUnit4ZKTestRunner$LoggedInvokeMethod@58] - Memory used 51223
2016-08-23 01:07:53,295 [myid:] - INFO  [Thread-1:JUnit4ZKTestRunner$LoggedInvokeMethod@63] - Number of threads 7
2016-08-23 01:07:53,296 [myid:] - INFO  [Thread-1:JUnit4ZKTestRunner$LoggedInvokeMethod@78] - FINISHED TEST METHOD testForwardCompatibility
2016-08-23 01:07:53,296 [myid:] - INFO  [main:ZKTestCase$1@60] - SUCCEEDED testForwardCompatibility
2016-08-23 01:07:53,297 [myid:] - INFO  [main:ZKTestCase$1@55] - FINISHED testForwardCompatibility
2016-08-23 01:07:53,301 [myid:] - INFO  [main:ZKTestCase$1@50] - STARTING testBackwardCompatibility
2016-08-23 01:07:53,301 [myid:] - INFO  [Thread-2:JUnit4ZKTestRunner$LoggedInvokeMethod@53] - RUNNING TEST METHOD testBackwardCompatibility
2016-08-23 01:07:53,301 [myid:] - INFO  [Thread-2:PortAssignment@32] - assigning port 11230
2016-08-23 01:07:53,302 [myid:] - INFO  [Thread-2:PortAssignment@32] - assigning port 11231
2016-08-23 01:07:53,302 [myid:] - INFO  [Thread-2:QuorumPeer$QuorumServer@149] - Resolved hostname: 0.0.0.0 to address: /0.0.0.0
2016-08-23 01:07:53,302 [myid:] - INFO  [Thread-2:PortAssignment@32] - assigning port 11232
2016-08-23 01:07:53,302 [myid:] - INFO  [Thread-2:PortAssignment@32] - assigning port 11233
2016-08-23 01:07:53,302 [myid:] - INFO  [Thread-2:PortAssignment@32] - assigning port 11234
2016-08-23 01:07:53,302 [myid:] - INFO  [Thread-2:QuorumPeer$QuorumServer@149] - Resolved hostname: 0.0.0.0 to address: /0.0.0.0
2016-08-23 01:07:53,303 [myid:] - INFO  [Thread-2:PortAssignment@32] - assigning port 11235
2016-08-23 01:07:53,303 [myid:] - INFO  [Thread-2:PortAssignment@32] - assigning port 11236
2016-08-23 01:07:53,303 [myid:] - INFO  [Thread-2:PortAssignment@32] - assigning port 11237
2016-08-23 01:07:53,303 [myid:] - INFO  [Thread-2:QuorumPeer$QuorumServer@149] - Resolved hostname: 0.0.0.0 to address: /0.0.0.0
2016-08-23 01:07:53,304 [myid:] - INFO  [Thread-2:PortAssignment@32] - assigning port 11238
2016-08-23 01:07:53,304 [myid:] - INFO  [Thread-2:NIOServerCnxnFactory@89] - binding to port 0.0.0.0/0.0.0.0:11232
2016-08-23 01:07:53,308 [myid:] - INFO  [Thread-2:JUnit4ZKTestRunner$LoggedInvokeMethod@58] - Memory used 53844
2016-08-23 01:07:53,308 [myid:] - INFO  [Thread-2:JUnit4ZKTestRunner$LoggedInvokeMethod@63] - Number of threads 8
2016-08-23 01:07:53,309 [myid:] - INFO  [Thread-2:JUnit4ZKTestRunner$LoggedInvokeMethod@78] - FINISHED TEST METHOD testBackwardCompatibility
2016-08-23 01:07:53,309 [myid:] - INFO  [main:ZKTestCase$1@60] - SUCCEEDED testBackwardCompatibility
2016-08-23 01:07:53,309 [myid:] - INFO  [main:ZKTestCase$1@55] - FINISHED testBackwardCompatibility
------------- ---------------- ---------------

Testcase: testForwardCompatibility took 0.136 sec
Testcase: testBackwardCompatibility took 0.008 sec
