Testsuite: org.apache.zookeeper.test.FLERestartTest
Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.647 sec
------------- Standard Output ---------------
2016-08-23 01:15:36,363 [myid:] - INFO  [main:ZKTestCase$1@50] - STARTING testLERestart
2016-08-23 01:15:36,371 [myid:] - INFO  [main:JUnit4ZKTestRunner$LoggedInvokeMethod@53] - RUNNING TEST METHOD testLERestart
2016-08-23 01:15:36,378 [myid:] - INFO  [main:FLERestartTest@178] - TestLE: testLERestart, 3
2016-08-23 01:15:36,380 [myid:] - INFO  [main:PortAssignment@32] - assigning port 11221
2016-08-23 01:15:36,380 [myid:] - INFO  [main:PortAssignment@32] - assigning port 11222
2016-08-23 01:15:36,403 [myid:] - INFO  [main:QuorumPeer$QuorumServer@149] - Resolved hostname: 0.0.0.0 to address: /0.0.0.0
2016-08-23 01:15:36,418 [myid:] - INFO  [main:PortAssignment@32] - assigning port 11223
2016-08-23 01:15:36,418 [myid:] - INFO  [main:PortAssignment@32] - assigning port 11224
2016-08-23 01:15:36,419 [myid:] - INFO  [main:PortAssignment@32] - assigning port 11225
2016-08-23 01:15:36,419 [myid:] - INFO  [main:QuorumPeer$QuorumServer@149] - Resolved hostname: 0.0.0.0 to address: /0.0.0.0
2016-08-23 01:15:36,419 [myid:] - INFO  [main:PortAssignment@32] - assigning port 11226
2016-08-23 01:15:36,419 [myid:] - INFO  [main:PortAssignment@32] - assigning port 11227
2016-08-23 01:15:36,420 [myid:] - INFO  [main:PortAssignment@32] - assigning port 11228
2016-08-23 01:15:36,420 [myid:] - INFO  [main:QuorumPeer$QuorumServer@149] - Resolved hostname: 0.0.0.0 to address: /0.0.0.0
2016-08-23 01:15:36,420 [myid:] - INFO  [main:PortAssignment@32] - assigning port 11229
2016-08-23 01:15:36,448 [myid:] - INFO  [main:NIOServerCnxnFactory@89] - binding to port 0.0.0.0/0.0.0.0:11223
2016-08-23 01:15:36,489 [myid:] - INFO  [main:QuorumPeer@533] - currentEpoch not found! Creating with a reasonable default of 0. This should only happen when you are upgrading your installation
2016-08-23 01:15:36,491 [myid:] - INFO  [main:QuorumPeer@548] - acceptedEpoch not found! Creating with a reasonable default of 0. This should only happen when you are upgrading your installation
2016-08-23 01:15:36,497 [myid:] - INFO  [ListenerThread:QuorumCnxManager$Listener@534] - My election bind port: /0.0.0.0:11222
2016-08-23 01:15:36,503 [myid:] - INFO  [main:FLERestartTest$FLERestartThread@109] - Constructor: Thread-1
2016-08-23 01:15:36,504 [myid:] - INFO  [Thread-1:FLERestartTest$FLERestartThread@116] - Going to call leader election again.
2016-08-23 01:15:36,504 [myid:] - INFO  [main:NIOServerCnxnFactory@89] - binding to port 0.0.0.0/0.0.0.0:11226
2016-08-23 01:15:36,504 [myid:] - INFO  [main:QuorumPeer@533] - currentEpoch not found! Creating with a reasonable default of 0. This should only happen when you are upgrading your installation
2016-08-23 01:15:36,507 [myid:] - INFO  [main:QuorumPeer@548] - acceptedEpoch not found! Creating with a reasonable default of 0. This should only happen when you are upgrading your installation
2016-08-23 01:15:36,507 [myid:] - INFO  [ListenerThread:QuorumCnxManager$Listener@534] - My election bind port: /0.0.0.0:11225
2016-08-23 01:15:36,508 [myid:] - INFO  [main:FLERestartTest$FLERestartThread@109] - Constructor: Thread-2
2016-08-23 01:15:36,508 [myid:] - INFO  [Thread-2:FLERestartTest$FLERestartThread@116] - Going to call leader election again.
2016-08-23 01:15:36,508 [myid:] - INFO  [main:NIOServerCnxnFactory@89] - binding to port 0.0.0.0/0.0.0.0:11229
2016-08-23 01:15:36,509 [myid:] - INFO  [main:QuorumPeer@533] - currentEpoch not found! Creating with a reasonable default of 0. This should only happen when you are upgrading your installation
2016-08-23 01:15:36,509 [myid:] - INFO  [main:QuorumPeer@548] - acceptedEpoch not found! Creating with a reasonable default of 0. This should only happen when you are upgrading your installation
2016-08-23 01:15:36,510 [myid:] - INFO  [ListenerThread:QuorumCnxManager$Listener@534] - My election bind port: /0.0.0.0:11228
2016-08-23 01:15:36,510 [myid:] - INFO  [main:FLERestartTest$FLERestartThread@109] - Constructor: Thread-4
2016-08-23 01:15:36,511 [myid:] - INFO  [main:FLERestartTest@194] - Started threads testLERestart
2016-08-23 01:15:36,511 [myid:] - INFO  [Thread-4:FLERestartTest$FLERestartThread@116] - Going to call leader election again.
2016-08-23 01:15:36,553 [myid:] - INFO  [Thread-1:FastLeaderElection@818] - New election. My id =  0, proposed zxid=0x0
2016-08-23 01:15:36,554 [myid:] - WARN  [Thread-2:MBeanRegistry@104] - Failed to register MBean LeaderElection
2016-08-23 01:15:36,554 [myid:] - WARN  [Thread-4:MBeanRegistry@104] - Failed to register MBean LeaderElection
2016-08-23 01:15:36,554 [myid:] - WARN  [Thread-2:FastLeaderElection@800] - Failed to register with JMX
javax.management.InstanceAlreadyExistsException: org.apache.ZooKeeperService:name0=LeaderElection
	at com.sun.jmx.mbeanserver.Repository.addMBean(Repository.java:437)
	at com.sun.jmx.interceptor.DefaultMBeanServerInterceptor.registerWithRepository(DefaultMBeanServerInterceptor.java:1898)
	at com.sun.jmx.interceptor.DefaultMBeanServerInterceptor.registerDynamicMBean(DefaultMBeanServerInterceptor.java:966)
	at com.sun.jmx.interceptor.DefaultMBeanServerInterceptor.registerObject(DefaultMBeanServerInterceptor.java:900)
	at com.sun.jmx.interceptor.DefaultMBeanServerInterceptor.registerMBean(DefaultMBeanServerInterceptor.java:324)
	at com.sun.jmx.mbeanserver.JmxMBeanServer.registerMBean(JmxMBeanServer.java:522)
	at org.apache.zookeeper.jmx.MBeanRegistry.register(MBeanRegistry.java:100)
	at org.apache.zookeeper.server.quorum.FastLeaderElection.lookForLeader(FastLeaderElection.java:797)
	at org.apache.zookeeper.test.FLERestartTest$FLERestartThread.run(FLERestartTest.java:117)
2016-08-23 01:15:36,556 [myid:] - INFO  [WorkerReceiver[myid=0]:FastLeaderElection@600] - Notification: 1 (message format version), 0 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 0 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,556 [myid:] - INFO  [Thread-2:FastLeaderElection@818] - New election. My id =  1, proposed zxid=0x0
2016-08-23 01:15:36,554 [myid:] - WARN  [Thread-4:FastLeaderElection@800] - Failed to register with JMX
javax.management.InstanceAlreadyExistsException: org.apache.ZooKeeperService:name0=LeaderElection
	at com.sun.jmx.mbeanserver.Repository.addMBean(Repository.java:437)
	at com.sun.jmx.interceptor.DefaultMBeanServerInterceptor.registerWithRepository(DefaultMBeanServerInterceptor.java:1898)
	at com.sun.jmx.interceptor.DefaultMBeanServerInterceptor.registerDynamicMBean(DefaultMBeanServerInterceptor.java:966)
	at com.sun.jmx.interceptor.DefaultMBeanServerInterceptor.registerObject(DefaultMBeanServerInterceptor.java:900)
	at com.sun.jmx.interceptor.DefaultMBeanServerInterceptor.registerMBean(DefaultMBeanServerInterceptor.java:324)
	at com.sun.jmx.mbeanserver.JmxMBeanServer.registerMBean(JmxMBeanServer.java:522)
	at org.apache.zookeeper.jmx.MBeanRegistry.register(MBeanRegistry.java:100)
	at org.apache.zookeeper.server.quorum.FastLeaderElection.lookForLeader(FastLeaderElection.java:797)
	at org.apache.zookeeper.test.FLERestartTest$FLERestartThread.run(FLERestartTest.java:117)
2016-08-23 01:15:36,557 [myid:] - INFO  [Thread-4:FastLeaderElection@818] - New election. My id =  2, proposed zxid=0x0
2016-08-23 01:15:36,559 [myid:] - INFO  [/0.0.0.0:11222:QuorumCnxManager$Listener@541] - Received connection request /192.168.1.9:52179
2016-08-23 01:15:36,559 [myid:] - INFO  [/0.0.0.0:11225:QuorumCnxManager$Listener@541] - Received connection request /192.168.1.9:52180
2016-08-23 01:15:36,562 [myid:] - INFO  [WorkerSender[myid=0]:QuorumCnxManager@199] - Have smaller server identifier, so dropping the connection: (1, 0)
2016-08-23 01:15:36,563 [myid:] - INFO  [/0.0.0.0:11228:QuorumCnxManager$Listener@541] - Received connection request /192.168.1.9:52182
2016-08-23 01:15:36,563 [myid:] - INFO  [WorkerSender[myid=0]:QuorumCnxManager@199] - Have smaller server identifier, so dropping the connection: (2, 0)
2016-08-23 01:15:36,569 [myid:] - INFO  [/0.0.0.0:11222:QuorumCnxManager$Listener@541] - Received connection request /192.168.1.9:52181
2016-08-23 01:15:36,569 [myid:] - INFO  [WorkerReceiver[myid=1]:FastLeaderElection@600] - Notification: 1 (message format version), 1 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 1 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,569 [myid:] - INFO  [WorkerReceiver[myid=0]:FastLeaderElection@600] - Notification: 1 (message format version), 2 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 2 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,569 [myid:] - INFO  [WorkerSender[myid=1]:QuorumCnxManager@199] - Have smaller server identifier, so dropping the connection: (2, 1)
2016-08-23 01:15:36,569 [myid:] - INFO  [/0.0.0.0:11225:QuorumCnxManager$Listener@541] - Received connection request /192.168.1.9:52184
2016-08-23 01:15:36,569 [myid:] - INFO  [WorkerReceiver[myid=1]:FastLeaderElection@600] - Notification: 1 (message format version), 0 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 0 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,569 [myid:] - INFO  [/0.0.0.0:11228:QuorumCnxManager$Listener@541] - Received connection request /192.168.1.9:52183
2016-08-23 01:15:36,569 [myid:] - INFO  [WorkerReceiver[myid=2]:FastLeaderElection@600] - Notification: 1 (message format version), 0 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 0 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,570 [myid:] - INFO  [WorkerReceiver[myid=1]:FastLeaderElection@600] - Notification: 1 (message format version), 2 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 0 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,569 [myid:] - INFO  [WorkerReceiver[myid=0]:FastLeaderElection@600] - Notification: 1 (message format version), 2 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 0 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,570 [myid:] - WARN  [RecvWorker:1:QuorumCnxManager$RecvWorker@810] - Connection broken for id 1, my id = 2, error = 
java.net.SocketException: Socket closed
	at java.net.SocketInputStream.socketRead0(Native Method)
	at java.net.SocketInputStream.socketRead(SocketInputStream.java:116)
	at java.net.SocketInputStream.read(SocketInputStream.java:170)
	at java.net.SocketInputStream.read(SocketInputStream.java:141)
	at java.net.SocketInputStream.read(SocketInputStream.java:223)
	at java.io.DataInputStream.readInt(DataInputStream.java:387)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$RecvWorker.run(QuorumCnxManager.java:795)
2016-08-23 01:15:36,570 [myid:] - INFO  [WorkerReceiver[myid=1]:FastLeaderElection@600] - Notification: 1 (message format version), 2 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 2 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,570 [myid:] - WARN  [SendWorker:1:QuorumCnxManager$SendWorker@727] - Interrupted while waiting for message on queue
java.lang.InterruptedException
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.reportInterruptAfterWait(AbstractQueuedSynchronizer.java:2014)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.awaitNanos(AbstractQueuedSynchronizer.java:2088)
	at java.util.concurrent.ArrayBlockingQueue.poll(ArrayBlockingQueue.java:418)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.pollSendQueue(QuorumCnxManager.java:879)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.access$500(QuorumCnxManager.java:65)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$SendWorker.run(QuorumCnxManager.java:715)
2016-08-23 01:15:36,570 [myid:] - INFO  [WorkerReceiver[myid=2]:FastLeaderElection@600] - Notification: 1 (message format version), 2 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 2 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,571 [myid:] - WARN  [SendWorker:1:QuorumCnxManager$SendWorker@736] - Send worker leaving thread
2016-08-23 01:15:36,570 [myid:] - INFO  [WorkerReceiver[myid=1]:FastLeaderElection@600] - Notification: 1 (message format version), 2 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 1 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,570 [myid:] - WARN  [RecvWorker:1:QuorumCnxManager$RecvWorker@816] - Interrupting SendWorker
2016-08-23 01:15:36,570 [myid:] - WARN  [SendWorker:2:QuorumCnxManager$SendWorker@732] - Exception when using channel: for id 2 my id = 1 error = java.net.SocketException: Broken pipe
2016-08-23 01:15:36,570 [myid:] - INFO  [/0.0.0.0:11225:QuorumCnxManager$Listener@541] - Received connection request /192.168.1.9:52185
2016-08-23 01:15:36,570 [myid:] - INFO  [WorkerReceiver[myid=0]:FastLeaderElection@600] - Notification: 1 (message format version), 1 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 1 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,570 [myid:] - WARN  [RecvWorker:2:QuorumCnxManager$RecvWorker@810] - Connection broken for id 2, my id = 1, error = 
java.net.SocketException: Connection reset
	at java.net.SocketInputStream.read(SocketInputStream.java:209)
	at java.net.SocketInputStream.read(SocketInputStream.java:141)
	at java.net.SocketInputStream.read(SocketInputStream.java:223)
	at java.io.DataInputStream.readInt(DataInputStream.java:387)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$RecvWorker.run(QuorumCnxManager.java:795)
2016-08-23 01:15:36,571 [myid:] - INFO  [WorkerReceiver[myid=0]:FastLeaderElection@600] - Notification: 1 (message format version), 2 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 1 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,571 [myid:] - WARN  [SendWorker:2:QuorumCnxManager$SendWorker@736] - Send worker leaving thread
2016-08-23 01:15:36,571 [myid:] - INFO  [WorkerReceiver[myid=2]:FastLeaderElection@600] - Notification: 1 (message format version), 2 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 0 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,571 [myid:] - INFO  [WorkerReceiver[myid=1]:FastLeaderElection@600] - Notification: 1 (message format version), 2 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 2 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,572 [myid:] - INFO  [WorkerReceiver[myid=2]:FastLeaderElection@600] - Notification: 1 (message format version), 2 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 1 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,571 [myid:] - WARN  [RecvWorker:2:QuorumCnxManager$RecvWorker@816] - Interrupting SendWorker
2016-08-23 01:15:36,772 [myid:] - INFO  [Thread-4:FLERestartTest$FLERestartThread@129] - Finished election: 2, 2
2016-08-23 01:15:36,772 [myid:] - INFO  [Thread-4:FLERestartTest$FLERestartThread@156] - First peer, do nothing, just join
2016-08-23 01:15:36,772 [myid:] - INFO  [Thread-2:FLERestartTest$FLERestartThread@129] - Finished election: 1, 2
2016-08-23 01:15:36,772 [myid:] - INFO  [Thread-2:FLERestartTest$FLERestartThread@149] - Second entering case
2016-08-23 01:15:36,772 [myid:] - INFO  [Thread-1:FLERestartTest$FLERestartThread@129] - Finished election: 0, 2
2016-08-23 01:15:36,772 [myid:] - INFO  [Thread-1:FLERestartTest$FLERestartThread@135] - First peer, shutting it down
2016-08-23 01:15:36,775 [myid:] - INFO  [Thread-1:QuorumBase@306] - Shutting down quorum peer QuorumPeer
2016-08-23 01:15:36,777 [myid:] - ERROR [/0.0.0.0:11222:QuorumCnxManager$Listener@547] - Exception while listening
java.net.SocketException: Socket closed
	at java.net.PlainSocketImpl.socketAccept(Native Method)
	at java.net.AbstractPlainSocketImpl.accept(AbstractPlainSocketImpl.java:404)
	at java.net.ServerSocket.implAccept(ServerSocket.java:545)
	at java.net.ServerSocket.accept(ServerSocket.java:513)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$Listener.run(QuorumCnxManager.java:539)
2016-08-23 01:15:36,777 [myid:] - WARN  [SendWorker:1:QuorumCnxManager$SendWorker@727] - Interrupted while waiting for message on queue
java.lang.InterruptedException
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.reportInterruptAfterWait(AbstractQueuedSynchronizer.java:2014)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.awaitNanos(AbstractQueuedSynchronizer.java:2088)
	at java.util.concurrent.ArrayBlockingQueue.poll(ArrayBlockingQueue.java:418)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.pollSendQueue(QuorumCnxManager.java:879)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.access$500(QuorumCnxManager.java:65)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$SendWorker.run(QuorumCnxManager.java:715)
2016-08-23 01:15:36,777 [myid:] - WARN  [RecvWorker:2:QuorumCnxManager$RecvWorker@810] - Connection broken for id 2, my id = 0, error = 
java.net.SocketException: Socket closed
	at java.net.SocketInputStream.socketRead0(Native Method)
	at java.net.SocketInputStream.socketRead(SocketInputStream.java:116)
	at java.net.SocketInputStream.read(SocketInputStream.java:170)
	at java.net.SocketInputStream.read(SocketInputStream.java:141)
	at java.net.SocketInputStream.read(SocketInputStream.java:223)
	at java.io.DataInputStream.readInt(DataInputStream.java:387)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$RecvWorker.run(QuorumCnxManager.java:795)
2016-08-23 01:15:36,777 [myid:] - INFO  [Thread-1:QuorumBase@310] - Shutting down leader election QuorumPeer
2016-08-23 01:15:36,777 [myid:] - WARN  [SendWorker:2:QuorumCnxManager$SendWorker@727] - Interrupted while waiting for message on queue
java.lang.InterruptedException
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.reportInterruptAfterWait(AbstractQueuedSynchronizer.java:2014)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.awaitNanos(AbstractQueuedSynchronizer.java:2088)
	at java.util.concurrent.ArrayBlockingQueue.poll(ArrayBlockingQueue.java:418)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.pollSendQueue(QuorumCnxManager.java:879)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.access$500(QuorumCnxManager.java:65)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$SendWorker.run(QuorumCnxManager.java:715)
2016-08-23 01:15:36,777 [myid:] - WARN  [RecvWorker:0:QuorumCnxManager$RecvWorker@810] - Connection broken for id 0, my id = 2, error = 
java.io.EOFException
	at java.io.DataInputStream.readInt(DataInputStream.java:392)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$RecvWorker.run(QuorumCnxManager.java:795)
2016-08-23 01:15:36,777 [myid:] - WARN  [RecvWorker:1:QuorumCnxManager$RecvWorker@810] - Connection broken for id 1, my id = 0, error = 
java.net.SocketException: Socket closed
	at java.net.SocketInputStream.socketRead0(Native Method)
	at java.net.SocketInputStream.socketRead(SocketInputStream.java:116)
	at java.net.SocketInputStream.read(SocketInputStream.java:170)
	at java.net.SocketInputStream.read(SocketInputStream.java:141)
	at java.net.SocketInputStream.read(SocketInputStream.java:223)
	at java.io.DataInputStream.readInt(DataInputStream.java:387)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$RecvWorker.run(QuorumCnxManager.java:795)
2016-08-23 01:15:36,777 [myid:] - WARN  [RecvWorker:0:QuorumCnxManager$RecvWorker@810] - Connection broken for id 0, my id = 1, error = 
java.io.EOFException
	at java.io.DataInputStream.readInt(DataInputStream.java:392)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$RecvWorker.run(QuorumCnxManager.java:795)
2016-08-23 01:15:36,778 [myid:] - WARN  [RecvWorker:1:QuorumCnxManager$RecvWorker@816] - Interrupting SendWorker
2016-08-23 01:15:36,778 [myid:] - WARN  [RecvWorker:0:QuorumCnxManager$RecvWorker@816] - Interrupting SendWorker
2016-08-23 01:15:36,778 [myid:] - WARN  [SendWorker:2:QuorumCnxManager$SendWorker@736] - Send worker leaving thread
2016-08-23 01:15:36,779 [myid:] - WARN  [SendWorker:0:QuorumCnxManager$SendWorker@727] - Interrupted while waiting for message on queue
java.lang.InterruptedException
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.reportInterruptAfterWait(AbstractQueuedSynchronizer.java:2014)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.awaitNanos(AbstractQueuedSynchronizer.java:2088)
	at java.util.concurrent.ArrayBlockingQueue.poll(ArrayBlockingQueue.java:418)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.pollSendQueue(QuorumCnxManager.java:879)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.access$500(QuorumCnxManager.java:65)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$SendWorker.run(QuorumCnxManager.java:715)
2016-08-23 01:15:36,778 [myid:] - INFO  [Thread-1:QuorumBase@315] - Waiting for QuorumPeer to exit thread
2016-08-23 01:15:36,778 [myid:] - WARN  [RecvWorker:2:QuorumCnxManager$RecvWorker@816] - Interrupting SendWorker
2016-08-23 01:15:36,777 [myid:] - WARN  [SendWorker:1:QuorumCnxManager$SendWorker@736] - Send worker leaving thread
2016-08-23 01:15:36,779 [myid:] - WARN  [SendWorker:0:QuorumCnxManager$SendWorker@736] - Send worker leaving thread
2016-08-23 01:15:36,778 [myid:] - WARN  [RecvWorker:0:QuorumCnxManager$RecvWorker@816] - Interrupting SendWorker
2016-08-23 01:15:36,779 [myid:] - INFO  [Thread-1:NIOServerCnxnFactory@89] - binding to port 0.0.0.0/0.0.0.0:11223
2016-08-23 01:15:36,779 [myid:] - WARN  [SendWorker:0:QuorumCnxManager$SendWorker@727] - Interrupted while waiting for message on queue
java.lang.InterruptedException
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.reportInterruptAfterWait(AbstractQueuedSynchronizer.java:2014)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.awaitNanos(AbstractQueuedSynchronizer.java:2088)
	at java.util.concurrent.ArrayBlockingQueue.poll(ArrayBlockingQueue.java:418)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.pollSendQueue(QuorumCnxManager.java:879)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.access$500(QuorumCnxManager.java:65)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$SendWorker.run(QuorumCnxManager.java:715)
2016-08-23 01:15:36,780 [myid:] - WARN  [SendWorker:0:QuorumCnxManager$SendWorker@736] - Send worker leaving thread
2016-08-23 01:15:36,780 [myid:] - INFO  [ListenerThread:QuorumCnxManager$Listener@534] - My election bind port: /0.0.0.0:11222
2016-08-23 01:15:36,780 [myid:] - INFO  [Thread-1:FLERestartTest$FLERestartThread@116] - Going to call leader election again.
2016-08-23 01:15:36,781 [myid:] - INFO  [Thread-1:FastLeaderElection@818] - New election. My id =  0, proposed zxid=0x0
2016-08-23 01:15:36,781 [myid:] - INFO  [WorkerReceiver[myid=0]:FastLeaderElection@600] - Notification: 1 (message format version), 0 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 0 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,781 [myid:] - INFO  [WorkerSender[myid=0]:QuorumCnxManager@199] - Have smaller server identifier, so dropping the connection: (1, 0)
2016-08-23 01:15:36,781 [myid:] - INFO  [/0.0.0.0:11225:QuorumCnxManager$Listener@541] - Received connection request /192.168.1.9:52186
2016-08-23 01:15:36,781 [myid:] - INFO  [WorkerSender[myid=0]:QuorumCnxManager@199] - Have smaller server identifier, so dropping the connection: (2, 0)
2016-08-23 01:15:36,781 [myid:] - INFO  [/0.0.0.0:11228:QuorumCnxManager$Listener@541] - Received connection request /192.168.1.9:52187
2016-08-23 01:15:36,782 [myid:] - INFO  [/0.0.0.0:11222:QuorumCnxManager$Listener@541] - Received connection request /192.168.1.9:52188
2016-08-23 01:15:36,782 [myid:] - INFO  [/0.0.0.0:11222:QuorumCnxManager$Listener@541] - Received connection request /192.168.1.9:52189
2016-08-23 01:15:36,782 [myid:] - INFO  [WorkerReceiver[myid=0]:FastLeaderElection@600] - Notification: 1 (message format version), 2 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 1 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,782 [myid:] - INFO  [WorkerReceiver[myid=1]:FastLeaderElection@600] - Notification: 1 (message format version), 0 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 0 (n.sid), 0x0 (n.peerEpoch) FOLLOWING (my state)
2016-08-23 01:15:36,782 [myid:] - INFO  [WorkerReceiver[myid=0]:FastLeaderElection@600] - Notification: 1 (message format version), 2 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 0 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,782 [myid:] - INFO  [WorkerReceiver[myid=2]:FastLeaderElection@600] - Notification: 1 (message format version), 0 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 0 (n.sid), 0x0 (n.peerEpoch) LEADING (my state)
2016-08-23 01:15:36,782 [myid:] - INFO  [WorkerReceiver[myid=0]:FastLeaderElection@600] - Notification: 1 (message format version), 2 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 2 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,783 [myid:] - INFO  [WorkerReceiver[myid=2]:FastLeaderElection@600] - Notification: 1 (message format version), 2 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 0 (n.sid), 0x0 (n.peerEpoch) LEADING (my state)
2016-08-23 01:15:36,783 [myid:] - INFO  [WorkerReceiver[myid=1]:FastLeaderElection@600] - Notification: 1 (message format version), 2 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LOOKING (n.state), 0 (n.sid), 0x0 (n.peerEpoch) FOLLOWING (my state)
2016-08-23 01:15:36,783 [myid:] - INFO  [WorkerReceiver[myid=0]:FastLeaderElection@600] - Notification: 1 (message format version), 2 (n.leader), 0x0 (n.zxid), 0x1 (n.round), FOLLOWING (n.state), 1 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,783 [myid:] - INFO  [WorkerReceiver[myid=0]:FastLeaderElection@600] - Notification: 1 (message format version), 2 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LEADING (n.state), 2 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,783 [myid:] - INFO  [WorkerReceiver[myid=0]:FastLeaderElection@600] - Notification: 1 (message format version), 2 (n.leader), 0x0 (n.zxid), 0x1 (n.round), LEADING (n.state), 2 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,783 [myid:] - INFO  [WorkerReceiver[myid=0]:FastLeaderElection@600] - Notification: 1 (message format version), 2 (n.leader), 0x0 (n.zxid), 0x1 (n.round), FOLLOWING (n.state), 1 (n.sid), 0x0 (n.peerEpoch) LOOKING (my state)
2016-08-23 01:15:36,988 [myid:] - INFO  [Thread-1:FLERestartTest$FLERestartThread@129] - Finished election: 0, 2
2016-08-23 01:15:36,988 [myid:] - INFO  [Thread-4:FLERestartTest$FLERestartThread@159] - Release
2016-08-23 01:15:36,988 [myid:] - INFO  [Thread-2:FLERestartTest$FLERestartThread@152] - Release
2016-08-23 01:15:36,989 [myid:] - INFO  [main:JUnit4ZKTestRunner$LoggedInvokeMethod@58] - Memory used 22159
2016-08-23 01:15:36,989 [myid:] - INFO  [main:JUnit4ZKTestRunner$LoggedInvokeMethod@63] - Number of threads 28
2016-08-23 01:15:36,989 [myid:] - INFO  [main:JUnit4ZKTestRunner$LoggedInvokeMethod@78] - FINISHED TEST METHOD testLERestart
2016-08-23 01:15:36,989 [myid:] - ERROR [/0.0.0.0:11222:QuorumCnxManager$Listener@547] - Exception while listening
java.net.SocketException: Socket closed
	at java.net.PlainSocketImpl.socketAccept(Native Method)
	at java.net.AbstractPlainSocketImpl.accept(AbstractPlainSocketImpl.java:404)
	at java.net.ServerSocket.implAccept(ServerSocket.java:545)
	at java.net.ServerSocket.accept(ServerSocket.java:513)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$Listener.run(QuorumCnxManager.java:539)
2016-08-23 01:15:36,989 [myid:] - WARN  [RecvWorker:2:QuorumCnxManager$RecvWorker@810] - Connection broken for id 2, my id = 0, error = 
java.net.SocketException: Socket closed
	at java.net.SocketInputStream.socketRead0(Native Method)
	at java.net.SocketInputStream.socketRead(SocketInputStream.java:116)
	at java.net.SocketInputStream.read(SocketInputStream.java:170)
	at java.net.SocketInputStream.read(SocketInputStream.java:141)
	at java.net.SocketInputStream.read(SocketInputStream.java:223)
	at java.io.DataInputStream.readInt(DataInputStream.java:387)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$RecvWorker.run(QuorumCnxManager.java:795)
2016-08-23 01:15:36,990 [myid:] - WARN  [SendWorker:2:QuorumCnxManager$SendWorker@727] - Interrupted while waiting for message on queue
java.lang.InterruptedException
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.reportInterruptAfterWait(AbstractQueuedSynchronizer.java:2014)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.awaitNanos(AbstractQueuedSynchronizer.java:2088)
	at java.util.concurrent.ArrayBlockingQueue.poll(ArrayBlockingQueue.java:418)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.pollSendQueue(QuorumCnxManager.java:879)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.access$500(QuorumCnxManager.java:65)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$SendWorker.run(QuorumCnxManager.java:715)
2016-08-23 01:15:36,990 [myid:] - WARN  [SendWorker:1:QuorumCnxManager$SendWorker@727] - Interrupted while waiting for message on queue
java.lang.InterruptedException
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.reportInterruptAfterWait(AbstractQueuedSynchronizer.java:2014)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.awaitNanos(AbstractQueuedSynchronizer.java:2088)
	at java.util.concurrent.ArrayBlockingQueue.poll(ArrayBlockingQueue.java:418)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.pollSendQueue(QuorumCnxManager.java:879)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.access$500(QuorumCnxManager.java:65)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$SendWorker.run(QuorumCnxManager.java:715)
2016-08-23 01:15:36,990 [myid:] - WARN  [SendWorker:1:QuorumCnxManager$SendWorker@736] - Send worker leaving thread
2016-08-23 01:15:36,990 [myid:] - WARN  [SendWorker:0:QuorumCnxManager$SendWorker@727] - Interrupted while waiting for message on queue
java.lang.InterruptedException
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.reportInterruptAfterWait(AbstractQueuedSynchronizer.java:2014)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.awaitNanos(AbstractQueuedSynchronizer.java:2088)
	at java.util.concurrent.ArrayBlockingQueue.poll(ArrayBlockingQueue.java:418)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.pollSendQueue(QuorumCnxManager.java:879)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.access$500(QuorumCnxManager.java:65)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$SendWorker.run(QuorumCnxManager.java:715)
2016-08-23 01:15:36,990 [myid:] - WARN  [SendWorker:0:QuorumCnxManager$SendWorker@736] - Send worker leaving thread
2016-08-23 01:15:36,990 [myid:] - ERROR [/0.0.0.0:11228:QuorumCnxManager$Listener@547] - Exception while listening
java.net.SocketException: Socket closed
	at java.net.PlainSocketImpl.socketAccept(Native Method)
	at java.net.AbstractPlainSocketImpl.accept(AbstractPlainSocketImpl.java:404)
	at java.net.ServerSocket.implAccept(ServerSocket.java:545)
	at java.net.ServerSocket.accept(ServerSocket.java:513)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$Listener.run(QuorumCnxManager.java:539)
2016-08-23 01:15:36,990 [myid:] - WARN  [RecvWorker:2:QuorumCnxManager$RecvWorker@810] - Connection broken for id 2, my id = 1, error = 
java.net.SocketException: Socket closed
	at java.net.SocketInputStream.socketRead0(Native Method)
	at java.net.SocketInputStream.socketRead(SocketInputStream.java:116)
	at java.net.SocketInputStream.read(SocketInputStream.java:170)
	at java.net.SocketInputStream.read(SocketInputStream.java:141)
	at java.net.SocketInputStream.read(SocketInputStream.java:223)
	at java.io.DataInputStream.readInt(DataInputStream.java:387)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$RecvWorker.run(QuorumCnxManager.java:795)
2016-08-23 01:15:36,991 [myid:] - WARN  [RecvWorker:2:QuorumCnxManager$RecvWorker@816] - Interrupting SendWorker
2016-08-23 01:15:36,990 [myid:] - WARN  [RecvWorker:2:QuorumCnxManager$RecvWorker@816] - Interrupting SendWorker
2016-08-23 01:15:36,990 [myid:] - WARN  [RecvWorker:1:QuorumCnxManager$RecvWorker@810] - Connection broken for id 1, my id = 2, error = 
java.io.EOFException
	at java.io.DataInputStream.readInt(DataInputStream.java:392)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$RecvWorker.run(QuorumCnxManager.java:795)
2016-08-23 01:15:36,989 [myid:] - WARN  [SendWorker:0:QuorumCnxManager$SendWorker@727] - Interrupted while waiting for message on queue
java.lang.InterruptedException
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.reportInterruptAfterWait(AbstractQueuedSynchronizer.java:2014)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.awaitNanos(AbstractQueuedSynchronizer.java:2088)
	at java.util.concurrent.ArrayBlockingQueue.poll(ArrayBlockingQueue.java:418)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.pollSendQueue(QuorumCnxManager.java:879)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.access$500(QuorumCnxManager.java:65)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$SendWorker.run(QuorumCnxManager.java:715)
2016-08-23 01:15:36,989 [myid:] - ERROR [/0.0.0.0:11225:QuorumCnxManager$Listener@547] - Exception while listening
java.net.SocketException: Socket closed
	at java.net.PlainSocketImpl.socketAccept(Native Method)
	at java.net.AbstractPlainSocketImpl.accept(AbstractPlainSocketImpl.java:404)
	at java.net.ServerSocket.implAccept(ServerSocket.java:545)
	at java.net.ServerSocket.accept(ServerSocket.java:513)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$Listener.run(QuorumCnxManager.java:539)
2016-08-23 01:15:36,989 [myid:] - WARN  [RecvWorker:0:QuorumCnxManager$RecvWorker@810] - Connection broken for id 0, my id = 2, error = 
java.io.EOFException
	at java.io.DataInputStream.readInt(DataInputStream.java:392)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$RecvWorker.run(QuorumCnxManager.java:795)
2016-08-23 01:15:36,989 [myid:] - WARN  [SendWorker:2:QuorumCnxManager$SendWorker@727] - Interrupted while waiting for message on queue
java.lang.InterruptedException
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.reportInterruptAfterWait(AbstractQueuedSynchronizer.java:2014)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.awaitNanos(AbstractQueuedSynchronizer.java:2088)
	at java.util.concurrent.ArrayBlockingQueue.poll(ArrayBlockingQueue.java:418)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.pollSendQueue(QuorumCnxManager.java:879)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.access$500(QuorumCnxManager.java:65)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$SendWorker.run(QuorumCnxManager.java:715)
2016-08-23 01:15:36,989 [myid:] - WARN  [RecvWorker:1:QuorumCnxManager$RecvWorker@810] - Connection broken for id 1, my id = 0, error = 
java.net.SocketException: Socket closed
	at java.net.SocketInputStream.socketRead0(Native Method)
	at java.net.SocketInputStream.socketRead(SocketInputStream.java:116)
	at java.net.SocketInputStream.read(SocketInputStream.java:170)
	at java.net.SocketInputStream.read(SocketInputStream.java:141)
	at java.net.SocketInputStream.read(SocketInputStream.java:223)
	at java.io.DataInputStream.readInt(DataInputStream.java:387)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$RecvWorker.run(QuorumCnxManager.java:795)
2016-08-23 01:15:36,992 [myid:] - WARN  [RecvWorker:1:QuorumCnxManager$RecvWorker@816] - Interrupting SendWorker
2016-08-23 01:15:36,989 [myid:] - WARN  [SendWorker:1:QuorumCnxManager$SendWorker@727] - Interrupted while waiting for message on queue
java.lang.InterruptedException
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.reportInterruptAfterWait(AbstractQueuedSynchronizer.java:2014)
	at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.awaitNanos(AbstractQueuedSynchronizer.java:2088)
	at java.util.concurrent.ArrayBlockingQueue.poll(ArrayBlockingQueue.java:418)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.pollSendQueue(QuorumCnxManager.java:879)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager.access$500(QuorumCnxManager.java:65)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$SendWorker.run(QuorumCnxManager.java:715)
2016-08-23 01:15:36,989 [myid:] - WARN  [RecvWorker:0:QuorumCnxManager$RecvWorker@810] - Connection broken for id 0, my id = 1, error = 
java.io.EOFException
	at java.io.DataInputStream.readInt(DataInputStream.java:392)
	at org.apache.zookeeper.server.quorum.QuorumCnxManager$RecvWorker.run(QuorumCnxManager.java:795)
2016-08-23 01:15:36,992 [myid:] - WARN  [SendWorker:1:QuorumCnxManager$SendWorker@736] - Send worker leaving thread
2016-08-23 01:15:36,992 [myid:] - WARN  [SendWorker:2:QuorumCnxManager$SendWorker@736] - Send worker leaving thread
2016-08-23 01:15:36,992 [myid:] - WARN  [RecvWorker:0:QuorumCnxManager$RecvWorker@816] - Interrupting SendWorker
2016-08-23 01:15:36,991 [myid:] - WARN  [SendWorker:0:QuorumCnxManager$SendWorker@736] - Send worker leaving thread
2016-08-23 01:15:36,991 [myid:] - WARN  [RecvWorker:1:QuorumCnxManager$RecvWorker@816] - Interrupting SendWorker
2016-08-23 01:15:36,991 [myid:] - INFO  [main:ZKTestCase$1@60] - SUCCEEDED testLERestart
2016-08-23 01:15:36,990 [myid:] - WARN  [SendWorker:2:QuorumCnxManager$SendWorker@736] - Send worker leaving thread
2016-08-23 01:15:36,993 [myid:] - INFO  [main:ZKTestCase$1@55] - FINISHED testLERestart
2016-08-23 01:15:36,992 [myid:] - WARN  [RecvWorker:0:QuorumCnxManager$RecvWorker@816] - Interrupting SendWorker
------------- ---------------- ---------------

Testcase: testLERestart took 0.634 sec
